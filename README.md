# 项目dc-backend

## 工程说明
本工程是数字货币支付系统软件，开发使用的golang

本工程必须放在/data/gopath目录下

bin为编译后生成的可执行文件目录

src为代码目录

pkg为包目录

work为工作环境目录

## 环境说明
操作系统：ubuntu16.04，不支持window操作系统

基本软件：gcc，go1.10，jdk8

服务软件：redis，mariadb


## 编译方法
mv dc-gateway-gopath /data/gopath

cd /data/gopath

cd src/dc-gateway

chmod +x ./build.sh

./build.sh

## 执行方法
需要将$GOPATH/bin放到$PATH环境变量中

bee run

## 代码说明
conf配置文件

controllers对外接口

logger日志输出

logs日志文件

models数据模型

routers请求路由

main.go启动函数

this project is protected by gpl.txt
