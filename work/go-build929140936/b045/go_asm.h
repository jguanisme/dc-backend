// generated by compile -asmhdr from package elliptic

#define CurveParams__size 64
#define CurveParams_P 0
#define CurveParams_N 8
#define CurveParams_B 16
#define CurveParams_Gx 24
#define CurveParams_Gy 32
#define CurveParams_BitSize 40
#define CurveParams_Name 48
#define p224Curve__size 104
#define p224Curve_CurveParams 0
#define p224Curve_gx 8
#define p224Curve_gy 40
#define p224Curve_b 72
#define const_two31p3 0x80000008
#define const_two31m3 0x7ffffff8
#define const_two31m15m3 0x7fff7ff8
#define const_two63p35 0x8000000800000000
#define const_two63m35 0x7ffffff800000000
#define const_two63m35m19 0x7ffffff7fff80000
#define const_bottom12Bits 0xfff
#define const_bottom28Bits 0xfffffff
#define p256Curve__size 8
#define p256Curve_CurveParams 0
#define p256Point__size 96
#define p256Point_xyz 0
