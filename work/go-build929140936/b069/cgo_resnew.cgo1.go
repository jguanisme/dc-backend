// Created by cgo - DO NOT EDIT

//line /data/go/src/net/cgo_resnew.go:1
// Copyright 2015 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build cgo,!netgo
// +build darwin linux,!android netbsd solaris

package net

/*
#include <sys/types.h>
#include <sys/socket.h>

#include <netdb.h>
*/
import _ "unsafe"

import "unsafe"

func cgoNameinfoPTR(b []byte, sa *_Ctype_struct_sockaddr, salen _Ctype_socklen_t) (int, error) {
	gerrno, err := (_C2func_getnameinfo)(sa, salen, (*_Ctype_char)(unsafe.Pointer(&b[0])), _Ctype_socklen_t(len(b)), nil, 0, (_Ciconst_NI_NAMEREQD))
	return int(gerrno), err
}
