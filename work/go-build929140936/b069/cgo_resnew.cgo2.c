
#line 1 "cgo-builtin-prolog"
#include <stddef.h> /* for ptrdiff_t and size_t below */

/* Define intgo when compiling with GCC.  */
typedef ptrdiff_t intgo;

typedef struct { const char *p; intgo n; } _GoString_;
typedef struct { char *p; intgo n; intgo c; } _GoBytes_;
_GoString_ GoString(char *p);
_GoString_ GoStringN(char *p, int l);
_GoBytes_ GoBytes(void *p, int n);
char *CString(_GoString_);
void *CBytes(_GoBytes_);
void *_CMalloc(size_t);

__attribute__ ((unused))
static size_t _GoStringLen(_GoString_ s) { return s.n; }

__attribute__ ((unused))
static const char *_GoStringPtr(_GoString_ s) { return s.p; }

#line 10 "/data/go/src/net/cgo_resnew.go"

#include <sys/types.h>
#include <sys/socket.h>

#include <netdb.h>

#line 1 "cgo-generated-wrapper"


#line 1 "cgo-gcc-prolog"
/*
  If x and y are not equal, the type will be invalid
  (have a negative array count) and an inscrutable error will come
  out of the compiler and hopefully mention "name".
*/
#define __cgo_compile_assert_eq(x, y, name) typedef char name[(x-y)*(x-y)*-2+1];

/* Check at compile time that the sizes we use match our expectations. */
#define __cgo_size_assert(t, n) __cgo_compile_assert_eq(sizeof(t), n, _cgo_sizeof_##t##_is_not_##n)

__cgo_size_assert(char, 1)
__cgo_size_assert(short, 2)
__cgo_size_assert(int, 4)
typedef long long __cgo_long_long;
__cgo_size_assert(__cgo_long_long, 8)
__cgo_size_assert(float, 4)
__cgo_size_assert(double, 8)

extern char* _cgo_topofstack(void);

#include <errno.h>
#include <string.h>


#define CGO_NO_SANITIZE_THREAD
#define _cgo_tsan_acquire()
#define _cgo_tsan_release()

CGO_NO_SANITIZE_THREAD
int
_cgo_f7895c2c5a3a_C2func_getnameinfo(void *v)
{
	int _cgo_errno;
	struct {
		struct sockaddr const* p0;
		socklen_t p1;
		char __pad12[4];
		char* p2;
		socklen_t p3;
		char __pad28[4];
		char* p4;
		socklen_t p5;
		int p6;
		int r;
		char __pad52[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r;
	_cgo_tsan_acquire();
	errno = 0;
	r = getnameinfo(a->p0, a->p1, a->p2, a->p3, a->p4, a->p5, a->p6);
	_cgo_errno = errno;
	_cgo_tsan_release();
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
	return _cgo_errno;
}

CGO_NO_SANITIZE_THREAD
void
_cgo_f7895c2c5a3a_Cfunc_getnameinfo(void *v)
{
	struct {
		struct sockaddr const* p0;
		socklen_t p1;
		char __pad12[4];
		char* p2;
		socklen_t p3;
		char __pad28[4];
		char* p4;
		socklen_t p5;
		int p6;
		int r;
		char __pad52[4];
	} __attribute__((__packed__, __gcc_struct__)) *a = v;
	char *stktop = _cgo_topofstack();
	__typeof__(a->r) r;
	_cgo_tsan_acquire();
	r = getnameinfo(a->p0, a->p1, a->p2, a->p3, a->p4, a->p5, a->p6);
	_cgo_tsan_release();
	a = (void*)((char*)a + (_cgo_topofstack() - stktop));
	a->r = r;
}

