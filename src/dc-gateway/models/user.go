package models

import (
	"fmt"
	"time"
	"dc-gateway/common"
	"dc-gateway/logger"
	"dc-gateway/models/orms"
    mc "dc-gateway/models/common"
    
	"github.com/astaxie/beego/orm"
)

type User struct {
    Id int64 `json:"Id,omitempty"`
    Name string `json:"Name,omitempty"`
    Type string `json:"Type,omitempty"`
    State string `json:"State,omitempty"`
    Score int64 `json:"Score,omitempty"`
    Grade int64 `json:"Grade,omitempty"`
    Email string `json:"Email,omitempty"`
    FixedPhone string `json:"FixedPhone,omitempty"`
    MobilePhone string `json:"MobilePhone,omitempty"`
    Address string `json:"Address,omitempty"`
    PostCode string `json:"PostCode,omitempty"`
    CertType string `json:"CertType,omitempty"`
    CertNo string `json:"CertNo,omitempty"`
    Created time.Time `json:"Created,omitempty"`
    RealNameAuthMark string `json:"RealNameAuthMark,omitempty"`
    MobileAuthMark string `json:"MobileAuthMark,omitempty"`
    BizLicNo string `json:"BizLicNo,omitempty"`
    TaxRegNo string `json:"TaxRegNo,omitempty"`
    CorpCertType string `json:"CorpCertType,omitempty"`
    CorpCertNo string `json:"CorpCertNo,omitempty"`
    CorpName string `json:"CorpName,omitempty"`
    RegCap int64 `json:"RegCap,omitempty"`
    CorpType string `json:"CorpType,omitempty"`
    Industry string `json:"Industry,omitempty"`
    Turnover int64 `json:"Turnover,omitempty"`
    MonAmount int64 `json:"MonAmount,omitempty"`
    CorpWebUrl string `json:"CorpWebUrl,omitempty"`
}

func transferOrmUser(ormObj *orms.User) *User {
    obj := new(User)
    obj.Id = ormObj.Id
    obj.Name = ormObj.Name
    obj.Type = ormObj.Type
    obj.State = ormObj.State
    obj.Score = ormObj.Score
    obj.Grade = ormObj.Grade
    obj.Email = ormObj.Email
    obj.FixedPhone = ormObj.FixedPhone
    obj.MobilePhone = ormObj.MobilePhone
    obj.Address = ormObj.Address
    obj.PostCode = ormObj.PostCode
    obj.CertType = ormObj.CertType
    obj.CertNo = ormObj.CertNo
    obj.Created = ormObj.CreateTime
    obj.RealNameAuthMark = ormObj.RealNameAuthMark
    obj.MobileAuthMark = ormObj.MobileAuthMark
    obj.BizLicNo = ormObj.BizLicNo
    obj.TaxRegNo = ormObj.TaxRegNo
    obj.CorpCertType = ormObj.CorpCertType
    obj.CorpCertNo = ormObj.CorpCertNo
    obj.CorpName = ormObj.CorpName
    obj.RegCap = ormObj.RegCap
    obj.CorpType = ormObj.CorpType
    obj.Industry = ormObj.Industry
    obj.Turnover = ormObj.Turnover
    obj.MonAmount = ormObj.MonAmount
    obj.CorpWebUrl = ormObj.CorpWebUrl
    return obj
}

func transferUser(args *User) (*orms.User, *[]string) {
	f := []string{}
	obj := new(orms.User)
	obj.Id = args.Id
	if args.Name != "" {
		obj.Name = args.Name
		f = append(f, "Name")
	}
    if args.Type != "" {
        obj.Type = args.Type
        f = append(f, "Type")
    }
    if args.State != "" {
        obj.State = args.State
        f = append(f, "State")
    }
    if args.Score > 0 {
        obj.Score = args.Score
        f = append(f, "Score")
    }
    if args.Grade > 0 {
        obj.Grade = args.Grade
        f = append(f, "Grade")
    }
    if args.Email != "" {
        obj.Email = args.Email
        f = append(f, "Email")
    }
    if args.FixedPhone != "" {
        obj.FixedPhone = args.FixedPhone
        f = append(f, "FixedPhone")
    }
    if args.MobilePhone != "" {
        obj.MobilePhone = args.MobilePhone
        f = append(f, "MobilePhone")
    }
    if args.Address != "" {
        obj.Address = args.Address
        f = append(f, "Address")
    }
    if args.PostCode != "" {
        obj.PostCode = args.PostCode
        f = append(f, "PostCode")
    }
	if args.CertType != "" {
		obj.CertType = args.CertType
		f = append(f, "CertType")
	}
	if args.CertNo != "" {
		obj.CertNo = args.CertNo
		f = append(f, "CertNo")
	}
	if args.RealNameAuthMark != "" {
		obj.RealNameAuthMark = args.RealNameAuthMark
		f = append(f, "RealNameAuthMark")
	}
	if args.MobileAuthMark != "" {
		obj.MobileAuthMark = args.MobileAuthMark
		f = append(f, "MobileAuthMark")
	}
	if args.BizLicNo != "" {
		obj.BizLicNo = args.BizLicNo
		f = append(f, "BizLicNo")
	}
	if args.TaxRegNo != "" {
		obj.TaxRegNo = args.TaxRegNo
		f = append(f, "TaxRegNo")
	}
	if args.CorpCertType != "" {
		obj.CorpCertType = args.CorpCertType
		f = append(f, "CorpCertType")
	}
	if args.CorpCertNo != "" {
		obj.CorpCertNo = args.CorpCertNo
		f = append(f, "CorpCertNo")
	}
	if args.CorpName != "" {
		obj.CorpName = args.CorpName
		f = append(f, "CorpName")
	}
	if args.RegCap > 0 {
		obj.RegCap = args.RegCap
		f = append(f, "RegCap")
	}
	if args.CorpType != "" {
		obj.CorpType = args.CorpType
		f = append(f, "CorpType")
	}
	if args.Industry != "" {
		obj.Industry = args.Industry
		f = append(f, "Industry")
	}
	if args.Turnover > 0 {
		obj.Turnover = args.Turnover
		f = append(f, "Turnover")
	}
	if args.MonAmount > 0 {
		obj.MonAmount = args.MonAmount
		f = append(f, "MonAmount")
	}
	if args.CorpWebUrl != "" {
		obj.CorpWebUrl = args.CorpWebUrl
		f = append(f, "CorpWebUrl")
	}
	return obj, &f
}

func GetAllUser(o orm.Ormer) ([]*User, error){
    var ormObj []*orms.User
    _, err := o.QueryTable("user").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("no users")
//      return nil, errors.New("no users")
    }

    var obj []*User

    for _, v := range ormObj {
        obj = append(obj, transferOrmUser(v))
    }

    return obj, nil
}

func QueryUser(o orm.Ormer, qm *common.QueryParam) ([]*User, error){
    sqlMode := int64(common.QueryModePrepare)
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)

    // GetPagedSql Param table, conditions, default orderby
    sql,_ := qm.GetPagedSql("user", tConds, "id desc")

    var ormObj []*orms.User
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*User
    for _,v := range ormObj {
        obj = append(obj, transferOrmUser(v))
    }

    return obj, nil
}

func GetUser(o orm.Ormer, id int64) (ret *User, e error) {
    ormObj := orms.User{Id: int64(id)}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("nonexistent user")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    }
    return transferOrmUser(&ormObj), nil
}

// Need transactions
func AddUser(o orm.Ormer, obj *User) (int64, error) {
    ormObj,_ := transferUser(obj)
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddUser: %d Success",id))
    return int64(id),err
}

func DeleteUser(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.User{Id: int64(id)}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeleteUser: %d Success",id))
    }
}

func UpdateUser(o orm.Ormer, id int64, obj *User) (a *User, err error) {
    ormObj,feilds := transferUser(obj)
    ormObj.Id = int64(id)
    if _, err := o.Update(ormObj, *feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateUser: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdateUser: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj, nil
}
