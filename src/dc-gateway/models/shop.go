package models

import (
    "fmt"
    "time"
    "dc-gateway/logger"
    "dc-gateway/common"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"
    
    "github.com/astaxie/beego/orm"
)

type Shop struct {
    Id int64 `json:"Id,omitempty"`
    Name string `json:"Name,omitempty"`
    DisplayName string `json:"DisplayName,omitempty"`
    Description string `json:"Description,omitempty"`
    Created time.Time `json:"Created,omitempty"`
    UserId int64 `json:"UserId,omitempty"`
    AccountId int64 `json:"AccountId,omitempty"`
}

func transferOrmShop(args *orms.Shop) *Shop {
    obj := new(Shop)
    obj.Id = args.Id
    obj.Name = args.Name
    obj.Description = args.Description
    if args.User!=nil{
        obj.UserId = args.User.Id
    }
    if args.Account!=nil{
        obj.AccountId = args.Account.Id
    }
    obj.Created = args.Created
    return obj
}

func transferShop(obj *Shop) (*orms.Shop, *[]string) {
    f := []string{}
    ormObj := new(orms.Shop)
    ormObj.Id = obj.Id
    if obj.Name != "" {
        ormObj.Name = obj.Name
        f = append(f, "Name")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f = append(f, "Description")
    }
    if obj.UserId != 0 {
        ormObj.User = new(orms.User)
        ormObj.User.Id = obj.UserId
        f = append(f, "UserId")
    }
    if obj.AccountId != 0 {
        ormObj.Account = new(orms.Account)
        ormObj.Account.Id = obj.AccountId
        f = append(f, "AccountId")
    }
    return ormObj, &f
}

func GetShop(o orm.Ormer, id int64) (ret *Shop, e error) {
    ormObj := orms.Shop{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllShop No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return transferOrmShop(&ormObj), nil
}

func GetAllShop(o orm.Ormer) ([]*Shop, error){
    var ormObj []*orms.Shop
    _, err := o.QueryTable("shop").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllShop No Rows")
        // return nil,errors.New("查询不到")
    } 

    var obj []*Shop
    for _,v := range ormObj{
        obj = append(obj, transferOrmShop(v))
    }

    return obj, nil
}

func QueryShop(o orm.Ormer, qm *common.QueryParam) ([]*Shop, error){
    sqlMode := int64(common.QueryModePrepare)
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)

    // GetPagedSql Param    table,  conditions, default orderby
    sql,_ := qm.GetPagedSql("shop", tConds,     "id desc")
    
    var ormObj []*orms.Shop
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*Shop
    for _,v := range ormObj{
        obj=append(obj,transferOrmShop(v))
    }

    return obj,nil
}

// Need transactions
func AddShop(o orm.Ormer, obj *Shop) (int64,error) {
    ormObj, _ := transferShop(obj)
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddShop: %d Success",id))
    return int64(id),err
}

func DeleteShop(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.Shop{Id: id}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeleteShop: %d Success",id))
    }
}

func UpdateShop(o orm.Ormer, id int64, obj *Shop) (*Shop, error) {
    ormObj,feilds := transferShop(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateShop: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdateShop: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj,nil
}
