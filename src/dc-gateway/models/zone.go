package models

import (
    "fmt"
    "time"
    "dc-gateway/logger"
    "dc-gateway/common"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"
    
    "github.com/astaxie/beego/orm"
)

type Zone struct {
    Id int64 `json:"Id,omitempty"`
    Name string `json:"Name,omitempty"`
    Description string `json:"Description,omitempty"`
    Domain string `json:"Domain,omitempty"`
    PrivKey string `json:"PrivKey,omitempty"`
    PubKey string `json:"PubKey,omitempty"`
    Created time.Time `json:"Created,omitempty"`
}

func TransferOrmZone(ormObj *orms.Zone) *Zone{
    obj := new(Zone)
    obj.Id = ormObj.Id
    obj.Name = ormObj.Name
    obj.Description = ormObj.Description
    obj.Domain = ormObj.Domain
    obj.PrivKey = ormObj.PrivKey
    obj.PubKey = ormObj.PubKey
    obj.Created = ormObj.Created
    return obj
}

func TransferZone(obj *Zone) (*orms.Zone,*[]string){
    f := []string{}
    ormObj := new(orms.Zone)
    ormObj.Id = obj.Id
    if obj.Name != "" {
        ormObj.Name = obj.Name
        f = append(f, "Name")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f = append(f, "Description")
    }
    if obj.Domain != "" {
        ormObj.Domain = obj.Domain
        f = append(f, "Domain")
    }
    if obj.PrivKey != "" {
        ormObj.PrivKey = obj.PrivKey
        f = append(f, "PrivKey")
    }
    if obj.PubKey != "" {
        ormObj.PubKey = obj.PubKey
        f = append(f, "PubKey")
    }
/*
    if obj.Created != "" {
        ormObj.Created = obj.Created
    }
*/
    return ormObj, &f
}


func GetZone(o orm.Ormer, id int64) (ret *Zone, e error) {
    ormObj := orms.Zone{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllZone No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return TransferOrmZone(&ormObj), nil
}

func GetAllZone(o orm.Ormer) ([]*Zone, error){
    var ormObj []*orms.Zone
    _, err := o.QueryTable("zone").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllZone No Rows")
        // return nil,errors.New("查询不到")
    } 

    var obj []*Zone
    for _,v := range ormObj{
        obj=append(obj,TransferOrmZone(v))
    }

    return obj,nil
}

func QueryZone(o orm.Ormer, qm *common.QueryParam) ([]*Zone, error){
    sqlMode := int64(common.QueryModePrepare)
    
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)
    
    // GetPagedSql Param    table,  conditions, default orderby
    sql,_ := qm.GetPagedSql("zone", tConds,     "id desc")

    var ormObj []*orms.Zone
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*Zone
    for _,v := range ormObj{
        obj=append(obj,TransferOrmZone(v))
    }

    return obj,nil
}


// Need transactions
func AddZone(o orm.Ormer, obj *Zone) (int64,error) {
    ormObj,_ := TransferZone(obj)
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddZone: %d Success",id))
    return int64(id),err
}

func DeleteZone(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.Zone{Id: id}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeleteZone: %d Success",id))
    }
}

func UpdateZone(o orm.Ormer, id int64, obj *Zone) (a *Zone, err error) {
    ormObj,feilds := TransferZone(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj, *feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateZone: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{
        logger.Log.Informational(fmt.Sprintf("UpdateZone: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj, nil
}
