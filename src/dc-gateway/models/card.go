package models

import (
    "fmt"
    "time"
    "dc-gateway/logger"
    "dc-gateway/common"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"

    "github.com/astaxie/beego/orm"
)

type Card struct {
    Id int64 `json:"Id,omitempty"`
    Name string `json:"Name,omitempty"`
    CardNumber string `json:"CardNumber,omitempty"`
    Phone string `json:"Phone,omitempty"`
    IdNumber string `json:"IdNumber,omitempty"`
    PersonName string `json:"PersonName,omitempty"`
    Description string `json:"Description,omitempty"`
    Created time.Time `json:"Created,omitempty"`
    Attribute string `json:"Attribute,omitempty"`
    WalletId int64 `json:"WalletId,omitempty"`
}


func transferOrmCard(ormObj *orms.Card) *Card{
    obj := new(Card)
    obj.Id = ormObj.Id
    obj.Name = ormObj.Name
    obj.Description = ormObj.Description
    obj.Phone = ormObj.Phone

    cn,err:=common.DesDecryptStr(ormObj.CardNumber,ormObj.Phone)
    if err==nil{
        obj.CardNumber=cn
    }
    idn,err:=common.DesDecryptStr(ormObj.IdNumber,ormObj.Phone)
    if err==nil{
        obj.IdNumber=idn
    }
    pn,err:=common.DesDecryptStr(ormObj.PersonName,ormObj.Phone)
    if err==nil{
        obj.PersonName=pn
    }
    att,err:=common.DesDecryptStr(ormObj.Attribute,ormObj.Phone)
    if err==nil{
        obj.Attribute=att
    }
    obj.Created = ormObj.Created
    if ormObj.Wallet != nil {
        obj.WalletId = ormObj.Wallet.Id
    }    

    return obj
}

func transferCard(obj *Card) (*orms.Card,*[]string){
    f := []string{}
    ormObj := new(orms.Card)
    ormObj.Id = obj.Id
    if obj.Name != "" {
        ormObj.Name = obj.Name
        f=append(f,"Name")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.Phone != "" {
        ormObj.Phone = obj.Phone
        f=append(f,"Phone")
    }
    if obj.CardNumber != "" {
        d,err:=common.DesEncryptStr(obj.CardNumber,obj.Phone)
        if err==nil{
            ormObj.CardNumber=d
        }
        f=append(f,"CardNumber")
    }
    if obj.IdNumber != "" {
        d,err:=common.DesEncryptStr(obj.IdNumber,obj.Phone)
        if err==nil{
            ormObj.IdNumber=d
        }
        f=append(f,"IdNumber")
    }
    if obj.PersonName != "" {
        d,err:=common.DesEncryptStr(obj.PersonName,obj.Phone)
        if err==nil{
            ormObj.PersonName=d
        }
        f=append(f,"PersonName")
    }
    if obj.Attribute != "" {
        d,err:=common.DesEncryptStr(obj.Attribute,obj.Phone)
        if err==nil{
            ormObj.Attribute=d
        }
        f=append(f,"Attribute")
    }
    if obj.WalletId != 0 {
        ormObj.Wallet = new (orms.Wallet)
        ormObj.Wallet.Id = obj.WalletId
        f=append(f,"Wallet")
    } 
    //if obj.Created != "" {
        //ormObj.Created = obj.Created
    //}
    return ormObj,&f
}


func GetCard(o orm.Ormer, id int64) (ret *Card, e error) {
    ormObj := orms.Card{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllCard No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return transferOrmCard(&ormObj),nil
}

func GetAllCard(o orm.Ormer) ([]*Card, error){
    var ormObj []*orms.Card
    _, err := o.QueryTable("card").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllCard No Rows")
        // return nil,errors.New("查询不到")
    } 

    var obj []*Card
    for _,v := range ormObj{
        obj=append(obj,transferOrmCard(v))
    }

    return obj,nil
}

func QueryCard(o orm.Ormer, qm *common.QueryParam) ([]*Card, error){
    sqlMode := int64(common.QueryModePrepare)
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)

    // GetPagedSql Param    table,  conditions, default orderby
    sql,_ := qm.GetPagedSql("card", tConds,     "id desc")

    var ormObj []*orms.Card
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*Card
    for _,v := range ormObj{
        obj=append(obj,transferOrmCard(v))
    }

    return obj,nil
}

func QueryCardByWalletId(o orm.Ormer, walletId int64) ([]*Card, error){
    var ormObj []*orms.Card

    sql := "SELECT * FROM card WHERE %s=? ORDER BY created DESC"
    _, err := o.Raw(fmt.Sprintf(sql,"wallet_id"), walletId).QueryRows(&ormObj)
    if err == nil {
        //fmt.Println("card nums: ", num)
    }

    var obj []*Card
    for _,v := range ormObj{
        obj=append(obj,transferOrmCard(v))
    }

    return obj,nil
}

// Need transactions
func AddCard(o orm.Ormer, obj *Card) (int64,error) {
    ormObj,_ := transferCard(obj)
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddCard: %d Success",id))
    return int64(id),err
}

func DeleteCard(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.Card{Id: id}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeleteCard: %d Success",id))
    }
}

func UpdateCard(o orm.Ormer, id int64, obj *Card) (a *Card, err error) {
    ormObj,feilds := transferCard(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateCard: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdateCard: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj,nil
}
