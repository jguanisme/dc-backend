package models

import (
    "errors"
    "fmt"
    "time"
    "dc-gateway/logger"
    "dc-gateway/common"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"

    "github.com/astaxie/beego/orm"
)

type Wallet struct {
    Id int64 `json:"Id,omitempty"`
    Name string `json:"Name,omitempty"`
    Image string `json:"Image,omitempty"`
    Description string `json:"Description,omitempty"`
    Mode int64 `json:"Mode,omitempty"`
    Created time.Time `json:"Created,omitempty"`
    AccountId int64 `json:"AccountId,omitempty"`
    Balance int64 `json:"Balance,omitempty"`
    PubKey string `json:"PubKey,omitempty"`
    PrivKey string `json:"PrivKey,omitempty"`
    UtxoAddress	string `json:"UtxoAddress,omitempty"`
}

func TransferOrmWallet(ormObj *orms.Wallet) *Wallet{
    obj := new(Wallet)
    obj.Id = ormObj.Id
    obj.Name = ormObj.Name
    obj.Mode = ormObj.Mode
    obj.Description = ormObj.Description
    obj.Created = ormObj.Created
    obj.Balance = ormObj.Balance
    obj.PubKey = ormObj.PubKey
    if ormObj.PrivKey!=""{
        pk,err:=common.DesDecryptStr(ormObj.PrivKey,ormObj.UtxoAddress)
        if err==nil{
            obj.PrivKey=pk
        }        
    }
    obj.UtxoAddress = ormObj.UtxoAddress
    if ormObj.Account != nil {
        obj.AccountId = int64(ormObj.Account.Id)
    }    
    return obj
}

func TransferWallet(obj *Wallet) (*orms.Wallet,*[]string){
    f := []string{}
    ormObj := new(orms.Wallet)
    ormObj.Id = obj.Id

    if obj.Balance > 0 {
        ormObj.Balance = obj.Balance
        f=append(f,"Balance")
    }
    if obj.Name != "" {
        ormObj.Name = obj.Name
        f=append(f,"Name")
    }  
    if obj.Mode > 0 {
        ormObj.Mode = obj.Mode
        f=append(f,"Mode")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.PubKey != "" {
        ormObj.PubKey = obj.PubKey
        f=append(f,"PubKey")
    }
    if obj.PrivKey != "" {
        pk,err:=common.DesEncryptStr(obj.PrivKey,obj.UtxoAddress)
        if err==nil{
            ormObj.PrivKey=pk
        }
        f=append(f,"PrivKey")
    }
    if obj.UtxoAddress != "" {
        ormObj.UtxoAddress = obj.UtxoAddress
        f=append(f,"UtxoAddress")
    }
    if obj.AccountId != 0 {
        ormObj.Account = new (orms.Account)
        ormObj.Account.Id = int64(obj.AccountId)
        f=append(f,"Account")
    }
    return ormObj, &f
}

func GetWallet(o orm.Ormer, id int64) (ret *Wallet, e error) {
    ormObj := orms.Wallet{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllWallet No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    }
    ormObj.PrivKey = ""
    return TransferOrmWallet(&ormObj),nil
}

//need private key for full client mode
func GetWalletByUtxoAddress(o orm.Ormer, utxoAddress string) (ret *Wallet, e error) {
    var ormObj []*orms.Wallet

    sql := "SELECT * FROM wallet WHERE %s=? ORDER BY created DESC"
    num, err := o.Raw(fmt.Sprintf(sql,"utxo_address"), utxoAddress).QueryRows(&ormObj)
    if err != nil {
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }
    if num>=1{
        return TransferOrmWallet(ormObj[0]),nil
    }else{
        return nil,errors.New("GetWalletByUtxoAddress No Rows")
    }
}

func GetAllWallet(o orm.Ormer) ([]*Wallet, error){
    var ormObj []*orms.Wallet
    _, err := o.QueryTable("wallet").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllWallet No Rows")
        // return nil,errors.New("查询不到")
    } 

    var obj []*Wallet
    for _,v := range ormObj{
        v.PrivKey = ""
        obj=append(obj,TransferOrmWallet(v))
    }

    return obj,nil
}

func QueryWallet(o orm.Ormer, qm *common.QueryParam) ([]*Wallet, error){
    sqlMode := int64(common.QueryModePrepare)

    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)
    // GetPagedSql Param    table,  conditions, default orderby
    sql,_ := qm.GetPagedSql("wallet", tConds,     "id desc")

    var ormObj []*orms.Wallet
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*Wallet
    for _,v := range ormObj{
        v.PrivKey = ""
        obj=append(obj,TransferOrmWallet(v))
    }

    return obj,nil
}

func QueryWalletByAccountId(o orm.Ormer, accountId int64) ([]*Wallet, error){
    var ormObj []*orms.Wallet

    sql := "SELECT * FROM wallet WHERE %s=? ORDER BY created DESC"
    _, err := o.Raw(fmt.Sprintf(sql,"account_id"), accountId).QueryRows(&ormObj)
    if err == nil {
        //fmt.Println("wallet nums: ", num)
    }

    var obj []*Wallet
    for _,v := range ormObj{
        v.PrivKey = ""
        obj=append(obj,TransferOrmWallet(v))
    }

    return obj,nil
}

// Need transactions
func AddDefaultWallet(o orm.Ormer, addr string) (int64,error) {
    var w Wallet
    w.Name = "Default Wallet"
    w.Description = ""
    w.Mode = 1
    w.AccountId = -1
    w.Balance = 0
    w.PubKey = ""
    w.PrivKey = ""
    w.UtxoAddress = addr

    ormObj,_ := TransferWallet(&w)
    id, err := o.Insert(ormObj)    
    if err!=nil{
        return 0, common.MakeFromError(common.CodeDBInsertError,err)
    }
    return int64(id),err
}
func AddWallet(o orm.Ormer, obj *Wallet) (int64,error) {
    w,werr:=GetWalletByUtxoAddress(o,obj.UtxoAddress)
    if werr!=nil||w==nil{
        obj.Balance=0
        if obj.Mode <= 0{
            obj.Mode = 1
        }
        ormObj,_ := TransferWallet(obj)
        id, err := o.Insert(ormObj)
        if err!=nil{
            return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
        }
        logger.Log.Informational(fmt.Sprintf("AddWallet: %d Success",id))
        return int64(id),err
    }else{
        uo,err := UpdateWallet(o,w.Id,obj)
        if err!=nil||uo==nil{
            return 0,common.MakeFromError(common.CodeDBUpdateError,err)
        }
        return uo.Id,nil
    }
}

func DeleteWallet(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.Wallet{Id: id}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeleteWallet: %d Success",id))
    }
}

func UpdateWallet(o orm.Ormer, id int64, obj *Wallet) (a *Wallet, err error) {
    ormObj,feilds := TransferWallet(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateWallet: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdateWallet: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj,nil
}

func UpdateWalletBalance(o orm.Ormer, obj *Wallet, amount uint64, isAdd bool) error {
    //update wallet balance    
    flag := "+"
    if !isAdd {
        flag = "-"
    }
    sql := "UPDATE wallet SET balance = balance %s ? WHERE %s=?"
    _, err := o.Raw(fmt.Sprintf(sql,flag,"id"), amount,obj.Id).Exec()
    if err!=nil {
        logger.Log.Informational(fmt.Sprintf("对象Wallet修改记录为:%d, 错误:%s",obj.Id,err.Error()))
        return common.MakeFromError(common.CodeDBUpdateError,err)
    }
    return nil
}

func UpdateWalletBalanceByAddr(o orm.Ormer, addr string, amount uint64, isAdd bool) error {
    //update wallet balance    
    flag := "+"
    if !isAdd {
        flag = "-"
    }
    sql := "UPDATE wallet SET balance = balance %s ? WHERE %s=?"
    _, err := o.Raw(fmt.Sprintf(sql,flag,"utxo_address"), amount,addr).Exec()
    if err!=nil {
        logger.Log.Informational(fmt.Sprintf("对象Wallet修改 Addr:%s, 错误:%s",addr,err.Error()))
        return common.MakeFromError(common.CodeDBUpdateError,err)
    }
    return nil
}


