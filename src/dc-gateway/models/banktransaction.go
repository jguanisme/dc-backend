package models

import (
    "errors"
    "fmt"
    "time"
    "dc-gateway/logger"
    "dc-gateway/common"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"

    "github.com/astaxie/beego/orm"
)

const(
    _ = iota
    BankTransactionTypeIssue
    BankTransactionTypeDeposit
    BankTransactionTypeTransfer
    BankTransactionTypePay
)

const(
    _ = iota
    BankTransactionStateBankStart
    BankTransactionStateBankFailed
    BankTransactionStateBankSuccess
    BankTransactionStateBankFatal
)

type BankTransaction struct {
    Id int64 `json:"Id,omitempty"`
    //1取钱， 2存钱，3转账，4收钱
    BankTransactionType int64 `json:"BankTransactionType,omitempty"`
    Description string `json:"Description,omitempty"`
    Amount int64 `json:"Amount,omitempty"`
    Attributes string `json:"Attributes,omitempty"`
    CurrencyId int64 `json:"CurrencyId,omitempty"`
    CardId int64 `json:"CardId,omitempty"`
    CardAttribute string `json:"CardAttribute,omitempty"`
    WalletType int64 `json:"WalletType,omitempty"`
    WalletAddr string `json:"WalletAddr,omitempty"`
    ZoneName string `json:"ZoneName,omitempty"`
    AccountName string `json:"AccountName,omitempty"`
    //1 开始事务银行未完成, 2 银行完成失败 重试中, 3 银行完成成功, 4 银行完成失败 不再重试
    State int64 `json:"State,omitempty"`
    OrderId string `json:"OrderId,omitempty"`
    BankResult string `json:"BankResult,omitempty"`
    BankTransactionSeq string `json:"BankTransactionSeq,omitempty"`
    BankTransactionTime string `json:"BankTransactionTime,omitempty"`//交易发生时间
    Created time.Time `json:"Created,omitempty"`//创建时间

    UtxoPayTx string   `json:"UtxoPayTx,omitempty"`//json of tx signed by wallet and issue server
}

func transferOrmBankTransaction(ormObj *orms.BankTransaction) *BankTransaction{
    obj := new(BankTransaction)
    obj.Id = ormObj.Id
    obj.BankTransactionType = ormObj.BankTransactionType
    obj.Description = ormObj.Description
    obj.CurrencyId = ormObj.CurrencyId
    obj.CardId = ormObj.CardId
    if ormObj.CardAttribute!=""&&ormObj.OrderId!=""{
        ca,err:=common.DesDecryptStr(ormObj.CardAttribute,ormObj.OrderId)
        if err==nil{
            obj.CardAttribute=ca
        }
    }
    obj.Amount = ormObj.Amount
    obj.Attributes = ormObj.Attributes
    obj.WalletType = ormObj.WalletType
    obj.WalletAddr = ormObj.WalletAddr
    obj.ZoneName = ormObj.ZoneName
    obj.AccountName = ormObj.AccountName
    obj.State = ormObj.State
    obj.OrderId = ormObj.OrderId
    obj.BankResult = ormObj.BankResult
    obj.BankTransactionSeq = ormObj.BankTransactionSeq
    obj.BankTransactionTime = ormObj.BankTransactionTime
    obj.Created = ormObj.Created

    return obj
}

func transferBankTransaction(obj *BankTransaction) (*orms.BankTransaction,*[]string){
    f := []string{}
    ormObj := new(orms.BankTransaction)
    ormObj.Id = obj.Id
    if obj.BankTransactionType > 0 {
        ormObj.BankTransactionType = obj.BankTransactionType
        f=append(f,"BankTransactionType")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.Amount > 0 {
        ormObj.Amount = obj.Amount
        f=append(f,"Amount")
    }
    if obj.CurrencyId != 0 {
        ormObj.CurrencyId = obj.CurrencyId
        f=append(f,"CurrencyId")
    }
    if obj.CardId != 0 {
        ormObj.CardId = obj.CardId
        f=append(f,"CardId")
    }
    if obj.CardAttribute!=""&&obj.OrderId!=""{
        d,err:=common.DesEncryptStr(obj.CardAttribute,obj.OrderId)
        if err==nil{
            ormObj.CardAttribute=d
        }
        f=append(f,"CardAttribute")
    }
    if obj.Attributes != "" {
        ormObj.Attributes = obj.Attributes
        f=append(f,"Attributes")
    }
    if obj.WalletType > 0 {
        ormObj.WalletType = obj.WalletType
        f=append(f,"WalletType")
    }     
    if obj.WalletAddr != "" {
        ormObj.WalletAddr = obj.WalletAddr
        f=append(f,"WalletAddr")
    }
    if obj.ZoneName != "" {
        ormObj.ZoneName = obj.ZoneName
        f=append(f,"ZoneName")
    }      
    if obj.AccountName != "" {
        ormObj.AccountName = obj.AccountName
        f=append(f,"AccountName")
    }
    if obj.BankTransactionSeq != "" {
        ormObj.BankTransactionSeq = obj.BankTransactionSeq
        f=append(f,"BankTransactionSeq")
    }
    if obj.State > 0 {
        ormObj.State = obj.State
        f=append(f,"State")
    }  
    if obj.OrderId != "" {
        ormObj.OrderId = obj.OrderId
        //f=append(f,"OrderId")
    }
    if obj.BankResult != "" {
        ormObj.BankResult = obj.BankResult
        f=append(f,"BankResult")
    }
    if obj.BankTransactionTime != "" {
        ormObj.BankTransactionTime = obj.BankTransactionTime
        f=append(f,"BankTransactionTime")
    }
    return ormObj,&f
}

func GetBankTransaction(o orm.Ormer, id int64) (ret *BankTransaction, e error) {
    ormObj := orms.BankTransaction{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllBankTransaction No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return transferOrmBankTransaction(&ormObj),nil
}

func GetAllBankTransaction(o orm.Ormer) ([]*BankTransaction, error){
    var ormObj []*orms.BankTransaction
    _, err := o.QueryTable("bank_transaction").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllBankTransaction No Rows")
        // return nil,errors.New("查询不到")
    } 

    var obj []*BankTransaction
    for _,v := range ormObj{
        obj=append(obj,transferOrmBankTransaction(v))
    }

    return obj,nil
}

func QueryBankTransaction(o orm.Ormer, qm *common.QueryParam) ([]*BankTransaction, error){
    sqlMode := int64(common.QueryModePrepare)
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)

    // GetPagedSql Param    table,  conditions, default orderby
    sql,_ := qm.GetPagedSql("bank_transaction", tConds,     "id desc")

    var ormObj []*orms.BankTransaction
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*BankTransaction
    for _,v := range ormObj{
        obj=append(obj,transferOrmBankTransaction(v))
    }

    return obj,nil
}

type DepositRange struct{
    Id int64 `json:"Id,omitempty"`
    Amount int64 `json:"Amount,omitempty"`
    Created string `json:"Created,omitempty"`
    OrderId string `json:"OrderId,omitempty"`
    CardAttribute string `json:"CardAttribute,omitempty"`
}

func QueryDepositRange(o orm.Ormer,st string,ed string) ([]*DepositRange, error){
    var obj []*DepositRange
    
    sql:="select id,amount,created,order_id,card_attribute from bank_transaction where created between ? and ? and bank_transaction_type=2 and state=1"
    _, err := o.Raw(sql,st,ed).QueryRows(&obj)
    if err != nil {
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    for _,v:=range(obj){
            if v.CardAttribute!=""&&v.OrderId!=""{
            att,err:=common.DesDecryptStr(v.CardAttribute,v.OrderId)
            if err==nil{
                v.CardAttribute=att
            }            
        }
    }
    return obj,nil
}

func GetBankTransactionByOrderId(o orm.Ormer, orderId string) (ret *BankTransaction, e error) {
    var ormObj []*orms.BankTransaction

    sql := "SELECT * FROM bank_transaction WHERE %s=? ORDER BY created DESC"
    num, err := o.Raw(fmt.Sprintf(sql,"order_id"), orderId).QueryRows(&ormObj)
    if err != nil {
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }
    if num>=1{
        return transferOrmBankTransaction(ormObj[0]),nil
    }else{
        return nil,nil
    }
}

// Need transactions
func StartBankTransaction(o orm.Ormer, obj *BankTransaction) (string,error) {
    if obj.OrderId==""{
        obj.OrderId = mc.GenOrderId("MBTrans")
    }
    ormObj,_ := transferBankTransaction(obj)
    ormObj.State = BankTransactionStateBankStart //开始事务
    id, err := o.Insert(ormObj)
    if err != nil {
        logger.Log.Informational(fmt.Sprintf("AddBankTransaction: %d Success",id))
        return "", common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    return ormObj.OrderId,nil
}

func UpdateBankTransactionByOrderId(o orm.Ormer, obj *BankTransaction) (a *BankTransaction, err error) {
    bt,e := GetBankTransactionByOrderId(o,obj.OrderId)
    if bt == nil || e != nil {
        return nil,common.MakeError(common.CodeDBNoSuchDataError,"UpdateBankTransactionByOrderId No Such OrderId "+obj.OrderId)
    }
    ormObj,feilds := transferBankTransaction(obj)
    ormObj.Id = bt.Id
    if obj.State==0{
        ormObj.State = BankTransactionStateBankSuccess//事务已完成
    }
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateBankTransaction: %d, Error: %s", bt.Id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdateBankTransaction: %d, Success", bt.Id))
    }
    //obj.Id = ormObj.Id
    return bt,nil
}

func AddBankTransaction(o orm.Ormer, obj *BankTransaction) (int64,error) {
    if obj.OrderId==""{
        obj.OrderId = mc.GenOrderId("MBTrans")
    }
    ormObj,_ := transferBankTransaction(obj)
    id, err := o.Insert(ormObj)
    if err != nil {
        logger.Log.Informational(fmt.Sprintf("AddBankTransaction: %d Success",id))
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    return int64(id),err
}

func DeleteBankTransaction(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.Transaction{Id: id}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeleteBankTransaction: %d Success",id))
    }
}

func UpdateBankTransaction(o orm.Ormer, id int64, obj *BankTransaction) (a *BankTransaction, err error) {
    ormObj,feilds := transferBankTransaction(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateBankTransaction: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdateBankTransaction: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj,nil
}


/*=============exchang=============*/
func transferExchangeToTransactionHistory(obj *BankTransaction) (*orms.TransactionHistory,*[]string){
    f := []string{}
    ormObj := new(orms.TransactionHistory)
    ormObj.Id = obj.Id
    if obj.BankTransactionType > 0 {
        ormObj.TransactionType = obj.BankTransactionType
        f=append(f,"TransactionType")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.Amount >= 0 {
        ormObj.Amount = obj.Amount
        f=append(f,"Amount")
    }     
    if obj.Attributes != "" {
        ormObj.Attributes = obj.Attributes
        f=append(f,"Attributes")
    }
    if obj.AccountName != "" {
        ormObj.OutName = obj.AccountName
        f=append(f,"OutName")
    }
    if obj.WalletAddr != "" {
        ormObj.OutAddr = obj.WalletAddr
        f=append(f,"OutAddr")
    }
    if obj.State > 0 {
        ormObj.State = obj.State
        f=append(f,"State")
    }  
    if obj.WalletType > 0 {
        ormObj.WalletType = obj.WalletType
        f=append(f,"WalletType")
    }    
    if obj.ZoneName != "" {
        ormObj.ZoneName = obj.ZoneName
        f=append(f,"ZoneName")
    }
    if obj.BankTransactionSeq != "" {
        ormObj.TransactionSeq = obj.BankTransactionSeq
        f=append(f,"TransactionSeq")
    }
    if obj.BankTransactionTime != "" {
        ormObj.TransactionTime = obj.BankTransactionTime
        f=append(f,"TransactionTime")
    }

    //if obj.Created != "" {
        //ormObj.Created = obj.Created
    //}

    return ormObj,&f
}

func transferDepositToTransactionHistory(obj *BankTransaction) (*orms.TransactionHistory,*[]string){
    f := []string{}
    ormObj := new(orms.TransactionHistory)
    ormObj.Id = obj.Id
    if obj.BankTransactionType > 0 {
        ormObj.TransactionType = obj.BankTransactionType
        f=append(f,"TransactionType")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.Amount >= 0 {
        ormObj.Amount = obj.Amount
        f=append(f,"Amount")
    }     
    if obj.Attributes != "" {
        ormObj.Attributes = obj.Attributes
        f=append(f,"Attributes")
    }
    if obj.AccountName != "" {
        ormObj.InName = obj.AccountName
        f=append(f,"InName")
    }
    if obj.WalletAddr != "" {
        ormObj.InAddr = obj.WalletAddr
        f=append(f,"InAddr")
    }
    if obj.State > 0 {
        ormObj.State = obj.State
        f=append(f,"State")
    }  
    if obj.WalletType > 0 {
        ormObj.WalletType = obj.WalletType
        f=append(f,"WalletType")
    }    
    if obj.ZoneName != "" {
        ormObj.ZoneName = obj.ZoneName
        f=append(f,"ZoneName")
    }
    if obj.BankTransactionSeq != "" {
        ormObj.TransactionSeq = obj.BankTransactionSeq
        f=append(f,"TransactionSeq")
    }
    if obj.BankTransactionTime != "" {
        ormObj.TransactionTime = obj.BankTransactionTime
        f=append(f,"TransactionTime")
    }

    //if obj.Created != "" {
        //ormObj.Created = obj.Created
    //}

    return ormObj,&f
}

func Deposit(o orm.Ormer, obj *BankTransaction) (*string,error)  {
    aw,awerr:=GetWalletByUtxoAddress(o,obj.WalletAddr)
    if awerr!=nil||aw==nil{
        //return nil,awerr
        id,_:=AddDefaultWallet(o,obj.WalletAddr)
        aw,_=GetWallet(o,id)
    }
    ac,err := QueryCurrencyByWalletIdForUpdate(o,aw.Id)
    if err != nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }
    m := make(map[int64]int64)
    amount := obj.Amount
    for _,v :=range ac{
        fmt.Println("Deposit test:",amount,v.Amount,v.Id)
        if amount <= v.Amount {
            m[v.Id]=v.Amount - amount
            amount -= v.Amount
            break
        }

        m[v.Id] = 0
        amount -= v.Amount
    }    
    
    if amount > 0{
        return nil,errors.New("没有足够的金额")
    }
    //need do utxo, remove old, add new currency
    for k,v :=range m{
        var tc Currency
        tc.Id=k
        tc.Amount=v
        if v==0{
            tc.State=104//已作废，或者待删除
        }
        UpdateCurrency(o,tc.Id,&tc)
    }
    //    
    //need add currency circulating later,and set invalid currency

    //

    //update wallet balance    
    sql := "UPDATE wallet SET balance = balance %s ? WHERE %s=?"
    _, uerr2 := o.Raw(fmt.Sprintf(sql,"-","id"), obj.Amount,aw.Id).Exec()
    if uerr2!=nil {
        logger.Log.Informational(fmt.Sprintf("对象Wallet修改记录为:%d, 错误:%s",aw.Id,uerr2.Error()))
        return nil,common.MakeFromDBError(common.CodeDBQueryError,uerr2)
    }

    obj.BankTransactionType=1//存钱
    obj.State=1//开始存钱事务，银行操作未完成

    orderId,oe := StartBankTransaction(o, obj)
    // BankTransactionormObj,_ := transferBankTransaction(obj)
    // id, _ := o.Insert(BankTransactionormObj)
    if oe!= nil{
        return nil,oe
    }
    logger.Log.Informational(fmt.Sprintf("Deposit StartBankTransaction: %s Success",orderId))



    ormObj,_ := transferDepositToTransactionHistory(obj)
    hid, err := o.Insert(ormObj)
    if err!= nil{
        return nil, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddBankTransactionHistory: %d Success",hid))

    return &orderId,nil
}

func AddBankTransactionHistory(o orm.Ormer, obj *BankTransaction, isDeposit bool)( uint64, error) {
    var ormObj *orms.TransactionHistory
    if isDeposit {
        ormObj,_ = transferDepositToTransactionHistory(obj)
    }else{
        ormObj,_ = transferExchangeToTransactionHistory(obj)
    }
    ormObj.Id=0
    hid, err := o.Insert(ormObj)
    if err!= nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddBankTransactionHistory: %d Success",hid))
    return uint64(hid),nil
}

func Exchange(o orm.Ormer, obj *BankTransaction) (*BankTransaction,error) {
    var dest_currency_id int64
    bt,_ := GetBankTransactionByOrderId(o,obj.OrderId)
    if bt!=nil && bt.State>=2 {
        return nil,common.MakeError(common.CodeTxAlreadyCompletedError,"Bank Transaction alreadly compeleted")
    }

    aw,awerr:=GetWalletByUtxoAddress(o,obj.WalletAddr)
    if awerr!=nil||aw==nil{
        //return nil,awerr
        id,_:=AddDefaultWallet(o,obj.WalletAddr)
        aw,_=GetWallet(o,id)
    }
    ac,err := QueryCurrencyByWalletIdForUpdate(o,aw.Id)
    if err != nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }
    if len(ac)==0 {

        var tc Currency
        tc.Name = "New Exchange"
        tc.Description = obj.Description
        tc.Amount = obj.Amount
        tc.Attribute = ""
        tc.State = 1
        tc.SourceBank = ""
        tc.WalletId = aw.Id
        tc.PreCurrencyId = 0
        tc.UtxoTxid = ""
        tc.UtxoVoutIndex = 0
        tc.UtxoSingature = ""
        tc.UtxoAddress = ""
        tc.UtxoMessage = ""
        tc.UtxoValue = obj.Amount
        id,err:=AddCurrency(o,&tc)
        fmt.Println("add currency:",tc.WalletId,tc.Amount,id)
        if err != nil{
            return nil,err
        }
        dest_currency_id = id
    }else{
        dest_currency_id = ac[0].Id
        dest_amount := ac[0].Amount
        var tc Currency
        tc.Id=dest_currency_id
        tc.Amount=obj.Amount+dest_amount
        UpdateCurrency(o,tc.Id,&tc)
    }
    //need do utxo, remove old, add new currency

    //    
    //need add currency circulating later,and set invalid currency

    //

    //update wallet balance
    sql := "UPDATE wallet SET balance = balance %s ? WHERE %s=?"
    _, uerr1 := o.Raw(fmt.Sprintf(sql,"+","id"), obj.Amount,aw.Id).Exec()
    if uerr1!=nil {
        logger.Log.Informational(fmt.Sprintf("对象Wallet修改记录为:%d, 错误:%s",aw.Id,uerr1.Error()))
        return nil,common.MakeFromError(common.CodeDBUpdateError,uerr1)
    }
    obj.BankTransactionType=2//取钱
    obj.State=2//事务完成
    obj.CurrencyId = dest_currency_id
    
    BankTransactionormObj,_ := transferBankTransaction(obj)
    id, _ := o.Insert(BankTransactionormObj)
    logger.Log.Informational(fmt.Sprintf("Exchange AddBankTransaction: %d Success",id))

    ormObj,_ := transferExchangeToTransactionHistory(obj)
    hid, _ := o.Insert(ormObj)
    logger.Log.Informational(fmt.Sprintf("AddBankTransactionHistory: %d Success",hid))

    return GetBankTransaction(o,int64(id))
}
