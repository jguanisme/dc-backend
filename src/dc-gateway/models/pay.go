package models

import (
    "errors"
    "fmt"
    "time"
    "dc-gateway/logger"
    "dc-gateway/common"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"

    "github.com/astaxie/beego/orm"
)

const(
    _ = iota
    PayTransactionStateStart
    PayTransactionStateQuery
    PayTransactionStateSuccess
    PayTransactionStateFailed
)

const(
    _ = iota
    PayTransactionAddressTypeReceipt
    PayTransactionAddressTypeApply
)


//for old interface
type Pay struct {
    Id int64  `json:"Id,omitempty"`
    Name string `json:"Name,omitempty"`
    Description string `json:"Description,mitempty"`
    Amount int64 `json:"Amount,omitempty"`
    Attributes string `json:"Attributes,omitempty"`
    Signature string `json:"Signature,omitempty"`
    //1 开始付钱 未通知, 2 以查询 未付款, 3 交易完成
    State int64  `json:"State,omitempty"`
    OrderId string  `json:"OrderId,omitempty"`
    Created time.Time `json:"Created,omitempty"`
    Updated time.Time `json:"Updated,omitempty"`
    RelatedTransId int64 `json:"RelatedTransId,omitempty"`

    ApplyWalletAddr string `json:"ApplyWalletAddr,omitempty"`
    ReceiptWalletAddr string `json:"ReceiptWalletAddr,omitempty"`
    DeviceSn string `json:"DeviceSn,omitempty"`

    UtxoTxid string `json:"UtxoTxid,omitempty"`
    UtxoTime int64 `json:"UtxoTime,omitempty"`
    UtxoVersion int64 `json:"UtxoVersion,omitempty"`
    UtxoServerSignature string `json:"UtxoServerSignature,omitempty"`
    UtxoVin string `json:"UtxoVin,omitempty"`
    UtxoVout string `json:"UtxoVout,omitempty"`

    UtxoPayTx string `json:"UtxoPayTx,omitempty"`//json of tx signed by wallet
}

func transferOrmPay(ormObj *orms.Transaction) *Pay {
    obj := new(Pay)
    obj.Id = ormObj.Id
    obj.Name = ormObj.Name
    obj.Description = ormObj.Description
    obj.Amount = ormObj.Amount
    obj.Attributes = ormObj.Attributes
    obj.Signature = ormObj.Signature
    obj.State = ormObj.State
    obj.OrderId = ormObj.OrderId
    obj.Created = ormObj.Created
    obj.Updated = ormObj.Updated
    obj.RelatedTransId = ormObj.RelatedTransId
    obj.UtxoTxid = ormObj.UtxoTxid
    obj.UtxoTime = ormObj.UtxoTime
    obj.UtxoVersion = ormObj.UtxoVersion
    obj.UtxoServerSignature = ormObj.UtxoServerSignature
    obj.UtxoVin = ormObj.UtxoVin    
    obj.UtxoVout = ormObj.UtxoVout
    obj.ApplyWalletAddr = ormObj.ApplyWalletAddr
    obj.ReceiptWalletAddr = ormObj.ReceiptWalletAddr
    obj.DeviceSn = ormObj.DeviceSn
    return obj
}

func transferPay(obj *Pay) (*orms.Transaction,*[]string){
    f := []string{}
    ormObj := new(orms.Transaction)
    ormObj.Id = obj.Id
    if obj.Name != "" {
        ormObj.Name = obj.Name
        f=append(f,"Name")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.Amount > 0 {
        ormObj.Amount = obj.Amount
        f=append(f,"Amount")
    }     
    if obj.Attributes != "" {
        ormObj.Attributes = obj.Attributes
        f=append(f,"Attributes")
    }
    if obj.Signature != "" {
        ormObj.Signature = obj.Signature
        f=append(f,"Signature")
    }
    if obj.OrderId != "" {
        ormObj.OrderId = obj.OrderId
        f=append(f,"OrderId")
    }
    if obj.State > 0 {
        ormObj.State = obj.State
        f=append(f,"State")
    }  
    if obj.RelatedTransId != 0 {
        ormObj.RelatedTransId = obj.RelatedTransId
        f=append(f,"RelatedTransId")
    }
    if obj.ApplyWalletAddr != "" {
        ormObj.ApplyWalletAddr = obj.ApplyWalletAddr
        f=append(f,"ApplyWalletAddr")
    }
    if obj.ReceiptWalletAddr != "" {
        ormObj.ReceiptWalletAddr = obj.ReceiptWalletAddr
        f=append(f,"ReceiptWalletAddr")
    }
    if obj.DeviceSn != "" {
        ormObj.DeviceSn = obj.DeviceSn
        f=append(f,"DeviceSn")
    }
    if obj.UtxoTxid != "" {
        ormObj.UtxoTxid = obj.UtxoTxid
        f=append(f,"UtxoTxid")
    }
    if obj.UtxoTime > 0 {
        ormObj.UtxoTime = obj.UtxoTime
        f=append(f,"UtxoTime")
    }  
    if obj.UtxoVersion > 0 {
        ormObj.UtxoVersion = obj.UtxoVersion
        f=append(f,"UtxoVersion")
    }    
    if obj.UtxoServerSignature != "" {
        ormObj.UtxoServerSignature = obj.UtxoServerSignature
        f=append(f,"UtxoServerSignature")
    }
    if obj.UtxoVin != "" {
        ormObj.UtxoVin = obj.UtxoVin
        f=append(f,"UtxoVin")
    }
    if obj.UtxoVout != "" {
        ormObj.UtxoVout = obj.UtxoVout
        f=append(f,"UtxoVout")
    }

    //if obj.Created != "" {
        //ormObj.Created = obj.Created
    //}

    return ormObj,&f
}

func GetPay(o orm.Ormer, id int64) (ret *Pay, e error) {
    ormObj := orms.Transaction{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllPay No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return transferOrmPay(&ormObj),nil
}

func GetAllPay(o orm.Ormer) ([]*Pay, error){
    var ormObj []*orms.Transaction
    _, err := o.QueryTable("transaction").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllPay No Rows")
        // return nil,errors.New("查询不到")
    } 

    var obj []*Pay
    for _,v := range ormObj{
        obj=append(obj,transferOrmPay(v))
    }

    return obj,nil
}

func QueryPay(o orm.Ormer, qm *common.QueryParam) ([]*Pay, error){
    sqlMode := int64(common.QueryModePrepare)
    
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)
    
    // GetPagedSql Param    table,  conditions, default orderby
    sql,_ := qm.GetPagedSql("transaction", tConds,     "id desc")

    var ormObj []*orms.Transaction
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*Pay
    for _,v := range ormObj{
        obj=append(obj,transferOrmPay(v))
    }
    return obj,nil
}

func QueryPayByWalletAddr(o orm.Ormer, ApplyWalletAddr string, ReceiptWalletAddr string) ([]*Pay, error){
    var ormObj []*orms.Transaction

    sql := "SELECT * FROM transaction WHERE %s=? and %s=? ORDER BY created DESC"
    _, err := o.Raw(fmt.Sprintf(sql,"apply_wallet_addr","receipt_wallet_addr"), ApplyWalletAddr,ReceiptWalletAddr).QueryRows(&ormObj)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }
    var obj []*Pay
    for _,v := range ormObj{
        obj=append(obj,transferOrmPay(v))
    }

    return obj,nil
}

func QueryPayByOrderId(o orm.Ormer, orderId string, state int64) ([]*Pay, error){
    var ormObj []*orms.Transaction

    sql := "SELECT * FROM transaction WHERE %s=? AND %s=? ORDER BY created DESC"
    _, err := o.Raw(fmt.Sprintf(sql,"order_id","state"), orderId,state).QueryRows(&ormObj)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }
    var obj []*Pay
    for _,v := range ormObj{
        obj=append(obj,transferOrmPay(v))
    }

    return obj,nil
}

// Need transactions
func AddPay(o orm.Ormer, obj *Pay) (int64,error) {
    ormObj,_ := transferPay(obj)
    if ormObj.OrderId==""{
        ormObj.OrderId = mc.GenOrderId("MPay")
    }
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddPay: %d Success",id))
    return int64(id),err
}

func DeletePay(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.Transaction{Id: id}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeletePay: %d Success",id))
    }
}

func UpdatePay(o orm.Ormer, id int64, obj *Pay) (*Pay, error) {
    ormObj,feilds := transferPay(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdatePay: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdatePay: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj,nil
}


func transferPayToTransactionHistory(obj *Pay, t int64) (*orms.TransactionHistory,*[]string){
    f := []string{}
    ormObj := new(orms.TransactionHistory)
    //ormObj.Id = obj.Id
    if t > 0 {
        ormObj.TransactionType = t
        f=append(f,"TransactionType")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.Amount >= 0 {
        ormObj.Amount = obj.Amount
        f=append(f,"Amount")
    }     
    if obj.Attributes != "" {
        ormObj.Attributes = obj.Attributes
        f=append(f,"Attributes")
    }
    if obj.ApplyWalletAddr != "" {
        ormObj.InAddr = obj.ApplyWalletAddr
        f=append(f,"InAddr")
    }
    if obj.ReceiptWalletAddr != "" {
        ormObj.OutAddr = obj.ReceiptWalletAddr
        f=append(f,"OutAddr")
    }
    if obj.State > 0 {
        ormObj.State = obj.State
        f=append(f,"State")
    }
    if !obj.Created.IsZero() {
        ormObj.TransactionTime = obj.Created.Format(common.DividedTimeFormat)
        f=append(f,"TransactionTime")
    }
    return ormObj,&f
}

func AddPayTransactionHistory(o orm.Ormer, obj *Pay)( uint64, error) {
    h,_ := transferPayToTransactionHistory(obj,3)//3转账
    hid, err := o.Insert(h)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddBankTransactionHistory: %d Success",hid))
    return uint64(hid),err
}



func pay(o orm.Ormer, obj *Pay) (*Pay, error) {
    aw,awerr:=GetWalletByUtxoAddress(o,obj.ApplyWalletAddr)
    if awerr!=nil||aw==nil{
        //return nil,awerr
        id,_:=AddDefaultWallet(o,obj.ApplyWalletAddr)
        aw,_=GetWallet(o,id)
    }
    ac,_ := QueryCurrencyByWalletIdForUpdate(o,aw.Id)
    if ac == nil||len(ac)==0{
        return nil, common.MakeError(common.CodeNoCurrencyToPayError,"No Currency of Wallet:"+obj.ApplyWalletAddr)
    }
    rw,rwerr:=GetWalletByUtxoAddress(o,obj.ReceiptWalletAddr)
    if rwerr!=nil||rw==nil{
        //return nil,awerr
        id,_:=AddDefaultWallet(o,obj.ReceiptWalletAddr)
        rw,_=GetWallet(o,id)
    }
    rc,_ := QueryCurrencyByWalletIdForUpdate(o,rw.Id)    
    // if rc == nil||len(rc)==0{
    //     return nil,errors.New("No Currency")
    // }
    m := make(map[int64]int64)
    amount := obj.Amount
    for _,v :=range ac{
        //fmt.Println("pay test:",amount,v.Amount,v.Id)
        if amount <= v.Amount {
            m[v.Id]=v.Amount - amount
            amount -= v.Amount
            break
        }
        
        m[v.Id] = 0
        amount -= v.Amount
    }    
    
    if amount > 0{
        return nil,errors.New("没有足够的金额")
    }
    //need do utxo, remove old, add new currency

    for k,v :=range m{
        var tc Currency
        tc.Id=k
        tc.Amount=v
        if v==0{
            tc.State=104//已作废，或者待删除
        }
        UpdateCurrency(o,tc.Id,&tc)
    }
    //    
    //need add currency circulating later,and set invalid currency

    //
    if len(rc)==0 {
        var tc Currency
        tc.Name = "New Pay"
        tc.Description = obj.Description
        tc.Amount = obj.Amount
        tc.Attribute = ""
        tc.State = 1
        tc.SourceBank = ""
        tc.WalletId = rw.Id
        tc.PreCurrencyId = 0
        tc.UtxoTxid = obj.UtxoTxid
        tc.UtxoVoutIndex = 0
        tc.UtxoSingature = ""
        tc.UtxoAddress = ""
        tc.UtxoMessage = ""
        tc.UtxoValue = obj.Amount
        id,err:=AddCurrency(o,&tc)
        fmt.Println("[pay.go] pay,add currency:",tc.WalletId,tc.Amount,id)
        if err != nil{
            return nil, common.MakeFromError(common.CodeDBInsertError,err)
        }
    }else{
        currencyId := rc[0].Id
        sql := "UPDATE currency SET amount = amount %s ? WHERE %s=?"
        _, uerr1 := o.Raw(fmt.Sprintf(sql,"+","id"), obj.Amount,currencyId).Exec()
        if uerr1!=nil {
            logger.Log.Informational(fmt.Sprintf("对象Currency修改记录为:%d, 错误:%s",currencyId,uerr1.Error()))
            return nil,common.MakeFromDBError(common.CodeDBUpdateError,uerr1)
        }
    }

    
    //update wallet balance
    sql := "UPDATE wallet SET balance = balance %s ? WHERE %s=?"
    _, uerr1 := o.Raw(fmt.Sprintf(sql,"+","id"), obj.Amount,rw.Id).Exec()
    if uerr1!=nil {
        logger.Log.Informational(fmt.Sprintf("对象Wallet修改记录为:%d, 错误:%s",rw.Id,uerr1.Error()))
        return nil,common.MakeFromError(common.CodeDBUpdateError,uerr1)
    }
    _, uerr2 := o.Raw(fmt.Sprintf(sql,"-","id"), obj.Amount,aw.Id).Exec()
    if uerr2!=nil {
        logger.Log.Informational(fmt.Sprintf("对象Wallet修改记录为:%d, 错误:%s",aw.Id,uerr2.Error()))
        return nil,common.MakeFromError(common.CodeDBUpdateError,uerr2)
    }


    if obj.Id > 0 {
        obj.State = PayTransactionStateSuccess
        ormObj,feilds := transferPay(obj)
        ormObj.Id = obj.Id
        ormObj.Updated = time.Now()
        *feilds=append(*feilds,"Updated")
        _, e := o.Update(ormObj,*feilds...);
        if  e != nil {
            logger.Log.Informational(fmt.Sprintf("对象修改记录为:%d, 错误:%s",obj.Id,e.Error()))
            return nil,common.MakeFromError(common.CodeDBUpdateError,e)
        }

        h,_ := transferPayToTransactionHistory(obj,1)
        hid, err := o.Insert(h)
        if err!=nil{
            return nil, common.MakeFromError(common.CodeDBInsertError,err)
        }
        logger.Log.Informational(fmt.Sprintf("AddBankTransactionHistory: %d Success",hid))

        return obj,nil
    }else{
        obj.State = PayTransactionStateSuccess
        id,e := AddPay(o,obj)
        if e != nil{
            logger.Log.Informational(fmt.Sprintf("对象修改记录为:%d, 错误:%s",obj.Id,e.Error()))
            return nil,e
        }
        opay,_ := GetPay(o,id)
        h,_ := transferPayToTransactionHistory(opay,2)
        hid, err := o.Insert(h)
        if err!=nil{
            return nil, common.MakeFromError(common.CodeDBInsertError,err)
        }
        logger.Log.Informational(fmt.Sprintf("AddPayTransactionHistory: %d Success",hid))

        return opay,err
    }
}

func TransferPayTx(o orm.Ormer, obj *Pay) (*Pay, error) {
    if obj.Id>0{
        return nil,common.MakeError(common.CodeTxAlreadyExistsError,"交易已经存在")
    }
    return pay(o,obj)
}


func CreatePayTx(o orm.Ormer, obj *Pay) (int64, error) {
    var exists []*orms.Transaction
    sql := "SELECT * FROM transaction WHERE apply_wallet_addr=? AND receipt_wallet_addr=? AND state=? ORDER BY created DESC LIMIT 1"
    _, err := o.Raw(sql, obj.ApplyWalletAddr,obj.ReceiptWalletAddr,2).QueryRows(&exists)
    if err == nil {
        //fmt.Println("GetTransactionByAccountIdAndState2 nums: ", num)
    }
    if len(exists)>0{
        for _,v:=range(exists){
            DeletePay(o,v.Id)
        }
    }

    ormObj,_ := transferPay(obj)
    ormObj.State=PayTransactionStateQuery
    if ormObj.OrderId==""{
        ormObj.OrderId = mc.GenOrderId("MPay")
    }
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("CreatePayTx: %d Success",id))
    return int64(id),err
}

func QueryPayTx(o orm.Ormer, walletAddr string, state int64, t int64, sn string) ([]*Pay, error) {
    var ormObj []*orms.Transaction
    f :=  "apply_wallet_addr"
    if t==1 {
        f = "receipt_wallet_addr"
    }
    if sn!=""{
        sql := "SELECT * FROM transaction WHERE %s=? AND state=? AND device_sn=? ORDER BY created DESC LIMIT 1"
        _, err := o.Raw(fmt.Sprintf(sql,f), walletAddr,state,sn).QueryRows(&ormObj)
        if err == nil {
            //fmt.Println("GetTransactionByAccountIdAndState2 nums: ", num)
        }
    }else{
        sql := "SELECT * FROM transaction WHERE %s=? AND state=? ORDER BY created DESC LIMIT 1"
        _, err := o.Raw(fmt.Sprintf(sql,f), walletAddr,state).QueryRows(&ormObj)
        if err == nil {
            //fmt.Println("GetTransactionByAccountIdAndState2 nums: ", num)
        } 
    }

    var obj []*Pay
    for _,v := range ormObj{
        if len(obj) == 0 {            
            obj=append(obj,transferOrmPay(v))
        }else{
        // if 超时 set state = 3

        }
    }
    return obj,nil
}

func ConfirmPayTx(o orm.Ormer, obj *Pay) (*Pay,error) {
    if obj.Id>0{
        t,err := GetPay(o,obj.Id)
        if err != nil{
            return nil,err
        }
        if t.State > PayTransactionStateQuery{
            return nil,common.MakeError(common.CodeTxAlreadyCompletedError,"已经交易过了")
        }
        return pay(o,t)
    }else{
        return nil,common.MakeError(common.CodeTxNotExistsError,"交易未创建")
    }
}
