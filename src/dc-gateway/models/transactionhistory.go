package models

import (
    "fmt"
    "time"
    "dc-gateway/logger"
    "dc-gateway/common"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"

    "github.com/astaxie/beego/orm"
)

type TransactionHistory struct {
    Id int64 `json:"Id,omitempty"`
    //1 存钱，2取钱，3转账，4收钱
    TransactionType int64 `json:"TransactionType,omitempty"`
    Description string `json:"Description,omitempty"`
    Amount int64 `json:"Amount,omitempty"`
    Attributes string `json:"Attributes,omitempty"`
    InName string `json:"InName,omitempty"`
    InAddr string `json:"InAddr,omitempty"`
    OutName string `json:"OutName,omitempty"`
    OutAddr string `json:"OutAddr,omitempty"`
    OrderId string `json:"OrderId,omitempty"`
    State int64 `json:"State,omitempty"`
    WalletType int64 `json:"WalletType,omitempty"`
    ZoneName string `json:"ZoneName,omitempty"`
    TransactionSeq string `json:"TransactionSeq,omitempty"`
    TransactionTime string `json:"TransactionTime,omitempty"`//交易发生时间
    Created time.Time `json:"Created,omitempty"`//创建时间
}

func transferOrmTransactionHistory(ormObj *orms.TransactionHistory) *TransactionHistory{
    obj := new(TransactionHistory)
    obj.Id = ormObj.Id
    obj.TransactionType = ormObj.TransactionType
    obj.Description = ormObj.Description
    obj.Amount = ormObj.Amount
    obj.Attributes = ormObj.Attributes
    obj.InName = ormObj.InName
    obj.InAddr = ormObj.InAddr
    obj.OutName = ormObj.OutName
    obj.OutAddr = ormObj.OutAddr
    obj.State = ormObj.State
    obj.WalletType = ormObj.WalletType
    obj.ZoneName = ormObj.ZoneName
    obj.TransactionSeq = ormObj.TransactionSeq
    obj.TransactionTime = ormObj.TransactionTime
    obj.Created = ormObj.Created

    return obj
}

func transferTransactionHistory(obj *TransactionHistory) (*orms.TransactionHistory,*[]string){
    f := []string{}
    ormObj := new(orms.TransactionHistory)
    ormObj.Id = obj.Id
    if obj.TransactionType > 0 {
        ormObj.TransactionType = obj.TransactionType
        f=append(f,"TransactionType")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.Amount >= 0 {
        ormObj.Amount = obj.Amount
        f=append(f,"Amount")
    }     
    if obj.Attributes != "" {
        ormObj.Attributes = obj.Attributes
        f=append(f,"Attributes")
    }
    if obj.InName != "" {
        ormObj.InName = obj.InName
        f=append(f,"InName")
    }
    if obj.InAddr != "" {
        ormObj.InAddr = obj.InAddr
        f=append(f,"InAddr")
    }
    if obj.OutName != "" {
        ormObj.OutName = obj.OutName
        f=append(f,"OutName")
    }
    if obj.OutAddr != "" {
        ormObj.OutAddr = obj.OutAddr
        f=append(f,"OutAddr")
    }
    if obj.State > 0 {
        ormObj.State = obj.State
        f=append(f,"State")
    }  
    if obj.WalletType > 0 {
        ormObj.WalletType = obj.WalletType
        f=append(f,"WalletType")
    }    
    if obj.ZoneName != "" {
        ormObj.ZoneName = obj.ZoneName
        f=append(f,"ZoneName")
    }
    if obj.TransactionSeq != "" {
        ormObj.TransactionSeq = obj.TransactionSeq
        f=append(f,"TransactionSeq")
    }
    
    if obj.TransactionTime != "" {
        ormObj.TransactionTime = obj.TransactionTime
        f=append(f,"TransactionTime")
    }


    //if obj.Created != "" {
        //ormObj.Created = obj.Created
    //}

    return ormObj,&f
}

func GetTransactionHistory(o orm.Ormer, id int64) (ret *TransactionHistory, e error) {
    ormObj := orms.TransactionHistory{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllTransactionHistory No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return transferOrmTransactionHistory(&ormObj),nil
}

func GetAllTransactionHistory(o orm.Ormer) ([]*TransactionHistory, error){
    var ormObj []*orms.TransactionHistory
    _, err := o.QueryTable("transaction_history").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllTransactionHistory No Rows")
        // return nil,errors.New("查询不到")
    } 

    var obj []*TransactionHistory
    for _,v := range ormObj{
        obj=append(obj,transferOrmTransactionHistory(v))
    }

    return obj,nil
}
func QueryTransactionHistoryByWalletAddr(o orm.Ormer, qm *common.QueryParam) ([]*TransactionHistory, error){
    sqlMode := int64(1) //1 sql mode, 2 prepare mode
    tConds := []string{}

    addr,err1 := qm.GetString("utxoAddr")
    if err1 == nil { 
        tConds = qm.GetStringConds(tConds,sqlMode,"in_addr",addr)
        tConds = qm.GetStringConds(tConds,sqlMode,"out_addr",addr)
    }

    // GetPagedSql Param                     table,  conditions, default orderby
    sql,_ := qm.GetOrPagedSql("transaction_history", tConds,     "id desc")

    logger.Log.Debug(fmt.Sprintf("QueryTransactionHistoryByWalletAddr sql: %s",sql))

    var ormObj []*orms.TransactionHistory
    var num int64
    var err error
    if sqlMode == 2 {
        num, err = o.Raw(sql, addr,addr).QueryRows(&ormObj)
    }
    if sqlMode == 1 {
        num, err = o.Raw(sql).QueryRows(&ormObj)
    }
    if err == nil {
        logger.Log.Informational(fmt.Sprintf("QueryTransactionHistoryByWalletAddr found results num: %d",num))
    }else{
        logger.Log.Warn(fmt.Sprintf("QueryTransactionHistoryByWalletAddr Error: %s",err.Error()))
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*TransactionHistory
    for _,v := range ormObj{
        obj=append(obj,transferOrmTransactionHistory(v))
    }

    return obj,nil
}


func QueryTransactionHistory(o orm.Ormer, qm *common.QueryParam) ([]*TransactionHistory, error){
    sqlMode := int64(common.QueryModePrepare)
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)

    // GetPagedSql Param    table,  conditions, default orderby
    sql,_ := qm.GetPagedSql("transaction_history", tConds,     "id desc")

    var ormObj []*orms.TransactionHistory
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*TransactionHistory
    for _,v := range ormObj{
        obj=append(obj,transferOrmTransactionHistory(v))
    }

    return obj,nil
}

// Need transactions
func AddTransactionHistory(o orm.Ormer, obj *TransactionHistory) (int64,error) {
    ormObj,_ := transferTransactionHistory(obj)
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddTransactionHistory: %d Success",id))
    return int64(id),err
}

func DeleteTransactionHistory(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.Transaction{Id: id}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeleteTransactionHistory: %d Success",id))
    }
}

func UpdateTransactionHistory(o orm.Ormer, id int64, obj *TransactionHistory) (a *TransactionHistory, err error) {
    ormObj,feilds := transferTransactionHistory(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateTransactionHistory: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdateTransactionHistory: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj,nil
}
