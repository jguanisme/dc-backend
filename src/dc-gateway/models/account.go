package models

import (
	"fmt"
	"time"

    "dc-gateway/common"
	"dc-gateway/logger"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"

	"github.com/astaxie/beego/orm"
)

type FastRegArgs struct {
    AcctName string `json:"AcctName,omitempty"`
    AcctPasswd string `json:"AcctPasswd,omitempty"`
    Salt string `json:"Salt,omitempty"`
}

type AcctLoginArgs struct {
    AcctName string `json:"AcctName,omitempty"`
    AcctPasswd string `json:"AcctPasswd,omitempty"`
    Salt string `json:"Salt,omitempty"`
}

type ModAcctPwdArgs struct {
    AcctId int64 `json:"AcctId,omitempty"`
    AcctName string `json:"AcctName,omitempty"`
    OldPwd string `json:"OldPwd,omitempty"`
    NewPwd string `json:"NewPwd,omitempty"`
    NewSalt string `json:"NewSalt,omitempty"`
    OldSalt string `json:"OldSalt,omitempty"`
}

type Account struct {
    Id int64 `json:"Id,omitempty"`
    ZoneId int64 `json:"ZoneId,omitempty"`
    Name string `json:"Name,omitempty"`
    Salt string `json:"Salt,omitempty"`
    LoginPwd string `json:"LoginPwd,omitempty"`
    TradePwd string `json:"TradePwd,omitempty"`
    Description string `json:"Description,omitempty"`
    Created time.Time `json:"Created,omitempty"`
    UserId int64 `json:"UserId,omitempty"`
    Type string `json:"Type,omitempty"`
    Stat string `json:"Stat,omitempty"`
    TotalAmount int64 `json:"TotalAmount,omitempty"`
    CashAmount int64 `json:"CashAmount,omitempty"`
    UncashAmount int64 `json:"UncashAmount,omitempty"`
    FreezeCashAmount int64 `json:"FreezeCashAmount,omitempty"`
    FreezeUncashAmount int64 `json:"FreezeUncashAmount,omitempty"`
    LastTermJixiDate time.Time `json:"LastTermJixiDate,omitempty"`
    LastUpdateTime time.Time `json:"LastUpdateTime,omitempty"`
    LastTermAmount int64 `json:"LastTermAmount,omitempty"`
}

func transferOrmAccount(ormObj *orms.Account) *Account {
    obj := new(Account)
    obj.Id = ormObj.Id
    obj.Name = ormObj.Name
    if ormObj.User!=nil{
        obj.UserId = ormObj.User.Id
    }
    if ormObj.Zone!=nil{
        obj.ZoneId = ormObj.Zone.Id
    }
    obj.Salt = ormObj.Salt
    obj.LoginPwd = ormObj.LoginPwd
    obj.TradePwd = ormObj.TradePwd
    obj.Description = ormObj.Description
    obj.Created = ormObj.Created
    obj.Type = ormObj.Type
    obj.Stat = ormObj.Stat
    obj.TotalAmount = ormObj.TotalAmount
    obj.CashAmount = ormObj.CashAmount
    obj.UncashAmount = ormObj.UncashAmount
    obj.FreezeCashAmount = ormObj.FreezeCashAmount
    obj.FreezeUncashAmount = ormObj.FreezeUncashAmount
    obj.LastTermJixiDate = ormObj.LastTermJixiDate
    obj.LastUpdateTime = ormObj.LastUpdateTime
    obj.LastTermAmount = ormObj.LastTermAmount
    return obj
}

func transferAccount(obj *Account) (*orms.Account, *[]string) {
    f := []string{}
    ormObj := new(orms.Account)
    ormObj.Id = obj.Id
    if obj.Name != "" {
        ormObj.Name = obj.Name
        f = append(f, "Name")
    }
    if obj.ZoneId != 0 {
        ormObj.Zone = new(orms.Zone)
        ormObj.Zone.Id = obj.ZoneId
        f = append(f, "ZoneId")
    }
    if obj.Salt != "" {
        ormObj.Salt = obj.Salt
        f = append(f, "Salt")
    }
    if obj.LoginPwd != "" {
        ormObj.LoginPwd = obj.LoginPwd
        f = append(f, "LoginPwd")
    }
    if obj.TradePwd != "" {
        ormObj.TradePwd = obj.TradePwd
        f = append(f, "TradePwd")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f = append(f, "Description")
    }
    if obj.UserId != 0 {
        ormObj.User = new(orms.User)
        ormObj.User.Id = obj.UserId
        f = append(f, "UserId")
    }
    if obj.Type != "" {
        ormObj.Type = obj.Type
        f = append(f, "Type")
    }
    if obj.TotalAmount > 0 {
        ormObj.TotalAmount = obj.TotalAmount
        f = append(f, "TotalAmount")
    }
    if obj.CashAmount > 0 {
        ormObj.CashAmount = obj.CashAmount
        f = append(f, "CashAmount")
    }
    if obj.UncashAmount > 0 {
        ormObj.UncashAmount = obj.UncashAmount
        f = append(f, "UncashAmount")
    }
    if obj.FreezeCashAmount > 0 {
        ormObj.FreezeCashAmount = obj.FreezeCashAmount
        f = append(f, "FreezeCashAmount")
    }
    if obj.FreezeUncashAmount > 0 {
        ormObj.FreezeUncashAmount = obj.FreezeUncashAmount
        f = append(f, "FreezeUncashAmount")
    }
    if obj.Stat != "" {
        ormObj.Stat = obj.Stat
        f = append(f, "Stat")
    }
    if obj.LastTermAmount > 0 {
        ormObj.LastTermAmount = obj.LastTermAmount
        f = append(f, "LastTermAmount")
    }
/*
    if obj.LastTermJixiDate.IsZero() == true {
        ormObj.LastTermJixiDate = obj.LastTermJixiDate
        f = append(f, "LastTermJixiDate")
    }
    if obj.LastUpdateTime.IsZero() == true {
        ormObj.LastUpdateTime = obj.LastUpdateTime
        f = append(f, "LastUpdateTime")
    }
    if obj.CustName != "" {
        ormObj.CustName = obj.CustName
        f = append(f, "CustName")
    }
    if obj.Created.IsZero() == true {
        ormObj.Created = obj.Created
        f = append(f, "Created")
    }
*/
    return ormObj, &f
}

func GetAccount(o orm.Ormer, id int) (ret *Account, e error) {
    ormObj := orms.Account{Id: int64(id)}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Info("GetAllAccount No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return transferOrmAccount(&ormObj),nil
}

func GetAllAccount(o orm.Ormer) ([]*Account, error){
    var ormObj []*orms.Account
    _, err := o.QueryTable("account").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Info("GetAllAccount No Rows")
//      return nil,errors.New("查询不到")
    } 

    var obj []*Account
    for _,v := range ormObj{
        obj=append(obj, transferOrmAccount(v))
    }

    return obj, nil
}

func QueryAccount(o orm.Ormer, qm *common.QueryParam) ([]*Account, error){
    sqlMode := int64(common.QueryModePrepare)
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)

    // GetPagedSql Param table, conditions, default orderby
    sql, _ := qm.GetPagedSql("account", tConds, "id desc")

    var ormObj []*orms.Account
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*Account
    for _, v := range ormObj {
        obj = append(obj, transferOrmAccount(v))
    }

    return obj, nil
}

// Need transactions
func AddAccount(o orm.Ormer, obj *Account) (int, error) {
    ormObj, _ := transferAccount(obj)
    id, err := o.Insert(ormObj)
    if err != nil {
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
//    logger.Info(fmt.Sprintf("AddAccount: %d Success", id))
    return int(id), nil
}

func DeleteAccount(o orm.Ormer, id int) {
    if _, err := o.Delete(&orms.Account{Id: int64(id)}); err == nil {
        logger.Info(fmt.Sprintf("DeleteAccount: %d Success", id))
    }
}

func UpdateAccount(o orm.Ormer, id int, obj *Account) (a *Account, err error) {
    ormObj, feilds := transferAccount(obj)
    ormObj.Id = int64(id)
    if _, err := o.Update(ormObj, *feilds...); err != nil {
        logger.Info(fmt.Sprintf("UpdateAccount: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Info(fmt.Sprintf("UpdateAccount: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj, nil
}

func FastRegister(o orm.Ormer, args *FastRegArgs) (int64, error) {
    obj := orms.Account{Name: args.AcctName, LoginPwd: args.AcctPasswd, Salt: args.Salt, User: new(orms.User),Zone: new(orms.Zone)}
    if err := o.Read(&obj, "Name"); err != nil {
        if err == orm.ErrNoRows {
            id, err := o.Insert(&obj)
            if err != nil {
                return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
            }
            return id, nil
        }
        return 0, common.MakeFromError(common.CodeDBQueryError,err)
    }
    logger.Log.Error("fast register failed, account %s already exist.", args.AcctName)
    return obj.Id, common.MakeError(common.CodeDBAlreadyExistsError,fmt.Sprintf("account %s already exist", args.AcctName))
}

func AcctLogin(o orm.Ormer, args *AcctLoginArgs) (int64, error) {
    obj := orms.Account{Name: args.AcctName}
    if err := o.Read(&obj, "Name"); err != nil {
        if err == orm.ErrNoRows {
            logger.Log.Error("account login failed, account %s is not exist.", args.AcctName)
            return 0, common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("account %s is not exist", args.AcctName))
        }
        return 0, common.MakeFromError(common.CodeDBQueryError,err)
    }
    obj.LoginPwd = args.AcctPasswd
    obj.Salt = args.Salt
    if err := o.Read(&obj, "Name", "LoginPwd", "Salt"); err != nil {
        if err == orm.ErrNoRows {
            logger.Log.Error("account login failed, password is incorrect.")
            return 0, common.MakeError(common.CodeDBAlreadyExistsError,"login password error")
        }
        return 0, common.MakeFromError(common.CodeDBQueryError,err)
    }
    return obj.Id, nil
}

func ModAcctPwd(o orm.Ormer, args *ModAcctPwdArgs) (error) {
    obj := orms.Account{Id: args.AcctId}
    if err := o.Read(&obj); err != nil {
        if err == orm.ErrNoRows {
            logger.Log.Error("modify login password failed, account id %d is not exist.", args.AcctId)
            return common.MakeError(common.CodeDBAlreadyExistsError,fmt.Sprintf("account id %d not exist.", args.AcctId))
        }
        return common.MakeFromError(common.CodeDBQueryError,err)
    }
    obj.LoginPwd = args.OldPwd
    obj.Salt = args.OldSalt
    if err := o.Read(&obj, "Id", "LoginPwd", "Salt"); err != nil {
        if err == orm.ErrNoRows {
            logger.Log.Error("modify login password failed, old password is incorrect.")
            return common.MakeError(common.CodeDBAlreadyExistsError,"old password is incorrect.")
        }
        return common.MakeFromError(common.CodeDBQueryError,err)
    }
    obj.LoginPwd = args.NewPwd
    obj.Salt = args.NewSalt
    if _, err := o.Update(&obj, "LoginPwd", "Salt"); err != nil {
        return common.MakeFromError(common.CodeDBUpdateError,err)
    }
    return nil
}

func GetAcctsByUserId(o orm.Ormer, UserId int64) ([]*Account, error){
    var ormObj []*orms.Account
    if _, err := o.Raw("select * from account where user_id = ?", UserId).QueryRows(&ormObj); err != nil {
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }
    var obj []*Account
    for _, v := range ormObj {
        obj = append(obj, transferOrmAccount(v))
    }
    return obj, nil
}

type FillUsrInfoArgs struct {
    AcctId int64
    Name string
    Type string
    State string
    Score int64
    Grade int64
    Email string
    FixedPhone string
    MobilePhone string
    Address string
    PostCode string
    CertType string
    CertNo string
    RealNameAuthMark string
    MobileAuthMark string
    BizLicNo string
    TaxRegNo string
    CorpCertType string
    CorpCertNo string
    CorpName string
    RegCap int64
    CorpType string
    Industry string
    Turnover int64
    MonAmount int64
    CorpWebUrl string
}

func TransToOrmUsr(args *FillUsrInfoArgs, UsrId int64) (*orms.User, *[]string) {
    f := []string{}
    obj := new(orms.User)
    if UsrId > 0 {
        obj.Id = UsrId
    }
    if args.Name != "" {
        obj.Name = args.Name
        f = append(f, "Name")
    }
    if args.Type != "" {
        obj.Type = args.Type
        f = append(f, "Type")
    }
    if args.Score > 0 {
        obj.Score = args.Score
        f = append(f, "Score")
    }
    if args.Grade > 0 {
        obj.Grade = args.Grade
        f = append(f, "Grade")
    }
    if args.Email != "" {
        obj.Email = args.Email
        f = append(f, "Email")
    }
    if args.FixedPhone != "" {
        obj.FixedPhone = args.FixedPhone
        f = append(f, "FixedPhone")
    }
    if args.MobilePhone != "" {
        obj.MobilePhone = args.MobilePhone
        f = append(f, "MobilePhone")
    }
    if args.Address != "" {
        obj.Address = args.Address
        f = append(f, "Address")
    }
    if args.PostCode != "" {
        obj.PostCode = args.PostCode
        f = append(f, "PostCode")
    }
    if args.CertType != "" {
        obj.CertType = args.CertType
        f = append(f, "CertType")
    }
    if args.CertNo != "" {
        obj.CertNo = args.CertNo
        f = append(f, "CertNo")
    }
    if args.BizLicNo != "" {
        obj.BizLicNo = args.BizLicNo
        f = append(f, "BizLicNo")
    }
    if args.TaxRegNo != "" {
        obj.TaxRegNo = args.TaxRegNo
        f = append(f, "TaxRegNo")
    }
    if args.CorpCertType != "" {
        obj.CorpCertType = args.CorpCertType
        f = append(f, "CorpCertType")
    }
    if args.CorpCertNo != "" {
        obj.CorpCertNo = args.CorpCertNo
        f = append(f, "CorpCertNo")
    }
    if args.CorpName != "" {
        obj.CorpName = args.CorpName
        f = append(f, "CorpName")
    }
    if args.RegCap > 0 {
        obj.RegCap = args.RegCap
        f = append(f, "RegCap")
    }
    if args.CorpType != "" {
        obj.CorpType = args.CorpType
        f = append(f, "CorpType")
    }
    if args.Industry != "" {
        obj.Industry = args.Industry
        f = append(f, "Industry")
    }
    if args.Turnover > 0 {
        obj.Turnover = args.Turnover
        f = append(f, "Turnover")
    }
    if args.MonAmount > 0 {
        obj.MonAmount = args.MonAmount
        f = append(f, "MonAmount")
    }
    if args.CorpWebUrl != "" {
        obj.CorpWebUrl = args.CorpWebUrl
        f = append(f, "CorpWebUrl")
    }
    if args.RealNameAuthMark != "" {
       obj.RealNameAuthMark = args.RealNameAuthMark
       f = append(f, "RealNameAuthMark")
    }
    if args.MobileAuthMark != "" {
       obj.MobileAuthMark = args.MobileAuthMark
       f = append(f, "MobileAuthMark")
    }
    return obj, &f
}

type FillUsrInfoResp struct {
    UserId int64
}

func FillUsrInfo(o orm.Ormer, args *FillUsrInfoArgs) (*FillUsrInfoResp, error) {
    AcctOrmObj := orms.Account{Id: args.AcctId}
    resp := new(FillUsrInfoResp)
    if err := o.Read(&AcctOrmObj); err != nil {
        if err == orm.ErrNoRows {
            logger.Log.Error("account id %d is not exist.", args.AcctId)
            return nil, common.MakeError(common.CodeDBAlreadyExistsError,"Account id is not exist.")
        }
        logger.Log.Error("%s", err)
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }
    if 0 == AcctOrmObj.User.Id {
        UsrOrmObj, _ := TransToOrmUsr(args, 0)
        UserId, err := o.Insert(UsrOrmObj)
        if err != nil {
            logger.Log.Error("%s", err)
            return nil,common.MakeFromError(common.CodeDBInsertError,err)
        }
        AcctOrmObj.User.Id = UserId
        if _, err := o.Update(&AcctOrmObj, "user_id"); err != nil {
            logger.Log.Error("%s", err.Error())
            return nil,common.MakeFromError(common.CodeDBUpdateError,err)
        }
        resp.UserId = UserId
        return resp, nil
    }
    UsrOrmObj, feilds := TransToOrmUsr(args, AcctOrmObj.User.Id)
    if _, err := o.Update(UsrOrmObj, *feilds...); err != nil {
        logger.Log.Error("%s", err.Error())
        return nil,common.MakeFromError(common.CodeDBUpdateError,err)
    }
    resp.UserId = AcctOrmObj.User.Id
    return resp, nil
}
