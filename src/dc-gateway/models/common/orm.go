package common

import (
	"fmt"
    "time"
	"strings"

    "dc-gateway/conf"
    "dc-gateway/logger"
	cm "dc-gateway/common"

    "github.com/astaxie/beego/orm"
    _ "github.com/go-sql-driver/mysql"
)

func init() {
	orm.RegisterDriver("mysql", orm.DRMySQL)
//	orm.RegisterDataBase("default", "mysql", "chenglei:87cl*&CL@/dc?charset=utf8&loc=Local")
//	orm.RegisterDataBase("default", "mysql", "root:t&e$s^t)@/dcdb?charset=utf8&loc=Local")
    cfg:=conf.GetAppConfig()
    u:=cfg.DbUser
    p:=cfg.DbPassword
    url:=cfg.DbUrls
    n:=cfg.DbName
    dbs:=fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&loc=Local",u,p,url,n)
	orm.RegisterDataBase("default", "mysql",dbs)
	orm.Debug = (cfg.DbDebug==1)
    if orm.Debug{
        orm.DebugLog = orm.NewLog(logger.GetLoggerWriter())
    }
}

func OrmRunCmd() {
    orm.RunCommand()
}

func OrmSyncDB() {
	orm.Debug = true
	// 数据库别名
	name := "default"
	force := true
	verbose := true
	err := orm.RunSyncdb(name, force, verbose)
	if err != nil {
	    logger.DumpError(err)
	}
}

func CreateTrans() orm.Ormer {
	return orm.NewOrm()
}

func CreateTransDB(dname string) orm.Ormer {
	o := orm.NewOrm()
	o.Using(dname)
	return o
}
func BeginTrans(o orm.Ormer) {
	o.Begin()
}

func EndTrans(o orm.Ormer, in *error) {
	if in!=nil{
		e := *in
		if e!=nil{
            o.Rollback()
            return
		}		
	}
    o.Commit()
}

func QuerySql(o orm.Ormer,sql string, tVals []interface{}, ormObj interface{}, sqlMode int64) error{
	var num int64
    var err error

	switch(sqlMode){
    case 1:
        num, err = o.Raw(sql).QueryRows(ormObj)
    case 2:
        num, err = o.Raw(sql, tVals...).QueryRows(ormObj)
    default:
        num, err = o.Raw(sql, tVals...).QueryRows(ormObj)
    }

    if err != nil {
        logger.Warn(fmt.Sprintf("QuerySql: %s, Error: %s, Num: %d",sql,err.Error(),num))
        return err
	}
    //logger.Log.Debug(fmt.Sprintf("QuerySql: %s found results num: %d",sql,num))
    return nil
}


//============
func InsertInto(o orm.Ormer,tname string,z interface{})(int64,error){
    s:=cm.GetDefaultReflectSql()
    sql,v,e:=s.ReflectSqlInsert(tname,z)
    if e!=nil{
        return 0,e
    }
    fmt.Println(sql,v,len(v),e)
    f:=func(o orm.Ormer,sql string,v []interface{},obj interface{})(int64,error){
        res,err := o.Raw(sql,v...).Exec()
        if err!=nil{
            return 0,err
        }
        return res.LastInsertId()
    }
    return queryBase(o,tname,sql,v,nil,f)
}


func UpdateTo(o orm.Ormer,tname string,z interface{})(int64,error){
    s:=cm.GetDefaultReflectSql()
    sql,v,e:=s.ReflectSqlUpdateOne(tname,z)
    if e!=nil{
        return 0,e
    }
    f:=func(o orm.Ormer,sql string,v []interface{},obj interface{})(int64,error){
        res,err := o.Raw(sql,v...).Exec()
        if err!=nil{
            return 0,err
        }
        return res.RowsAffected()
    }
    return queryBase(o,tname,sql,v,nil,f) 
}


func DeleteTo(o orm.Ormer,tname string,id int64)(int64,error){    
    s:=cm.GetDefaultReflectSql()
    sql,v,e:=s.ReflectSqlDeleteOne(tname,id)
    if e!=nil{
        return 0,e
    }
    f:=func(o orm.Ormer,sql string,v []interface{},obj interface{})(int64,error){
        res,err := o.Raw(sql,v...).Exec()
        if err!=nil{
            return 0,err
        }
        return res.RowsAffected()
    }
    return queryBase(o,tname,sql,v,nil,f) 
}

func ReadFrom(o orm.Ormer,ormObj interface{},tname string,id int64)(error){
    sql,v,e:=cm.GetDefaultReflectSql().ReflectSqlSelectOne(tname,id)
    if e!=nil{
        return e
    }
    _,eqo:=QueryOne(o,tname,sql,v,ormObj)
    return eqo
}

func SelectCondOne(o orm.Ormer,obj interface{},t string,c *cm.ReflectSqlConds)(error){
    sql,v,e:=c.ReflectSqlSelectRows(t)
    if e!=nil{
        return e
    }
    _,eqo:=QueryOne(o,t,sql,v,obj)
    return eqo
}
func SelectCondMany(o orm.Ormer,obj interface{},t string,c *cm.ReflectSqlConds)(error){
    _,e:=SelectCondManyNum(o,obj,t,c)
    return e
}
func SelectCondManyNum(o orm.Ormer,obj interface{},t string,c *cm.ReflectSqlConds)(int64,error){
    sql,v,e:=c.ReflectSqlSelectRows(t)
    if e!=nil{
        return 0,e
    }
    return QueryMany(o,t,sql,v,obj)
}

type QueryHistoryParam struct{
    Tname string
    Stime string
    Etime string
}
type HistoryRowsFunc func()bool
func (p *QueryHistoryParam)SelectHistoryRows(o orm.Ormer,r interface{},
        f HistoryRowsFunc,c *cm.ReflectSqlConds)(interface{},error){

    if c==nil{
        return nil,cm.MakeError(cm.CodeDBQueryError,"Conditions are not set.")
    }

    st,err:=cm.HistoryParseTime(p.Stime)
    if err!=nil{
        return nil,err
    }
    et,err:=cm.HistoryParseTime(p.Stime)
    if err!=nil{
        return nil,err
    }
    
    //-3m need udpate with query.go
    m,_:=time.ParseDuration("-3m")
    c.AddBtw("Created",p.Stime,p.Etime)
    //4 is define as 1 year
    //4 x 3 = 12, 12m = 1y

    t:=int64(0)
    for i:=0;st.After(et)&&i<4;i++{
        t1,_:=cm.GetHistoryTableByTime(p.Tname,et)
        n,err:=SelectCondManyNum(o,r,t1,c)
        if err==nil{
            f()
        }
        if t>int64(c.P.Limit){
            break
        }
        
        t=t+n
        et=et.Add(m)
    }
    return r,nil
}

func CreateTableDivided(o orm.Ormer,tname string,tnameDivided string)(int64,error){	
    sql:=fmt.Sprintf("CREATE TABLE %s AS SELECT * FROM %s WHERE 1=2",tnameDivided,tname)
    res,err := o.Raw(sql).Exec()
    if err!=nil{
    	return 0,err
    }
    ai:=fmt.Sprintf("ALTER TABLE %s MODIFY COLUMN %s BIGINT(20) PRIMARY KEY AUTO_INCREMENT",tnameDivided,"id")
    res,err = o.Raw(ai).Exec()
    if err!=nil{
        return 0,err
    }
    //todo alter table add all unique conditions when copy a divided table

    //
    return res.RowsAffected()
}

func Tunecate(o orm.Ormer,tnameDivided string)(int64,error){ 
    sql:=fmt.Sprintf("TRUNCATE TABLE %s",tnameDivided)
    res,err := o.Raw(sql).Exec()
    if err!=nil{
        return 0,err
    }
    return res.RowsAffected()
}

func DropTable(o orm.Ormer,tnameDivided string)(int64,error){ 
    sql:=fmt.Sprintf("DROP TABLE %s",tnameDivided)
    res,err := o.Raw(sql).Exec()
    if err!=nil{
        return 0,err
    }
    return res.RowsAffected()
}

type queryCallback func(o orm.Ormer,s string,v []interface{},obj interface{})(int64,error)
func queryBase(o orm.Ormer,t string,s string,v []interface{},obj interface{},cb queryCallback)(int64,error){
    if cb!=nil{
        n,e:=cb(o,s,v,obj)
        if e!=nil{
            if strings.Split(e.Error(),":")[0]=="Error 1146"{
                logger.Warn(fmt.Sprintf("Insert Table:%s Error,No Such Table Need Create.",t))
                _,e=CreateTableDivided(o,cm.GetOriginalTable(t),t)
                if e==nil||strings.Split(e.Error(),":")[0]=="Error 1050"{
                    n,e=cb(o,s,v,obj)
                }
            }
            if e!=nil{
                logger.Warn(fmt.Sprintf("Sql: %s, Error: %s.",s,e.Error()))
            }
        }
        return n,e
    }
    return 0,cm.MakeError(-1,"Query Error.")
}
func QueryOne(o orm.Ormer,tname string,sql string,v []interface{},obj interface{})(int64,error){
    f:=func(o orm.Ormer,sql string,v []interface{},obj interface{})(int64,error){
        return 0,o.Raw(sql, v...).QueryRow(obj)
    }
    return queryBase(o,tname,sql,v,obj,f)
}

func QueryMany(o orm.Ormer,tname string,sql string,v []interface{},obj interface{})(int64,error){
    f:=func(o orm.Ormer,sql string,v []interface{},obj interface{})(int64,error){
        r,e:=o.Raw(sql, v...).QueryRows(obj)
        return r,e
    }
    return queryBase(o,tname,sql,v,obj,f)
}