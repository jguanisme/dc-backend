package common

import (
    "dc-gateway/common"
)

func GenOrderId(prefix string) string {
    return common.DptsGenUUId(prefix)
}

func ModelsValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "AccountId":return common.QueryParamTypeInt64
    case "Balance":return common.QueryParamTypeUInt64
    case "Score":return common.QueryParamTypeUInt64
    case "Grade":return common.QueryParamTypeUInt64
    case "RegCap":return common.QueryParamTypeUInt64
    case "Turnover":return common.QueryParamTypeUInt64
    case "MonAmount":return common.QueryParamTypeUInt64
    case "TransactionType":return common.QueryParamTypeInt64
    case "State":return common.QueryParamTypeInt64
    case "WalletType":return common.QueryParamTypeInt64
    case "UserId":return common.QueryParamTypeInt64
    case "Amount":return common.QueryParamTypeInt64
    case "RelatedTransId":return common.QueryParamTypeInt64
    case "UtxoTime":return common.QueryParamTypeInt64
    case "UtxoVersion":return common.QueryParamTypeInt64
    case "ShopId":return common.QueryParamTypeInt64
    case "PreCurrencyId":return common.QueryParamTypeInt64
    case "WalletId":return common.QueryParamTypeInt64
    case "UtxoVoutIndex":return common.QueryParamTypeUInt64
    case "UtxoValue":return common.QueryParamTypeUInt64
    case "BankTransactionType":return common.QueryParamTypeInt64
    case "CurrencyId":return common.QueryParamTypeInt64
    case "CardId":return common.QueryParamTypeInt64
    case "ZoneId":return common.QueryParamTypeInt64
    case "TotalAmount":return common.QueryParamTypeInt64
    case "CashAmount":return common.QueryParamTypeInt64
    case "UncashAmount":return common.QueryParamTypeInt64
    case "FreezeCashAmount":return common.QueryParamTypeInt64
    case "FreezeUncashAmount":return common.QueryParamTypeInt64
    case "LastTermAmount":return common.QueryParamTypeInt64
    default:
        return common.QueryParamTypeString
    }
    
}
func AccountValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "ZoneId":return common.QueryParamTypeInt64
    case "UserId":return common.QueryParamTypeInt64
    case "TotalAmount":return common.QueryParamTypeInt64
    case "CashAmount":return common.QueryParamTypeInt64
    case "UncashAmount":return common.QueryParamTypeInt64
    case "FreezeCashAmount":return common.QueryParamTypeInt64
    case "FreezeUncashAmount":return common.QueryParamTypeInt64
    case "LastTermAmount":return common.QueryParamTypeInt64
    default:
        return common.QueryParamTypeString
    }
}

func BankTransactionValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "BankTransactionType":return common.QueryParamTypeInt64
    case "Amount":return common.QueryParamTypeInt64
    case "CurrencyId":return common.QueryParamTypeInt64
    case "CardId":return common.QueryParamTypeInt64
    case "WalletType":return common.QueryParamTypeInt64
    case "State":return common.QueryParamTypeInt64
    default:
        return common.QueryParamTypeString
    }
}

func CardValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "WalletId":return common.QueryParamTypeInt64
    default:
        return common.QueryParamTypeString
    }
}

func CurrencyValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "Amount":return common.QueryParamTypeInt64
    case "State":return common.QueryParamTypeInt64
    case "PreCurrencyId":return common.QueryParamTypeInt64
    case "WalletId":return common.QueryParamTypeInt64
    case "UtxoVoutIndex":return common.QueryParamTypeUInt64
    case "UtxoValue":return common.QueryParamTypeUInt64
    default:
        return common.QueryParamTypeString
    }
}

func DeviceValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "ShopId":return common.QueryParamTypeInt64
    case "AccountId":return common.QueryParamTypeInt64
    default:
        return common.QueryParamTypeString
    }
}


func PayValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "Amount":return common.QueryParamTypeInt64
    case "State":return common.QueryParamTypeInt64
    case "RelatedTransId":return common.QueryParamTypeInt64
    case "UtxoTime":return common.QueryParamTypeInt64
    case "UtxoVersion":return common.QueryParamTypeInt64
    default:
        return common.QueryParamTypeString
    }
}

func ShopValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "UserId":return common.QueryParamTypeInt64
    case "AccountId":return common.QueryParamTypeInt64
    default:
        return common.QueryParamTypeString
    }
}


func TransHistoryValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "TransactionType":return common.QueryParamTypeInt64
    case "Amount":return common.QueryParamTypeUInt64
    case "State":return common.QueryParamTypeInt64
    case "WalletType":return common.QueryParamTypeInt64
    default:
        return common.QueryParamTypeString
    }
}

func UserValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "Score":return common.QueryParamTypeUInt64
    case "Grade":return common.QueryParamTypeUInt64
    case "RegCap":return common.QueryParamTypeUInt64
    case "Turnover":return common.QueryParamTypeUInt64
    case "MonAmount":return common.QueryParamTypeUInt64
    default:
        return common.QueryParamTypeString
    }
}


func WalletValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    case "AccountId":return common.QueryParamTypeInt64
    case "Balance":return common.QueryParamTypeUInt64
    default:
        return common.QueryParamTypeString
    }
}


func ZoneValType(s string)int64{
    switch(s){
    case "Id":return common.QueryParamTypeUInt64
    default:
        return common.QueryParamTypeString
    }
}
