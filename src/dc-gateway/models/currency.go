package models

import (
    "fmt"
    "time"
    "dc-gateway/logger"
    "dc-gateway/common"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"

    "github.com/astaxie/beego/orm"
)

type Currency struct {
    Id int64 `json:"Id,omitempty"`
    Name string `json:"Name,omitempty"`
    Description	string `json:"Description,omitempty"`
    Amount int64 
    Attribute string `json:"Attribute,omitempty"`
    State int64 `json:"State,omitempty"`
    SourceBank string `json:"SourceBank,omitempty"`
    PreCurrencyId int64 `json:"PreCurrencyId,omitempty"`
    Created time.Time `json:"Created,omitempty"`
    Updated time.Time `json:"Updated,omitempty"`
    WalletId int64 `json:"WalletId,omitempty"`
    UtxoTxid string `json:"UtxoTxid,omitempty"`
    UtxoVoutIndex int64 `json:"UtxoVoutIndex,omitempty"`
    UtxoSingature string `json:"UtxoSingature,omitempty"`
    UtxoAddress string `json:"UtxoAddress,omitempty"`
    UtxoMessage string `json:"UtxoMessage,omitempty"`
    UtxoValue int64
}

func transferOrmCurrency(ormObj *orms.Currency) *Currency{
    obj := new(Currency)
    obj.Id = ormObj.Id
    obj.Name = ormObj.Name
    obj.Description = ormObj.Description
    obj.Amount = ormObj.Amount
    obj.State = ormObj.State
    obj.SourceBank = ormObj.SourceBank    
    obj.PreCurrencyId = ormObj.PreCurrencyId
    obj.Created = ormObj.Created
    obj.Updated = ormObj.Updated
    obj.UtxoTxid = ormObj.UtxoTxid
    obj.UtxoVoutIndex = ormObj.UtxoVoutIndex
    obj.UtxoSingature = ormObj.UtxoSingature    
    obj.UtxoAddress = ormObj.UtxoAddress
    obj.UtxoMessage = ormObj.UtxoMessage
    obj.UtxoValue = ormObj.UtxoValue    
    if ormObj.Wallet != nil {
        obj.WalletId = ormObj.Wallet.Id
    }    

    return obj
}

func transferCurrency(obj *Currency) (*orms.Currency,*[]string){
    f := []string{}
    ormObj := new(orms.Currency)
    ormObj.Id = obj.Id
    if obj.Name != "" {
        ormObj.Name = obj.Name
        f=append(f,"Name")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.Amount >= 0 {
        ormObj.Amount = obj.Amount
        f=append(f,"Amount")
    }     
    if obj.Attribute != "" {
        ormObj.Attribute = obj.Attribute
        f=append(f,"Attribute")
    }
    if obj.State > 0 {
        ormObj.State = obj.State
        f=append(f,"State")
    }  
    if obj.SourceBank != "" {
        ormObj.SourceBank = obj.SourceBank
        f=append(f,"SourceBank")
    }
    if obj.PreCurrencyId != 0 {
        ormObj.PreCurrencyId = obj.PreCurrencyId
        f=append(f,"PreCurrencyId")
    }      
    if obj.WalletId != 0 {
        ormObj.Wallet = new (orms.Wallet)
        ormObj.Wallet.Id = obj.WalletId
        f=append(f,"Wallet")
    }
    if obj.UtxoTxid != "" {
        ormObj.UtxoTxid = obj.UtxoTxid
        f=append(f,"UtxoTxid")
    }
    if obj.UtxoVoutIndex > 0 {
        ormObj.UtxoVoutIndex = obj.UtxoVoutIndex
        f=append(f,"UtxoVoutIndex")
    }         
    if obj.UtxoSingature != "" {
        ormObj.UtxoSingature = obj.UtxoSingature
        f=append(f,"UtxoSingature")
    }
    if obj.UtxoAddress != "" {
        ormObj.UtxoAddress = obj.UtxoAddress
        f=append(f,"UtxoAddress")
    }
    if obj.UtxoMessage != "" {
        ormObj.UtxoMessage = obj.UtxoMessage
        f=append(f,"UtxoMessage")
    }    
    if obj.UtxoValue > 0 {
        ormObj.UtxoValue = obj.UtxoValue
        f=append(f,"UtxoValue")
    }      
    //if obj.Created != "" {
        //ormObj.Created = obj.Created
    //}
    return ormObj,&f
}


func GetCurrency(o orm.Ormer, id int64) (ret *Currency, e error) {
    ormObj := orms.Currency{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllCurrency No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return transferOrmCurrency(&ormObj),nil
}

func GetAllCurrency(o orm.Ormer) ([]*Currency, error){
    var ormObj []*orms.Currency
    _, err := o.QueryTable("currency").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllCurrency No Rows")
        // return nil,errors.New("查询不到")
    } 

    var obj []*Currency
    for _,v := range ormObj{
        obj=append(obj,transferOrmCurrency(v))
    }

    return obj,nil
}

func QueryCurrency(o orm.Ormer, qm *common.QueryParam) ([]*Currency, error){
    sqlMode := int64(common.QueryModePrepare)
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.CurrencyValType)

    // GetPagedSql Param    table,  conditions, default orderby
    sql,_ := qm.GetPagedSql("currency", tConds,     "id desc")

    var ormObj []*orms.Currency
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*Currency
    for _,v := range ormObj{
        obj=append(obj,transferOrmCurrency(v))
    }

    return obj,nil
}
func QueryCurrencyByWalletIdForUpdate(o orm.Ormer, walletId int64) ([]*Currency, error){
    var ormObj []*orms.Currency

    sql := "SELECT * FROM currency WHERE %s=? ORDER BY created DESC FOR UPDATE"
    _, err := o.Raw(fmt.Sprintf(sql,"wallet_id"), walletId).QueryRows(&ormObj)
    if err == nil {
        //fmt.Println("currency nums: ", num)
    }

    var obj []*Currency
    for _,v := range ormObj{
        obj=append(obj,transferOrmCurrency(v))
    }

    return obj,nil
}
func QueryCurrencyByWalletId(o orm.Ormer, walletId int64) ([]*Currency, error){
    var ormObj []*orms.Currency

    sql := "SELECT * FROM currency WHERE %s=? ORDER BY created DESC"
    _, err := o.Raw(fmt.Sprintf(sql,"wallet_id"), walletId).QueryRows(&ormObj)
    if err == nil {
        //fmt.Println("currency nums: ", num)
    }

    var obj []*Currency
    for _,v := range ormObj{
        obj=append(obj,transferOrmCurrency(v))
    }

    return obj,nil
}


// Need transactions
func AddCurrency(o orm.Ormer, obj *Currency) (int64,error) {
    ormObj,_ := transferCurrency(obj)
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddCurrency: %d Success",id))
    return int64(id),err
}

func DeleteCurrency(o orm.Ormer, id int64) {
    _, err := o.Delete(&orms.Currency{Id: id});
    if err!=nil{
        logger.Log.Informational(fmt.Sprintf("TestParallel,DeleteCurrency: %d Error:",id),err.Error())
    }
}

func UpdateCurrency(o orm.Ormer, id int64, obj *Currency) (a *Currency, err error) {
    ormObj,feilds := transferCurrency(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateCurrency: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdateCurrency: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj,nil
}


type InvalidCurrency struct {
    Id int64
    Name string
    Description string
    Amount int64
    Attribute string
    State int64 
    SourceBank string
    NextCurrencyId int64
    Created time.Time
    Updated time.Time
    WalletId int64
    UtxoTxid string
    UtxoVoutIndex int64
    UtxoSingature string
    UtxoAddress string
    UtxoMessage string
    UtxoValue int64 
}

func transferOrmInvalidCurrency(ormObj *orms.InvalidCurrency) *InvalidCurrency{
    obj := new(InvalidCurrency)
    obj.Id = ormObj.Id
    obj.Name = ormObj.Name
    obj.Description = ormObj.Description
    obj.Amount = ormObj.Amount
    obj.State = ormObj.State
    obj.SourceBank = ormObj.SourceBank    
    obj.NextCurrencyId = ormObj.NextCurrencyId
    obj.Created = ormObj.Created
    obj.Updated = ormObj.Updated
    obj.UtxoTxid = ormObj.UtxoTxid
    obj.UtxoVoutIndex = ormObj.UtxoVoutIndex
    obj.UtxoSingature = ormObj.UtxoSingature    
    obj.UtxoAddress = ormObj.UtxoAddress
    obj.UtxoMessage = ormObj.UtxoMessage
    obj.UtxoValue = ormObj.UtxoValue    
    if ormObj.Wallet != nil {
        obj.WalletId = ormObj.Wallet.Id
    }    

    return obj
}

func transferInvalidCurrency(obj *InvalidCurrency) (*orms.InvalidCurrency,*[]string){
    f := []string{}
    ormObj := new(orms.InvalidCurrency)
    ormObj.Id = obj.Id
    if obj.Name != "" {
        ormObj.Name = obj.Name
        f=append(f,"Name")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f=append(f,"Description")
    }
    if obj.Amount >= 0 {
        ormObj.Amount = obj.Amount
        f=append(f,"Amount")
    }     
    if obj.Attribute != "" {
        ormObj.Attribute = obj.Attribute
        f=append(f,"Attribute")
    }
    if obj.State > 0 {
        ormObj.State = obj.State
        f=append(f,"State")
    }  
    if obj.SourceBank != "" {
        ormObj.SourceBank = obj.SourceBank
        f=append(f,"SourceBank")
    }
    if obj.NextCurrencyId != 0 {
        ormObj.NextCurrencyId = obj.NextCurrencyId
        f=append(f,"NextCurrencyId")
    }      
    if obj.WalletId != 0 {
        ormObj.Wallet = new (orms.Wallet)
        ormObj.Wallet.Id = obj.WalletId
        f=append(f,"Wallet")
    }
    if obj.UtxoTxid != "" {
        ormObj.UtxoTxid = obj.UtxoTxid
        f=append(f,"UtxoTxid")
    }
    if obj.UtxoVoutIndex > 0 {
        ormObj.UtxoVoutIndex = obj.UtxoVoutIndex
        f=append(f,"UtxoVoutIndex")
    }         
    if obj.UtxoSingature != "" {
        ormObj.UtxoSingature = obj.UtxoSingature
        f=append(f,"UtxoSingature")
    }
    if obj.UtxoAddress != "" {
        ormObj.UtxoAddress = obj.UtxoAddress
        f=append(f,"UtxoAddress")
    }
    if obj.UtxoMessage != "" {
        ormObj.UtxoMessage = obj.UtxoMessage
        f=append(f,"UtxoMessage")
    }    
    if obj.UtxoValue > 0 {
        ormObj.UtxoValue = obj.UtxoValue
        f=append(f,"UtxoValue")
    }      
    //if obj.Created != "" {
        //ormObj.Created = obj.Created
    //}
    return ormObj,&f
}


func GetInvalidCurrency(o orm.Ormer, id int64) (ret *InvalidCurrency, e error) {
    ormObj := orms.InvalidCurrency{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllInvalidCurrency No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return transferOrmInvalidCurrency(&ormObj),nil
}

func GetAllInvalidCurrency(o orm.Ormer) ([]*InvalidCurrency, error){
    var ormObj []*orms.InvalidCurrency
    _, err := o.QueryTable("invalidcurrency").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllInvalidCurrency No Rows")
        // return nil,errors.New("²éÑ¯²»µ½")
    } 

    var obj []*InvalidCurrency
    for _,v := range ormObj{
        obj=append(obj,transferOrmInvalidCurrency(v))
    }

    return obj,nil
}

func QueryInvalidCurrency(o orm.Ormer, qm *common.QueryParam) ([]*InvalidCurrency, error){
    sqlMode := int64(common.QueryModePrepare)
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)

    // GetPagedSql Param    table,  conditions, default orderby
    sql,_ := qm.GetPagedSql("invalid_currency", tConds,     "id desc")

    var ormObj []*orms.InvalidCurrency
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*InvalidCurrency
    for _,v := range ormObj{
        obj=append(obj,transferOrmInvalidCurrency(v))
    }

    return obj,nil
}

func QueryInvalidCurrencyByWalletId(o orm.Ormer, walletId int64) ([]*InvalidCurrency, error){
    var ormObj []*orms.InvalidCurrency

    sql := "SELECT * FROM invalid_currency WHERE %s=? ORDER BY created DESC"
    _, err := o.Raw(fmt.Sprintf(sql,"wallet_id"), walletId).QueryRows(&ormObj)
    if err == nil {
        //fmt.Println("currency nums: ", num)
    }

    var obj []*InvalidCurrency
    for _,v := range ormObj{
        obj=append(obj,transferOrmInvalidCurrency(v))
    }

    return obj,nil
}

// Need transactions
func AddInvalidCurrency(o orm.Ormer, obj *InvalidCurrency) (int64,error) {
    ormObj,_ := transferInvalidCurrency(obj)
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddInvalidCurrency: %d Success",id))
    return int64(id),err
}

func DeleteInvalidCurrency(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.InvalidCurrency{Id: id}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeleteInvalidCurrency: %d Success",id))
    }
}

func UpdateInvalidCurrency(o orm.Ormer, id int64, obj *InvalidCurrency) (a *InvalidCurrency, err error) {
    ormObj,feilds := transferInvalidCurrency(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateInvalidCurrency: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{        
        logger.Log.Informational(fmt.Sprintf("UpdateInvalidCurrency: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj,nil
}
