package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

type Zone struct {
	Id int64 `orm:"auto"`
	Name string `orm:"null;size(64)"`
	Description string `orm:"null;size(2048)"`
	Domain string `orm:"null;size(64)"`
	PrivKey string `orm:"null;size(2048)"`
	PubKey string `orm:"null;size(1024)"`
	Created time.Time `orm:"auto_now_add;type(datetime)"`
}

func init() {
    orm.RegisterModel(new(Zone))
}
