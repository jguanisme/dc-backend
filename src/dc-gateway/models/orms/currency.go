package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

/*
    流通中的货币需要做分表，必须按WalletId分表用于查询
*/
type Currency struct {
    Id int64 `orm:"auto"`
    Name string `orm:"null"`
    Description	string `orm:"null"`
    Amount int64
    Attribute string `orm:"size(1024)"`
    /*  
        1：正常流通
        103：已交易
        104：已作废
        501：待删除

        100以上为不可流通
    */
    State int64 
    SourceBank string `orm:"null"`

    PreCurrencyId int64 //来源货币，默认0，为银行发行的货币，无来源
    Created time.Time `orm:"auto_now_add;type(datetime)"`
    Updated time.Time `orm:"auto_now;type(datetime)"`
    Wallet *Wallet `orm:"rel(fk)"`    //设置一对多关系

    UtxoTxid string `orm:"null;size(64)"`
    UtxoVoutIndex int64
    UtxoSingature string `orm:"null;size(512)"`
    UtxoAddress string `orm:"size(64)"`
    UtxoMessage string `orm:"null;size(256)"`
    UtxoValue int64 
}

/*
    交易完成，作废，等，都必须将流通的货币放到无效货币
    无效货币需要做分表，必须按NextCurrencyId分表用于查询
*/

type InvalidCurrency struct {
    Id int64     `orm:"auto"`
    Name string
    Description string
    Amount int64
    Attribute string `orm:"size(1024)"`
    /*  
        103：已交易
        104：已作废
        501：待删除

        100以上为不可流通
    */
    State int64 
    SourceBank string

    NextCurrencyId int64  //目的货币，交易后生成的新货币
    Created time.Time `orm:"auto_now_add;type(datetime)"`
    Updated time.Time `orm:"auto_now;type(datetime)"`
    Wallet *Wallet  `orm:"rel(fk)"`    //设置一对多关系

    UtxoTxid string `orm:"size(64)"`
    UtxoVoutIndex int64
    UtxoSingature string `orm:"size(512)"`
    UtxoAddress string `orm:"size(64)"`
    UtxoMessage string `orm:"size(256)"`
    UtxoValue int64 
}

func init() {
    orm.RegisterModel(new(Currency),new(InvalidCurrency))
}
