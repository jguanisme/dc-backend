package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

/*
    交易记录，按完成时间分表，根据业务量决定3个月还是1年分表
*/
type Transaction struct {
    Id int64 `orm:"auto"`
    Name string `orm:"null"`
    Description	string `orm:"null"`
    Amount int64
    Attributes string `orm:"null;size(1024)"`
    Signature string `orm:"null;size(512)"`
    /*
        1：创建
        2：申请,未付款
        3：完成支付
        4：交易失败
        
        101：待删除

        退款则创建一个新的交易
    */
    State int64
    OrderId string `orm:"null;size(256)"`
    Created time.Time `orm:"auto_now_add;type(datetime)"` //创建时间
    Updated time.Time `orm:"auto_now;type(datetime)"`  //完成时间

    RelatedTransId int64 

    ApplyWalletAddr string `orm:"null;size(64)"`
    ReceiptWalletAddr string `orm:"null;size(64)"`
    DeviceSn string `orm:"null;size(64)"`

    UtxoTxid string `orm:"null;size(64)"`
    UtxoTime int64
    UtxoVersion int64
    UtxoServerSignature string `orm:"null;size(256)"`
    UtxoVin string `orm:"null;size(8192)"`   //currency txid slice, 逗号分隔, 顺序有关
    UtxoVout string `orm:"null;size(8192)"`   //currency txid slice, 逗号分隔, 顺序有关
}

func init() {
    orm.RegisterModel(new(Transaction))
}
