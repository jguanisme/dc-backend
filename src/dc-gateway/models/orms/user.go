package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

type User struct {
	Id int64 `orm:"auto"`
	Name string
	Type string `orm:"null;size(1)"`
	State string `orm:"null;size(2)"`
	Score int64
	Grade int64
	Email string `orm:"null;size(128)"`
	FixedPhone string `orm:"null;size(15)"`
	MobilePhone string `orm:"null;size(11)"`
	Address string `orm:"null;size(256)"`
	PostCode string `orm:"null;size(6)"`
	CertType string `orm:"null;size(2)"`
	CertNo string `orm:"null;size(32)"`
	CreateTime time.Time `orm:"auto_now;type(datetime)"`
	Account []*Account `orm:"null;reverse(many)"`
	Shop []*Shop `orm:"null;reverse(many)"`
	RealNameAuthMark string `orm:"null;size(1)"`
	MobileAuthMark string `orm:"null;size(1)"`
	BizLicNo string `orm:"null;size(32)"`
	TaxRegNo string `orm:"null;size(32)"`
	CorpCertType string `orm:"null;size(2)"`
	CorpCertNo string `orm:"null;size(32)"`
	CorpName string `orm:"null;size(40)"`
	RegCap int64
	CorpType string `orm:"null;size(2)"`
	Industry string `orm:"null;size(32)"`
	Turnover int64
	MonAmount int64
	CorpWebUrl string `orm:"null;size(512)"`
}

func init() {
    orm.RegisterModel(new(User))
}
