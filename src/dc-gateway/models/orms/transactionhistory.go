package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

/*
    交易记录查询
*/
type TransactionHistory struct {
    Id int64 `orm:"auto"`
    //1 存钱，2取钱，3转账，4收钱
    TransactionType int64
    Description	string `orm:"null;size(2048)"`
    Amount int64
    Attributes string `orm:"null;size(2048)"`
    InName string `orm:"null;size(256)"`
    InAddr string `orm:"null;size(64)"`
    OutName string `orm:"null;size(256)"`
    OutAddr string `orm:"null;size(64)"`
    OrderId string `orm:"null;size(256)"`
    State int64
    WalletType int64
    ZoneName string `orm:"null;size(64)"`
    TransactionSeq string `orm:"null;size(256)"`
    TransactionTime string `orm:"null;type(datetime)"` //交易发生时间
    Created time.Time `orm:"auto_now_add;type(datetime)"` //创建时间
}

func init() {
    orm.RegisterModel(new(TransactionHistory))
}
