package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

type Shop struct {
    Id int64 `orm:"auto"`
    Name string 
    DisplayName string `orm:"null"`
    Description string `orm:"null"`
    Created time.Time `orm:"auto_now_add;type(datetime)"`
    User *User `orm:"rel(fk)"`
    Account *Account `orm:"null;rel(fk)"`
    Device []*Device `orm:"null;reverse(many)"`
}

func init() {
    orm.RegisterModel(new(Shop))
}
