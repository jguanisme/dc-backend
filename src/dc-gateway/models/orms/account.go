package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

type Account struct {
	Id int64 `orm:"auto"`
	Name string
	LoginPwd string	`orm:"size(100)"`
	Salt string	`orm:"size(100)"`
	TradePwd string	`orm:"size(100)"`
	Description string `orm:"null"`
	Created time.Time `orm:"auto_now_add;type(datetime)"`
	Zone *Zone `orm:"null;rel(fk)"`
	User *User `orm:"null;rel(fk)"`
	Device []*Device `orm:"null;reverse(many)"`
	Type string `orm:"size(4)"`
	Stat string `orm:"size(2)"`
	TotalAmount int64
	CashAmount int64
	UncashAmount int64
	FreezeCashAmount int64
	FreezeUncashAmount int64
	LastTermJixiDate time.Time	`orm:"auto_now;type(datetime)"`
	LastUpdateTime time.Time	`orm:"auto_now;type(datetime)"`
	LastTermAmount int64
}

func init() {
	a:=new(Account)
    orm.RegisterModel(a)

}
