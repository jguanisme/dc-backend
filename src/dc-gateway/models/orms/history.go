package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

/*
    交易记录查询
*/
type WalletHistory struct {
    Id int64 `orm:"auto"`
    Description	string `orm:"null;size(2048)"`
    Attributes string `orm:"null;size(2048)"`
    ZoneName string `orm:"null;size(64)"`
    WalletName string `orm:"null;size(64)"`
    WalletAddress string `orm:"null;size(64)"`
    FromWalletName string `orm:"null;size(64)"`
    FromWalletAddress string `orm:"null;size(64)"`
    OrderId string `orm:"null;size(256)"`
    //1 存钱，2取钱，3转账，4收钱
    OrderType int64
    Amount int64
    State int64
    //0:+ 1:-
    HistoryType int64
    Created time.Time `orm:"auto_now_add;type(datetime)"` //创建时间
}

func init() {
    orm.RegisterModel(new(WalletHistory))
}
