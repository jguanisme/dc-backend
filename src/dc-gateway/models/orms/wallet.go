package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

type Wallet  struct {
    Id int64 `orm:"auto"`
    Name string `orm:"null;size(128)"`
    Image string `orm:"null;size(128)"`
    Mode int64 // 0,1 thin client, 2 pang client, 3 database mode
    Description string
    Created time.Time `orm:"auto_now_add;type(datetime)"`
    Account *Account   `orm:"null;rel(fk)"` // 设置一对一反向关系(可选)

    Balance int64
    PubKey string `orm:"null;size(1024)"`
    PrivKey string `orm:"null;size(2048)"`
    UtxoAddress	string `orm:"size(64);unique"` //Key
}


func init() {
    orm.RegisterModel(new(Wallet))
}