package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

type Card  struct {
    Id int64 `orm:"auto"`
    Name string `orm:"null"`
    CardNumber string `orm:"null"`
    Phone string `orm:"null"`
    IdNumber string `orm:"null"`
    PersonName string `orm:"null"`
    Description string `orm:"null"`
    Created time.Time `orm:"auto_now_add;type(datetime)"`
    Wallet *Wallet `orm:"null;rel(fk)"` // 设置一对一反向关系(可选)
    Attribute string `orm:"null;size(2048)"`
}


func init() {
    orm.RegisterModel(new(Card))
}