package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

type Device struct {
    Id int64 `orm:"auto"`
    Name string `orm:"null"`
    Description string `orm:"null"`
    Sn string   `orm:"unique"`
    Created time.Time `orm:"auto_now_add;type(datetime)"`
    Account *Account `orm:"null;rel(fk)"`
    Shop *Shop `orm:"null;rel(fk)"`
}

func init() {
    orm.RegisterModel(new(Device))
}
