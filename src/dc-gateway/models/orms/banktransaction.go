package orms

import (
    "time"
    "github.com/astaxie/beego/orm"
)

/*
    存钱银行结果查询
*/
type BankTransaction struct {
    Id int64 `orm:"auto"`
    //1 存钱，2取钱，3转账，4收钱
    BankTransactionType  int64
    Description	string `orm:"null;size(2048)"`
    Amount int64
    Attributes	string `orm:"null;size(2048)"`
    CurrencyId  int64
    CardId  int64
    CardAttribute string `orm:"null;size(2048)"`
    WalletType  int64
    WalletAddr string `orm:"size(64)"`
    ZoneName string `orm:"null;size(64)"`
    AccountName string `orm:"null;size(256)"`
    State int64
    OrderId string `orm:"null;size(256)"`
    BankResult string `orm:"null;size(2048)"`
    BankTransactionSeq string `orm:"null;size(256)"`
    BankTransactionTime string `orm:"null;type(datetime)"` //交易发生时间
    Created time.Time `orm:"auto_now_add;type(datetime)"` //创建时间
}

func init() {
    orm.RegisterModel(new(BankTransaction))
}
