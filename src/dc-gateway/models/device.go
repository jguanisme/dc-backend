package models

import (
    "fmt"
    "time"

    "dc-gateway/logger"
    "dc-gateway/common"
    "dc-gateway/models/orms"
    mc "dc-gateway/models/common"
    
    "github.com/astaxie/beego/orm"
)

type Device struct {
    Id int64 `json:"Id,omitempty"`
    Name string `json:"Name,omitempty"`
    Description string `json:"Description,omitempty"`
    Sn string `json:"Sn,omitempty"`
    AccountId int64 `json:"AccountId,omitempty"`
    ShopId int64 `json:"ShopId,omitempty"`
    Created time.Time `json:"Created,omitempty"`
}

func transferOrmDevice(args *orms.Device) *Device{
    obj := new(Device)
    obj.Id = args.Id
    obj.Name = args.Name
    obj.Description = args.Description
    obj.Sn = args.Sn
    if args.Account!=nil{
        obj.AccountId = args.Account.Id
    }
    if args.Shop!=nil{
        obj.ShopId = args.Shop.Id
    }
    obj.Created = args.Created
    return obj
}

func transferDevice(obj *Device) (*orms.Device, *[]string){
    f := []string{}
    ormObj := new(orms.Device)
    ormObj.Id = obj.Id
    if obj.Name != "" {
        ormObj.Name = obj.Name
        f = append(f, "Name")
    }
    if obj.Description != "" {
        ormObj.Description = obj.Description
        f = append(f, "Description")
    }
    if obj.Sn != "" {
        ormObj.Sn = obj.Sn
        f = append(f, "Sn")
    }
    if obj.AccountId != 0 {
        ormObj.Account = new(orms.Account)
        ormObj.Account.Id = obj.AccountId
        f = append(f, "AccountId")
    }
    if obj.ShopId != 0 {
        ormObj.Shop = new(orms.Shop)
        ormObj.Shop.Id = obj.ShopId
        f = append(f, "ShopId")
    }
    return ormObj, &f
}

func GetDevice(o orm.Ormer, id int64) (ret *Device, e error) {
    ormObj := orms.Device{Id: id}
    err := o.Read(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllDevice No Rows")
        return nil,common.MakeError(common.CodeDBNoSuchDataError,fmt.Sprintf("No Rows,Id:%d",id))
    } 
    return transferOrmDevice(&ormObj), nil
}

func GetAllDevice(o orm.Ormer) ([]*Device, error){
    var ormObj []*orms.Device
    _, err := o.QueryTable("device").All(&ormObj)
    if err == orm.ErrNoRows {
        logger.Log.Informational("GetAllDevice No Rows")
//      return nil, errors.New("查询不到")
    } 

    var obj []*Device
    for _,v := range ormObj{
        obj = append(obj, transferOrmDevice(v))
    }

    return obj, nil
}

func GetDeviceBySn(o orm.Ormer, sn string) ([]*Device, error){
    var ormObj []*orms.Device

    sql := "SELECT * FROM device WHERE %s=? ORDER BY created DESC"
    _, err := o.Raw(fmt.Sprintf(sql,"sn"), sn).QueryRows(&ormObj)
    if err == nil {
        //fmt.Println("device nums: ", num)
    }

    var obj []*Device
    for _,v := range ormObj{
        obj=append(obj,transferOrmDevice(v))
    }

    return obj,nil
}

func QueryDevice(o orm.Ormer, qm *common.QueryParam) ([]*Device, error){
    sqlMode := int64(common.QueryModePrepare)
    tConds,tVals := qm.GetSqlConds(sqlMode,mc.ModelsValType)

    // GetPagedSql Param    table,  conditions, default orderby
    sql, _ := qm.GetPagedSql("device", tConds, "id desc")
    
    var ormObj []*orms.Device
    err := mc.QuerySql(o,sql,tVals,&ormObj,sqlMode)
    if err!=nil{
        return nil,common.MakeFromError(common.CodeDBQueryError,err)
    }

    var obj []*Device
    for _,v := range ormObj{
        obj = append(obj, transferOrmDevice(v))
    }

    return obj, nil
}

// Need transactions
func AddDevice(o orm.Ormer, obj *Device) (int64,error) {
    ormObj,_ := transferDevice(obj)
    id, err := o.Insert(ormObj)
    if err!=nil{
        return 0, common.MakeFromDBError(common.CodeDBInsertError,err)
    }
    logger.Log.Informational(fmt.Sprintf("AddDevice: %d Success",id))
    return int64(id), err
}

func DeleteDevice(o orm.Ormer, id int64) {
    if _, err := o.Delete(&orms.Device{Id: id}); err == nil {
        logger.Log.Informational(fmt.Sprintf("DeleteDevice: %d Success",id))
    }
}

func UpdateDevice(o orm.Ormer, id int64, obj *Device) (a *Device, err error) {
    ormObj,feilds := transferDevice(obj)
    ormObj.Id = id
    if _, err := o.Update(ormObj,*feilds...); err != nil {
        logger.Log.Informational(fmt.Sprintf("UpdateDevice: %d, Error: %s", id, err.Error()))
        return nil,common.MakeFromDBError(common.CodeDBUpdateError,err)
    }else{
        logger.Log.Informational(fmt.Sprintf("UpdateDevice: %d, Success", id))
    }
    obj.Id = ormObj.Id
    return obj, nil
}

func GetDevicesByShopId(o orm.Ormer, ShopId int64) ([]*Device, error){
    var ormObj []*orms.Device
    if _, err := o.Raw("select * from device where shop_id = ?", ShopId).QueryRows(&ormObj); err != nil {
        logger.Log.Error("%s", err)
    }
    var obj []*Device
    for _, v := range ormObj {
        obj = append(obj, transferOrmDevice(v))
    }
    return obj, nil
}
