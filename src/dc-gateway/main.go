package main

import (
	_ "dc-gateway/routers"

	"fmt"

	"dc-gateway/logger"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
)

func HttpEnter(ctx *context.Context) {  
    logger.Log.Informational(fmt.Sprintf("[%s] - %s - Enter",ctx.Request.Method,ctx.Request.RequestURI))
}
func HttpExit(ctx *context.Context) {  
    logger.Log.Informational(fmt.Sprintf("[%s] - %s - Enter",ctx.Request.Method,ctx.Request.RequestURI))
}
/*
bee run -gendoc=true -downdoc=true
*/
func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	//models.OrmRunCmd()
	// beego.InsertFilter("/*",beego.BeforeExec,HttpEnter)
	// beego.InsertFilter("/*",beego.FinishRouter,HttpExit,false)
	beego.Run()
}
