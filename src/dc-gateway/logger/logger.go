package logger

import (
	"fmt"
	"strings"
	"runtime"
	"encoding/json"
	
    "dc-gateway/conf"

	"github.com/astaxie/beego/logs"

)

var	Log *logs.BeeLogger

type LoggerWriter struct{
}
func (w *LoggerWriter)Write(p []byte) (n int, err error){
	traceCaller(string(p),2,0)
	return len(p),nil
}
var loggerWriter *LoggerWriter
func GetLoggerWriter()(*LoggerWriter){
	if loggerWriter==nil{
		loggerWriter = new(LoggerWriter)
	}
	return loggerWriter
}

func init(){	
    InitLogger()
}

func InitLogger() {
    cfg:=conf.GetAppConfig()
    fn:=cfg.LogFileName
    if fn==""{
    	fn="./dcg.log"
    }
	fl:=fmt.Sprintf(`{"filename":"%s","daily":true,"maxdays":30}`,fn)
	logs.SetLogger("console")
	logs.SetLogger("file",fl)
	logs.Async()
	logs.SetLevel(logs.LevelDebug) 
	Log = logs.NewLogger()
	Log.Async()
	Log.SetLogger("console")  // 设置日志记录方式：控制台记录
	Log.SetLogger("file",fl)
	Log.SetLevel(logs.LevelDebug) // 设置日志写入缓冲区的等级：Debug级别（最低级别，所以所有log都会输入到缓冲区）

}

func trace(str string, level int){
	traceCaller(str,level,3)
}
func traceCaller(str string, level int, caller int){
	var name string
	var fname string
	//pc,file,line,ok

	pc,file,line,ok := runtime.Caller(caller)
	if ok{
		f := runtime.FuncForPC(pc)
		name = f.Name()
		fs:=strings.Split(file,"/")
		p := len(fs)
		if p>1{
			fname=fs[p-1]
		}
	}
	if Log==nil{
		fmt.Println(fmt.Sprintf("[%s:%d] [FMT] %s - %s - Need Init Logger First.",fname,line,name,str))
		return
	}
	switch(level){
	case 0:
		Log.Trace(fmt.Sprintf("[%s:%d] [Trace] %s - %s",fname,line,name,str))
		break
	case 1:
		Log.Informational(fmt.Sprintf("[%s:%d] [Dump] %s - %s",fname,line,name,str))
		break
	case 2:
		Log.Informational(fmt.Sprintf("[%s:%d] [Log] %s - %s",fname,line,name,str))
		break
	case 3:
		Log.Warn(fmt.Sprintf("[%s:%d] [Warn] %s - %s",fname,line,name,str))
		break
	case 4:
		Log.Informational(fmt.Sprintf("[%s:%d] [Info] %s - %s",fname,line,name,str))
		break
	default:
		Log.Debug(fmt.Sprintf("[%s:%d] [Debug] %s - %s",fname,line,name,str))
		break
	}
}

func TraceEnter(){
	trace("Enter",0)
}

func TraceExit(){
	trace("Exit",0)
}

func Dump(v interface{}){
	s,_ := json.Marshal(v)
	trace(string(s),1)
}

func Dumpve(s string,v interface{},err error){
	vs,_ := json.Marshal(v)
	e:="Dumpve:null"
	if err!=nil{
		e=err.Error()
	}	
	trace(fmt.Sprintf("%s-%s-%s",s,string(vs),e),1)
}

func Info(s string){
	trace(s,2)
}
func Warn(s string){
	trace(s,3)
}
func DumpError(err error){
	if err!=nil{
		trace(err.Error(),3)
	}else{		
		trace("Input Error Is Nil At DumpError",3)
	}
}