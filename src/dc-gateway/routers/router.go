// @APIVersion 1.0.0
// @Title DC Gateway API
// @Description DC Gateway Project autogenerate documents for your API
// @Contact jguanisme@163.com
// @TermsOfServiceUrl #
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"dc-gateway/controllers"

	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("/common",
			beego.NSInclude(
				&controllers.CommonController{},
			),
		),
		beego.NSNamespace("/dpts0",
			beego.NSInclude(
				&controllers.DptsController{},
			),
		),
		beego.NSNamespace("/dpts1",
			beego.NSInclude(
				&controllers.DptsController{},
			),
		),
		beego.NSNamespace("/user",
			beego.NSInclude(
				&controllers.UserController{},
			),
		),
		beego.NSNamespace("/account",
			beego.NSInclude(
				&controllers.AccountController{},
			),
		),
		beego.NSNamespace("/card",
			beego.NSInclude(
				&controllers.CardController{},
			),
		),		
		beego.NSNamespace("/wallet",
			beego.NSInclude(
				&controllers.WalletController{},
			),
		),
		beego.NSNamespace("/pay",
			beego.NSInclude(
				&controllers.PayController{},
			),
		),
		beego.NSNamespace("/shop",
			beego.NSInclude(
				&controllers.ShopController{},
			),
		),
		beego.NSNamespace("/device",
			beego.NSInclude(
				&controllers.DeviceController{},
			),
		),
		beego.NSNamespace("/currency",
			beego.NSInclude(
				&controllers.CurrencyController{},
			),
		),
		beego.NSNamespace("/invalidcurrency",
			beego.NSInclude(
				&controllers.InvalidCurrencyController{},
			),
		),
		beego.NSNamespace("/transactionhistory",
			beego.NSInclude(
				&controllers.TransactionHistoryController{},
			),
		),
		beego.NSNamespace("/banktransaction",
			beego.NSInclude(
				&controllers.BankTransactionController{},
			),
		),
		beego.NSNamespace("/zone",
			beego.NSInclude(
				&controllers.ZoneController{},
			),
		),
	)

	beego.AddNamespace(ns)
}
