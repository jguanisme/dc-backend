package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "AcctLogin",
			Router: `/AcctLogin`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "FastRegister",
			Router: `/FastRegister`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "FillUsrInfo",
			Router: `/FillUsrInfo`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "GetAcctsByUserId",
			Router: `/GetAcctsByUsrId`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "ModAcctPwd",
			Router: `/ModAcctPwd`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:AccountController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "Deposit",
			Router: `/Deposit`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "Exchange",
			Router: `/Exchange`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "GetBankTransactionByOrderId",
			Router: `/GetBankTransactionByOrderId`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "QueryDepositRange",
			Router: `/QueryDepositRange`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "StartBankTransaction",
			Router: `/StartBankTransaction`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "UpdateBankTransactionByOrderId",
			Router: `/UpdateBankTransactionByOrderId`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:BankTransactionController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CardController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CardController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CardController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CardController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CardController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CardController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CardController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CardController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CardController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CardController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CardController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CardController"],
		beego.ControllerComments{
			Method: "GetCardsByWalletId",
			Router: `/GetCardsByWalletId`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CardController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CardController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CommonController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CommonController"],
		beego.ControllerComments{
			Method: "QueryRfscMany",
			Router: `/queryRfscMany`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CommonController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CommonController"],
		beego.ControllerComments{
			Method: "QueryRfscOne",
			Router: `/queryRfscOne`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CommonController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CommonController"],
		beego.ControllerComments{
			Method: "SendSMsg",
			Router: `/sdsmsg`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CommonController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CommonController"],
		beego.ControllerComments{
			Method: "SendTemplateSMsg",
			Router: `/stsmsg`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"],
		beego.ControllerComments{
			Method: "GetCurrencyByWalletId",
			Router: `/GetCurrencyByWalletId`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:CurrencyController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "GetDeviceBySn",
			Router: `/GetDeviceBySn`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "GetDevicesByShopId",
			Router: `/GetDevicesByShopId`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"],
		beego.ControllerComments{
			Method: "CommitX",
			Router: `/CommitX`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"],
		beego.ControllerComments{
			Method: "DoX",
			Router: `/DoX`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"],
		beego.ControllerComments{
			Method: "DoHandleDptsPacket",
			Router: `/Dpts`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"],
		beego.ControllerComments{
			Method: "PrepareX",
			Router: `/PrepareX`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"],
		beego.ControllerComments{
			Method: "QueryPay",
			Router: `/QueryPay`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"],
		beego.ControllerComments{
			Method: "RollbackX",
			Router: `/RollbackX`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"],
		beego.ControllerComments{
			Method: "TryX",
			Router: `/TryX`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:DptsController"],
		beego.ControllerComments{
			Method: "UpdateRouteEntity",
			Router: `/UpdateRouteEntity`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"],
		beego.ControllerComments{
			Method: "GetInvalidCurrencyByWalletId",
			Router: `/GetInvalidCurrencyByWalletId`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:InvalidCurrencyController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "ConfirmPayTx",
			Router: `/ConfirmPayTx`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "CreatePayTx",
			Router: `/CreatePayTx`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "QueryPayByWalletAddr",
			Router: `/QueryPayByWalletAddr`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "QueryPayTx",
			Router: `/QueryPayTx`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "TransferPayTx",
			Router: `/TransferPayTx`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:PayController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:PayController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ShopController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:TransactionHistoryController"],
		beego.ControllerComments{
			Method: "QueryTransactionHistoryByWalletAddr",
			Router: `/queryByWalletAddr`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:UserController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:UserController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:UserController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:UserController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:UserController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:UserController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:UserController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:UserController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:UserController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:UserController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:UserController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"],
		beego.ControllerComments{
			Method: "GetWalletByUtxoAddress",
			Router: `/GetWalletByUtxoAddress`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"],
		beego.ControllerComments{
			Method: "GetWalletsByAccountId",
			Router: `/GetWalletsByAccountId`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:WalletController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"],
		beego.ControllerComments{
			Method: "GetCurrentZone",
			Router: `/getCurrentZone`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"] = append(beego.GlobalControllerRouter["dc-gateway/controllers:ZoneController"],
		beego.ControllerComments{
			Method: "Query",
			Router: `/query`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

}
