package controllers

import (
	"github.com/astaxie/beego"
	"dc-gateway/controllers/trans"
	"dc-gateway/models"
	"dc-gateway/common"
	"encoding/json"
)

// Operations about Currency
type CurrencyController struct {
	beego.Controller
	b trans.CurrencyTrans
}

// @Title GetAll
// @Description 获取所有的对象列表
// @Success 200 {object} []models.Currency "对象列表"
// @router / [get]
func (c *CurrencyController) GetAll() {
	defer c.ServeJSON()

	d,e := c.b.GetAllCurrency()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description 查询所有的对象列表
// @Param body body common.QueryParam true "查询条件，Conds所有数据为string，示例:{'id':'1'}，Orders支持asc和desc两种，示例:{'created':'asc'}，Size默认为10"
// @Success 200 {object} []models.Currency "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /query [post]
func (c *CurrencyController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryCurrency(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title GetCurrencyByWalletId
// @Description 查询所有的对象列表
// @Param walletId query int true "查询条件"
// @Success 200 {object} []models.Currency "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /GetCurrencyByWalletId [get]
func (c *CurrencyController) GetCurrencyByWalletId() {
	defer c.ServeJSON()

	walletId,e := c.GetInt64("walletId")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}	

	d,e := c.b.QueryCurrencyByWalletId(walletId)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Get Currency
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.Currency "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *CurrencyController) Get() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.GetCurrency(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CreateCurrency
// @Description 创建对象
// @Param body body models.Currency true "创建的对象内容"
// @Success 200 {int} models.Currency.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *CurrencyController) Post() {
	defer c.ServeJSON()

	var o models.Currency
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddCurrency(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete Currency
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *CurrencyController) Delete() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.DeleteCurrency(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}



// @Title Update Currency
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.Currency true "被修改的对象内容"
// @Success 200 {object} models.Currency
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *CurrencyController) Put() {
	defer c.ServeJSON()

	var o models.Currency
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.UpdateCurrency(id,&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


// Operations about InvalidCurrency
type InvalidCurrencyController struct {
	beego.Controller
	b trans.InvalidCurrencyTrans
}

// @Title GetAll
// @Description 获取所有的对象列表
// @Success 200 {object} []models.InvalidCurrency "对象列表"
// @router / [get]
func (c *InvalidCurrencyController) GetAll() {
	defer c.ServeJSON()

	d,e := c.b.GetAllInvalidCurrency()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description 查询所有的对象列表
// @Param body body common.QueryParam true "查询条件，Conds所有数据为string，示例:{'id':'1'}，Orders支持asc和desc两种，示例:{'created':'asc'}，Size默认为10"
// @Success 200 {object} []models.InvalidCurrency "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /query [post]
func (c *InvalidCurrencyController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryInvalidCurrency(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title GetInvalidCurrencyByWalletId
// @Description 查询所有的对象列表
// @Param walletId query int true "查询条件"
// @Success 200 {object} []models.InvalidCurrency "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /GetInvalidCurrencyByWalletId [get]
func (c *InvalidCurrencyController) GetInvalidCurrencyByWalletId() {
	defer c.ServeJSON()

	walletId,e := c.GetInt64("walletId")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}	

	d,e := c.b.QueryInvalidCurrencyByWalletId(walletId)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


// @Title Get InvalidCurrency
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.InvalidCurrency "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *InvalidCurrencyController) Get() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.GetInvalidCurrency(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CreateInvalidCurrency
// @Description 创建对象
// @Param body body models.InvalidCurrency true "创建的对象内容"
// @Success 200 {int} models.InvalidCurrency.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *InvalidCurrencyController) Post() {
	defer c.ServeJSON()

	var o models.InvalidCurrency
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddInvalidCurrency(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete InvalidCurrency
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *InvalidCurrencyController) Delete() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.DeleteInvalidCurrency(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}



// @Title Update InvalidCurrency
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.InvalidCurrency true "被修改的对象内容"
// @Success 200 {object} models.InvalidCurrency
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *InvalidCurrencyController) Put() {
	defer c.ServeJSON()

	var o models.InvalidCurrency
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.UpdateInvalidCurrency(id,&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


