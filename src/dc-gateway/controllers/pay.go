package controllers

import (
	"github.com/astaxie/beego"
	"dc-gateway/controllers/trans"
	"dc-gateway/models"
	"dc-gateway/common"
	"encoding/json"
)

// Operations about Pay
type PayController struct {
	beego.Controller
	b trans.PayTrans
}

// @Title GetAll
// @Description 获取所有的对象列表
// @Success 200 {object} []models.Pay "对象列表"
// @router / [get]
func (c *PayController) GetAll() {
	defer c.ServeJSON()

	d,e := c.b.GetAllPay()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description 查询所有的对象列表
// @Param body body common.QueryParam true "查询条件，Conds所有数据为string，示例:{'id':'1'}，Orders支持asc和desc两种，示例:{'created':'asc'}，Size默认为10"
// @Success 200 {object} []models.Pay "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /query [post]
func (c *PayController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryPay(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title QueryPayByWalletAddr
// @Description 查询所有的对象列表
// @Param applyWalletAddr query string false "查询条件,不填表示不查询该条件"
// @Param receiptWalletAddr query string false "查询条件,不填表示不查询该条件"
// @Success 200 {object} []models.Pay "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /QueryPayByWalletAddr [get]
func (c *PayController) QueryPayByWalletAddr() {
    defer c.ServeJSON()

    applyWalletAddr := c.GetString("applyWalletAddr")
    receiptWalletAddr := c.GetString("receiptWalletAddr")

    d,e := c.b.QueryPayByWalletAddr(applyWalletAddr, receiptWalletAddr)
    if e == nil {
		c.Data["json"] = common.NewOkResult(d)
    } else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
    }
}

// @Title Get Pay
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.Pay "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *PayController) Get() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.GetPay(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CreatePay
// @Description 创建对象
// @Param body body models.Pay true "创建的对象内容"
// @Success 200 {int} models.Pay.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *PayController) Post() {
	defer c.ServeJSON()

	var o models.Pay
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddPay(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete Pay
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *PayController) Delete() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.DeletePay(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}



// @Title Update Pay
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.Pay true "被修改的对象内容"
// @Success 200 {object} models.Pay
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *PayController) Put() {
	defer c.ServeJSON()

	var o models.Pay
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.UpdatePay(id,&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title TransferPayTx
// @Description transfer to Wallet of Accounts Directly
// @Param body body models.Pay true
// @Success 200 {object} models.Pay "Pay"
// @Failure 403 HTTP BODY invalid args
// @router /TransferPayTx [post]
func (c *PayController) TransferPayTx() {
	defer c.ServeJSON()

	var args models.Pay
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &args)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return
	}

    d, e := c.b.TransferPayTx(&args)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CreatePayTx
// @Description "create a pay transaction for user to accept"
// @Param body body models.Pay true
// @Success 200 {int} models.Pay.Id "创建后的对象标识"
// @Failure 403 HTTP BODY invalid args
// @router /CreatePayTx [post]
func (c *PayController) CreatePayTx() {
	defer c.ServeJSON()

	var o models.Pay
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return
	}

    d, e := c.b.CreatePayTx(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title QueryPayTx
// @Description "query a pay transaction for user to accept"
// @Param walletAddr query string true "wallet addr for user "
// @Param state query int true "2 未支付，3 已完成"
// @Param type query int false "0 付款人，1 收款人，不填默认0"
// @Param sn query string false "设备序列号，可不填"
// @Success 200 {object} models.Pay "Pay"
// @Failure 403 HTTP BODY invalid args
// @router /QueryPayTx [get]
func (c *PayController) QueryPayTx() {
	defer c.ServeJSON()

	walletAddr := c.GetString("walletAddr")
	sn := c.GetString("sn")
	t, e := c.GetInt64("type")
	if e != nil {
		t = 0
	}
	state, e := c.GetInt64("state")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d, e := c.b.QueryPayTx(walletAddr,state,t,sn)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title ConfirmPayTx
// @Description "confirm a pay transaction for user to accept"
// @Param body body models.Pay true "only need Id parameter"
// @Success 200 {object} models.Pay
// @Failure 403 HTTP BODY invalid args
// @router /ConfirmPayTx [post]
func (c *PayController) ConfirmPayTx() {
	defer c.ServeJSON()
	
	var args models.Pay
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &args)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return
	}

    d, e := c.b.ConfirmPayTx(&args)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}
