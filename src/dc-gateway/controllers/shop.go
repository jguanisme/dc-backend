package controllers

import (
	"encoding/json"
	"dc-gateway/models"
	"dc-gateway/common"
	"github.com/astaxie/beego"
	"dc-gateway/controllers/trans"
)

// Operations about Shop
type ShopController struct {
	beego.Controller
	b trans.ShopTrans
}

// @Title GetAll
// @Description Get all object list
// @Success 200 {object} []models.Shop "object list"
// @router / [get]
func (c *ShopController) GetAll() {
	defer c.ServeJSON()

	d,e := c.b.GetAllShop()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description Query all object list
// @Param body body common.QueryParam true "query terms must be string, e.g. {'id':'1'}; sort support asc/desc, e.g. {'created':'asc'}; default size is 10."
// @Success 200 {object} []models.Shop "object list"
// @Failure 403 HTTP BODY invalid args
// @router /query [post]
func (c *ShopController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryShop(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Get Shop
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.Shop "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *ShopController) Get() {
	defer c.ServeJSON()

	shop_id, e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d, e := c.b.GetShop(int64(shop_id))
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CreateShop
// @Description 创建对象
// @Param body body models.Shop true "创建的对象内容"
// @Success 200 {int} models.Shop.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *ShopController) Post() {
	defer c.ServeJSON()

	var o models.Shop
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddShop(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete Shop
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *ShopController) Delete() {
	defer c.ServeJSON()

	shop_id, e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d, e := c.b.DeleteShop(int64(shop_id))
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Update Shop
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.Shop true "被修改的对象内容"
// @Success 200 {object} models.Shop
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *ShopController) Put() {
	defer c.ServeJSON()

	var o models.Shop
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	shop_id, e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}
	d, e := c.b.UpdateShop(int64(shop_id), &o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}
