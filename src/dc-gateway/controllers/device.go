package controllers

import (
	"encoding/json"
	"dc-gateway/models"
	"dc-gateway/common"
	"github.com/astaxie/beego"
	"dc-gateway/controllers/trans"
)

// Operations about Device
type DeviceController struct {
	beego.Controller
	b trans.DeviceTrans
}

// @Title GetAll
// @Description 获取所有的对象列表
// @Success 200 {object} []models.Device "对象列表"
// @router / [get]
func (c *DeviceController) GetAll() {
	defer c.ServeJSON()

	d,e := c.b.GetAllDevice()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description 查询所有的对象列表
// @Param body body common.QueryParam true "查询条件，Conds所有数据为string，示例:{'id':'1'}，Orders支持asc和desc两种，示例:{'created':'asc'}，Size默认为10"
// @Success 200 {object} []models.Device "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /query [post]
func (c *DeviceController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryDevice(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Get Device
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.Device "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *DeviceController) Get() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.GetDevice(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title GetDeviceBySn
// @Description 查询所有的对象列表
// @Param sn query string true "查询条件"
// @Success 200 {object} []models.Device "对象"
// @Failure 403 HTTP BODY 输入参数为空
// @router /GetDeviceBySn [get]
func (c *DeviceController) GetDeviceBySn() {
	defer c.ServeJSON()

	sn := c.GetString("sn")
	d,e := c.b.GetDeviceBySn(sn)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CreateDevice
// @Description 创建对象
// @Param body body models.Device true "创建的对象内容"
// @Success 200 {int} models.Device.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *DeviceController) Post() {
	defer c.ServeJSON()

	var o models.Device
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddDevice(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete Device
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *DeviceController) Delete() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.DeleteDevice(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Update Device
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.Device true "被修改的对象内容"
// @Success 200 {object} models.Device
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *DeviceController) Put() {
	defer c.ServeJSON()

	var o models.Device
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.UpdateDevice(id,&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Param ShopId query int64 true
// @Success 200 {object} []models.Device
// @Failure 403 HTTP BODY invalid args
// @router /GetDevicesByShopId [get]
func (c *DeviceController) GetDevicesByShopId() {
	defer c.ServeJSON()

	ShopId,e := c.GetInt64("ShopId")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return
	}

	d, e := c.b.GetDevicesByShopId(int64(ShopId))
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}