package controllers

import (
	"encoding/json"
	"dc-gateway/common"
	"dc-gateway/dpts"
	dptsc "dc-gateway/dpts/common"
	"github.com/astaxie/beego"
)

// Operations about Dpts
type DptsController struct {
	beego.Controller
}

// @Title QueryPay
// @Description QueryPay Description
// @Param body body common.ReflectSqlConds true "查询条件"
// @Param InAddr query string true "请求路由寻址，必须和body中的对应参数相关联"
// @Param OutAddr query string false "两阶段查询的时候填写"
// @Success 200 OK
// @Failure 403 HTTP BODY 输入参数为空
// @router /QueryPay [post]
func (c *DptsController) QueryPay() {
	defer c.ServeJSON()
	
	o := string(c.Ctx.Input.RequestBody)
	InAddr := c.GetString("InAddr")
	OutAddr := c.GetString("OutAddr")

	pkt:=new(dptsc.DptsPacket)
	pkt.ReqId=common.DptsGenUUId("Req")
	pkt.InAddress=InAddr
	pkt.OutAddress=OutAddr
	pkt.Payload=o
	pkt.ServiceName="QueryPay"
	pkt.ServiceClass="pay"
	pkt.TransType=3
	pkt.TTL=1
    d,e := dpts.GetDptsServer().HandleDptsPacket(pkt)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Dpts
// @Description DoHandleDptsPacket Description
// @Param body body common.PostParam true "内容为string"
// @Success 200 OK
// @Failure 403 HTTP BODY 输入参数为空
// @router /Dpts [post]
func (c *DptsController) DoHandleDptsPacket() {
	defer c.ServeJSON()
	
	o := string(c.Ctx.Input.RequestBody)

    d,e := dpts.DoHandleDptsPacket(o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title PrepareX
// @Description PrepareX
// @Param body body common.PostParam true "内容为string"
// @Success 200 OK
// @Failure 403 HTTP BODY 输入参数为空
// @router /PrepareX [post]
func (c *DptsController) PrepareX() {
	defer c.ServeJSON()

	o := string(c.Ctx.Input.RequestBody)

    d,e := dpts.GetDptsServer().PrepareX(o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


// @Title DoX
// @Description DoX Description
// @Param body body common.PostParam true "内容为string"
// @Success 200 OK
// @Failure 403 HTTP BODY 输入参数为空
// @router /DoX [post]
func (c *DptsController) DoX() {
	defer c.ServeJSON()

	o := string(c.Ctx.Input.RequestBody)

    d,e := dpts.GetDptsServer().DoX(o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title TryX
// @Description TryX Description
// @Param body body common.PostParam true "内容为string"
// @Success 200 OK
// @Failure 403 HTTP BODY 输入参数为空
// @router /TryX [post]
func (c *DptsController) TryX() {
	defer c.ServeJSON()

	o := string(c.Ctx.Input.RequestBody)

    t,d,e := dpts.GetDptsServer().TryX(o)
	if e == nil {
		if t!=nil{
			//todo distribute transaction
		}
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CommitX
// @Description CommitX Description
// @Param body body common.PostParam true "内容为string"
// @Success 200 OK
// @Failure 403 HTTP BODY 输入参数为空
// @router /CommitX [post]
func (c *DptsController) CommitX() {
	defer c.ServeJSON()

	o := string(c.Ctx.Input.RequestBody)

    d,e := dpts.GetDptsServer().CommitX(o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title RollbackX
// @Description RollbackX Description
// @Param body body common.PostParam true "内容为string"
// @Success 200 OK
// @Failure 403 HTTP BODY 输入参数为空
// @router /RollbackX [post]
func (c *DptsController) RollbackX() {
	defer c.ServeJSON()

	o := string(c.Ctx.Input.RequestBody)

    d,e := dpts.GetDptsServer().RollbackX(o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


// @Title UpdateRouteEntity
// @Description UpdateRouteEntity Description
// @Param body body dptsc.RoutePacket true "内容"
// @Success 200 OK
// @Failure 403 HTTP BODY 输入参数为空
// @router /UpdateRouteEntity [post]
func (c *DptsController) UpdateRouteEntity() {
	defer c.ServeJSON()

	var pkt dptsc.RoutePacket
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &pkt)
	if err != nil {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,err)
		return
	}
	if pkt.ReqId==""{
		pkt.ReqId=common.DptsGenUUId("Req")
	}
    d,e := dpts.GetDptsServer().HandletRouteOperation(&pkt)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}
