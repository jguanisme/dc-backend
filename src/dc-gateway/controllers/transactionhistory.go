package controllers

import (
	"github.com/astaxie/beego"
	"dc-gateway/controllers/trans"
	"dc-gateway/models"
	"dc-gateway/common"
	"encoding/json"
)

// Operations about TransactionHistory
type TransactionHistoryController struct {
	beego.Controller
	b trans.TransactionHistoryTrans
}

// @Title GetAll
// @Description 获取所有的对象列表
// @Success 200 {object} []models.TransactionHistory "对象列表"
// @router / [get]
func (c *TransactionHistoryController) GetAll() {
	defer c.ServeJSON()

	d,e := c.b.GetAllTransactionHistory()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description 查询所有的对象列表
// @Param body body common.QueryParam true "查询条件，Conds所有数据为string，示例:{'id':'1'}，Orders支持asc和desc两种，示例:{'created':'asc'}，Size默认为10"
// @Success 200 {object} []models.TransactionHistory "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /query [post]
func (c *TransactionHistoryController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryTransactionHistory(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description 查询指定对象列表
// @Param body body common.QueryParam true "查询条件，Conds所有数据为string，示例:{'utxoAddr':'1'}，Orders支持asc和desc两种，示例:{'created':'asc'}，Size默认为10"
// @Success 200 {object} []models.TransactionHistory "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /queryByWalletAddr [post]
func (c *TransactionHistoryController) QueryTransactionHistoryByWalletAddr() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryTransactionHistoryByWalletAddr(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Get TransactionHistory
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.TransactionHistory "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *TransactionHistoryController) Get() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.GetTransactionHistory(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CreateTransactionHistory
// @Description 创建对象
// @Param body body models.TransactionHistory true "创建的对象内容"
// @Success 200 {int} models.TransactionHistory.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *TransactionHistoryController) Post() {
	defer c.ServeJSON()

	var o models.TransactionHistory
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddTransactionHistory(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete TransactionHistory
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *TransactionHistoryController) Delete() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.DeleteTransactionHistory(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}



// @Title Update TransactionHistory
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.TransactionHistory true "被修改的对象内容"
// @Success 200 {object} models.TransactionHistory
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *TransactionHistoryController) Put() {
	defer c.ServeJSON()

	var o models.TransactionHistory
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.UpdateTransactionHistory(id,&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


