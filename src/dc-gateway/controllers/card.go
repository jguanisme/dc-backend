package controllers

import (
	"github.com/astaxie/beego"
	"dc-gateway/controllers/trans"
	"dc-gateway/models"
	"dc-gateway/common"
	"encoding/json"
)

// Operations about Card
type CardController struct {
	beego.Controller
	b trans.CardTrans
}

// @Title GetAll
// @Description 获取所有的对象列表
// @Success 200 {object} []models.Card "对象列表"
// @router / [get]
func (c *CardController) GetAll() {
	defer c.ServeJSON()

	d,e := c.b.GetAllCard()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description 查询所有的对象列表
// @Param body body common.QueryParam true "查询条件，Conds所有数据为string，示例:{'id':'1'}，Orders支持asc和desc两种，示例:{'created':'asc'}，Size默认为10"
// @Success 200 {object} []models.Card "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /query [post]
func (c *CardController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryCard(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title GetCardsByWalletId
// @Description 查询所有的对象列表
// @Param walletId query int true "查询条件"
// @Success 200 {object} []models.Card "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /GetCardsByWalletId [get]
func (c *CardController) GetCardsByWalletId() {
	defer c.ServeJSON()

	walletId,e := c.GetInt64("walletId")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}	

	d,e := c.b.QueryCardByWalletId(walletId)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Get Card
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.Card "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *CardController) Get() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.GetCard(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CreateCard
// @Description 创建对象
// @Param body body models.Card true "创建的对象内容"
// @Success 200 {int} models.Card.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *CardController) Post() {
	defer c.ServeJSON()

	var o models.Card
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddCard(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete Card
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *CardController) Delete() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.DeleteCard(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}



// @Title Update Card
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.Card true "被修改的对象内容"
// @Success 200 {object} models.Card
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *CardController) Put() {
	defer c.ServeJSON()

	var o models.Card
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.UpdateCard(id,&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


