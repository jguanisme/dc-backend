package controllers

import (
	"github.com/astaxie/beego"
	"dc-gateway/controllers/trans"
	"dc-gateway/models"
	"dc-gateway/common"
	"encoding/json"
)

// Operations about BankTransaction
type BankTransactionController struct {
	beego.Controller
	b trans.BankTransactionTrans
}

// @Title GetAll
// @Description 获取所有的对象列表
// @Success 200 {object} []models.BankTransaction "对象列表"
// @router / [get]
func (c *BankTransactionController) GetAll() {
	defer c.ServeJSON()

	d,e := c.b.GetAllBankTransaction()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description 查询所有的对象列表
// @Param body body common.QueryParam true "查询条件，Conds所有数据为string，示例:{'id':'1'}，Orders支持asc和desc两种，示例:{'created':'asc'}，Size默认为10"
// @Success 200 {object} []models.BankTransaction "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /query [post]
func (c *BankTransactionController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryBankTransaction(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title QueryDepositRange
// @Description 查询所有的对象列表
// @Param sdate query string true "开始时间"
// @Param edate query string true "结束时间"
// @Success 200 {object} []models.DepositRange "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /QueryDepositRange [get]
func (c *BankTransactionController) QueryDepositRange() {
	defer c.ServeJSON()
	st:=c.GetString("sdate")
	ed:=c.GetString("edate")
	d,e := c.b.QueryDepositRange(st,ed)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Get BankTransaction
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.BankTransaction "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *BankTransactionController) Get() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.GetBankTransaction(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


// @Title Get BankTransactionByOrderId
// @Description 根据订单ID查询对象内容
// @Param orderId query string true "订单Id"
// @Success 200 {object} models.BankTransaction "对象实例"
// @Failure 403 orderId 参数为空
// @router /GetBankTransactionByOrderId [get]
func (c *BankTransactionController) GetBankTransactionByOrderId() {
	defer c.ServeJSON()

	orderId := c.GetString("orderId")

	d,e := c.b.GetBankTransactionByOrderId(orderId)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}
// @Title StartBankTransaction
// @Description 创建对象
// @Param body body models.BankTransaction true "创建的对象内容"
// @Success 200 {string} models.BankTransaction.OrderId "创建后的订单Id"
// @Failure 403 HTTP BODY 输入参数为空
// @router /StartBankTransaction [post]
func (c *BankTransactionController) StartBankTransaction() {
	defer c.ServeJSON()

	var o models.BankTransaction
	e := json.Unmarshal(c.Ctx.Input.RequestBody,&o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.StartBankTransaction(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Update BankTransactionByOrderId
// @Description 根据订单Id修改对象
// @Param body body models.BankTransaction true "被修改的对象内容 OrderId必填，修改内容必填，其它选填"
// @Success 200 {object} models.BankTransaction
// @router /UpdateBankTransactionByOrderId [put]
func (c *BankTransactionController) UpdateBankTransactionByOrderId() {
	defer c.ServeJSON()

	var o models.BankTransaction
	e := json.Unmarshal(c.Ctx.Input.RequestBody,&o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.UpdateBankTransactionByOrderId(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


// @Title CreateBankTransaction
// @Description 创建对象
// @Param body body models.BankTransaction true "创建的对象内容"
// @Success 200 {int} models.BankTransaction.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *BankTransactionController) Post() {
	defer c.ServeJSON()

	var o models.BankTransaction
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddBankTransaction(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete BankTransaction
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *BankTransactionController) Delete() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.DeleteBankTransaction(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}



// @Title Update BankTransaction
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.BankTransaction true "被修改的对象内容"
// @Success 200 {object} models.BankTransaction
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *BankTransactionController) Put() {
	defer c.ServeJSON()

	var o models.BankTransaction
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.UpdateBankTransaction(id,&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


// @Title Exchange
// @Description 兑换数字货币
// @Param body body models.BankTransaction true "创建的对象内容"
// @Success 200 {int} models.BankTransaction.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router /Exchange [post]
func (c *BankTransactionController) Exchange() {
	defer c.ServeJSON()

	var o models.BankTransaction
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.Exchange(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Deposit
// @Description 兑换数字货币
// @Param body body models.BankTransaction true "创建的对象内容"
// @Success 200 {int} models.BankTransaction.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router /Deposit [post]
func (c *BankTransactionController) Deposit() {
	defer c.ServeJSON()

	var o models.BankTransaction
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.Deposit(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}