package controllers

import (
	"encoding/json"
	"dc-gateway/models"
	"dc-gateway/common"
	"github.com/astaxie/beego"
	"dc-gateway/controllers/trans"
)

// Operations about Users
type UserController struct {
	beego.Controller
	b trans.UserTrans
}

// @Title GetAll
// @Description Get all object list
// @Success 200 {object} []models.User "object list"
// @router / [get]
func (c *UserController) GetAll() {
	defer c.ServeJSON()

	d, e := c.b.GetAllUser()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description Query all object list
// @Param body body common.QueryParam true "query terms must be string, e.g. {'id':'1'}; sort support asc/desc, e.g. {'created':'asc'}; default size 10."
// @Success 200 {object} []models.User "object list"
// @Failure 403 HTTP BODY invalid args
// @router /query [post]
func (c *UserController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return
	}

	d, e := c.b.QueryUser(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Get User
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.User "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *UserController) Get() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.GetUser(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title CreateUser
// @Description 创建对象
// @Param body body models.User true "创建的对象内容"
// @Success 200 {int} models.User.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *UserController) Post() {
	defer c.ServeJSON()

	var o models.User
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddUser(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete Zone
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *UserController) Delete() {
	defer c.ServeJSON()

	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.DeleteUser(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Update User
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.User true "被修改的对象内容"
// @Success 200 {object} models.User
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *UserController) Put() {
	defer c.ServeJSON()

	var o models.User
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	id,e := c.GetInt64(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.UpdateUser(id,&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}
