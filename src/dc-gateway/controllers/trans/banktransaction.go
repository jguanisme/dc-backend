package trans

import (
	"fmt"

	"dc-gateway/logger"
	"dc-gateway/utxo"
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"

    "github.com/astaxie/beego/orm"
	
)

// Transactions about BankTransaction
type BankTransactionTrans struct {
}

func (b *BankTransactionTrans) GetAllBankTransaction() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllBankTransaction(ormer)

	return common.MakeResult(err,o)
}

func (b *BankTransactionTrans) QueryBankTransaction(qm *common.QueryParam)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryBankTransaction(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *BankTransactionTrans) QueryDepositRange(st string,ed string) (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.QueryDepositRange(ormer,st,ed)

	return common.MakeResult(err,o)
}

func (b *BankTransactionTrans) GetBankTransaction(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetBankTransaction(ormer,id)

	return common.MakeResult(err,o)
}

func (b *BankTransactionTrans) GetBankTransactionByOrderId(orderId string)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetBankTransactionByOrderId(ormer,orderId)

	return common.MakeResult(err,o)
}

func (b *BankTransactionTrans)  StartBankTransaction(obj *models.BankTransaction)(interface{}, error) {
	var e error 
	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)
	//set it at update phase
	if obj.BankTransactionType==2{
		obj.BankTransactionType=0
	}
	o,err := models.StartBankTransaction(ormer,obj)

	return common.MakeResult(err, o)
}

func (b *BankTransactionTrans) UpdateBankTransactionByOrderId(obj *models.BankTransaction) (interface{}, error) {
	var e error 
	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	o,err := models.UpdateBankTransactionByOrderId(ormer,obj)
	if err==nil {
		_,ebth := HandleBankTransHistory(ormer,o,true)
		if ebth!=nil {
			e = ebth
		}
	}else{		
		e = err
	}

	return common.MakeResult(err,o)
}

// write operation for BankTransaction
func (b *BankTransactionTrans) AddBankTransaction(o *models.BankTransaction)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddBankTransaction(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err, id)
}


func (b *BankTransactionTrans) prepareBankTrans(ormer orm.Ormer,
				 o *models.BankTransaction)(*models.BankTransaction,*models.Wallet, error) {

	bt,_ := models.GetBankTransactionByOrderId(ormer,o.OrderId)
    if bt!=nil && bt.State>=2 {
    	return nil,nil,common.MakeError(common.CodeTxAlreadyCompletedError,"Bank Transaction alreadly compeleted")
    }
    aw,eaw:=models.GetWalletByUtxoAddress(ormer,o.WalletAddr)
    if eaw!=nil || aw==nil {
    	return nil,nil,common.MakeError(common.CodeCanNotFindWalletAddressError,"Can Not Find Wallet Address:"+o.WalletAddr)
    }
    return bt,aw,nil
}

func (b *BankTransactionTrans) Exchange(o *models.BankTransaction)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()

	m := utxo.GetUtxoServer().GetVerifyMode()
	if m==0{
    	return HandleTransaction(ormer,ISSUE_OPERATION,o)
    }
	if m==1 {

	    TransLock()
	    defer TransUnLock()

		mc.BeginTrans(ormer)
		defer mc.EndTrans(ormer,&e)
	
		bt,aw,epbt := b.prepareBankTrans(ormer,o)
		if epbt!=nil {
			return common.MakeResult(epbt,nil)
		}

		c,epay := utxo.GetUtxoServer().PayIssueUtxo(o)
		if epay!=nil {
			return common.MakeResult(epay,nil)
		}

		ehc := HandlePayCurrency(ormer,nil,c,nil)
		if ehc!=nil {
			e = ehc
			return common.MakeResult(e,nil)
		}
		
		euwb := models.UpdateWalletBalance(ormer,aw,uint64(o.Amount),true)
		if euwb!=nil {
			e = euwb
			return common.MakeResult(e,nil)
		}
		_,ebth := HandleBankTransHistory(ormer,bt,false)
		if ebth!=nil {
			e = ebth
			return common.MakeResult(e,nil)
		}
	    logger.Log.Informational(fmt.Sprintf("Issue BankTransaction: %s Success",bt.OrderId))
		return common.MakeResult(e,bt)
	}
	if m==2{
		b,_,epay := utxo.GetUtxoServer().PayUtxoDirect(o.UtxoPayTx)
		if !b {
			//return for defer func to commit or rollback
			e = common.MakeError(common.CodePayUtxoDirectError,"PayUtxoDirect Error, Ret: false")			
		}else{
			e = epay
		}
		return common.MakeResult(e,nil)
	}
	//if m == 0 
    TransLock()
    defer TransUnLock()
    
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	bt,err := models.Exchange(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,bt)
}

func (b *BankTransactionTrans) Deposit(o *models.BankTransaction)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()

	m := utxo.GetUtxoServer().GetVerifyMode()
    if m==0{
    	return HandleTransaction(ormer,DEPOSIT_OPERATION,o)
    }	
	if m==1 {
	    TransLock()
	    defer TransUnLock()
		mc.BeginTrans(ormer)
		defer mc.EndTrans(ormer,&e)

		_,aw,epbt := b.prepareBankTrans(ormer,o)
		if epbt!=nil {
			return common.MakeResult(epbt,nil)
		}
		
	    qc,eqc := models.QueryCurrencyByWalletId(ormer,aw.Id)
	    if eqc != nil{
			return common.MakeResult(eqc,nil)
	    }

		c,epay := utxo.GetUtxoServer().PayDepositUtxo(qc,o,aw)
		if epay!=nil {
			return common.MakeResult(epay,nil)
		}

		ehc := HandlePayCurrency(ormer,qc,c,nil)
		if ehc!=nil {
			e = ehc
			return common.MakeResult(e,nil)
		}
		
		euwb := models.UpdateWalletBalance(ormer,aw,uint64(o.Amount),false)
		if euwb!=nil {
			e = euwb
			return common.MakeResult(e,nil)
		}

		o.BankTransactionType=models.BankTransactionTypeIssue//存钱
	    o.State=models.BankTransactionStateBankStart//开始存钱事务，银行操作未完成
	    orderId,esbt := models.StartBankTransaction(ormer, o)
	    if esbt!= nil{
	        e = esbt
			return common.MakeResult(e,nil)
	    }

	    logger.Log.Informational(fmt.Sprintf("Deposit StartBankTransaction: %s Success",orderId))
		return common.MakeResult(nil,orderId)
	}
	if m==2{
		b,_,epay := utxo.GetUtxoServer().PayUtxoDirect(o.UtxoPayTx)
		if !b {
			//return for defer func to commit or rollback
			e = common.MakeError(common.CodePayUtxoDirectError,"PayUtxoDirect Error, Ret: false")
		}else{
			e = epay
		}
	
		return common.MakeResult(e,nil)
	}
	//if m == 0 
    TransLock()
    defer TransUnLock()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	bt,err := models.Deposit(ormer,o)
	sbt := ""
	if bt!=nil {
		sbt = *bt
	}
	e = err //return for defer func to commit or rollback

	return common.MakeResult(e,sbt)
}

func (b *BankTransactionTrans) DeleteBankTransaction(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteBankTransaction(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *BankTransactionTrans) UpdateBankTransaction(id int64, o *models.BankTransaction)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateBankTransaction(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
	    
}
