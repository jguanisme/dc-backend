package trans

import (
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about Device
type DeviceTrans struct {
}

func (b *DeviceTrans) GetAllDevice() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllDevice(ormer)

	return common.MakeResult(err,o)
}

func (b *DeviceTrans) QueryDevice(qm *common.QueryParam)(interface{}, error) {
    ormer := mc.CreateTrans()

	o,err := models.QueryDevice(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *DeviceTrans) GetDeviceBySn(sn string)(interface{}, error) {
    ormer := mc.CreateTrans()

	o,err := models.GetDeviceBySn(ormer, sn)

	return common.MakeResult(err,o)
}

func (b *DeviceTrans) GetDevice(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetDevice(ormer,id)

	return common.MakeResult(err,o)
}

// write operation for Device
func (b *DeviceTrans) AddDevice(o *models.Device)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddDevice(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err, id)
}

func (b *DeviceTrans) DeleteDevice(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteDevice(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *DeviceTrans) UpdateDevice(id int64, o *models.Device)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateDevice(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
	    
}

func (b *DeviceTrans) GetDevicesByShopId(ShopId int64)(interface{}, error) {
	ormer := mc.CreateTrans()
	o, err := models.GetDevicesByShopId(ormer, ShopId)
	return common.MakeResult(err, o)
}
