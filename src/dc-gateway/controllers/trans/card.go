package trans

import (
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about Card
type CardTrans struct {
}

func (b *CardTrans) GetAllCard() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllCard(ormer)

	return common.MakeResult(err,o)
}

func (b *CardTrans) QueryCard(qm *common.QueryParam)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryCard(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *CardTrans) QueryCardByWalletId(walletId int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryCardByWalletId(ormer, walletId)

	return common.MakeResult(err,o)
}

func (b *CardTrans) GetCard(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetCard(ormer,id)

	return common.MakeResult(err,o)
}

// write operation for Card
func (b *CardTrans) AddCard(o *models.Card)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddCard(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err, id)
}

func (b *CardTrans) DeleteCard(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteCard(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *CardTrans) UpdateCard(id int64, o *models.Card)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateCard(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
	    
}