package trans

import (
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about TransactionHistory
type TransactionHistoryTrans struct {
}

func (b *TransactionHistoryTrans) GetAllTransactionHistory() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllTransactionHistory(ormer)

	return common.MakeResult(err,o)
}

func (b *TransactionHistoryTrans) QueryTransactionHistory(qm *common.QueryParam)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryTransactionHistory(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *TransactionHistoryTrans) GetTransactionHistory(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetTransactionHistory(ormer,id)

	return common.MakeResult(err,o)
}


func (b *TransactionHistoryTrans) QueryTransactionHistoryByWalletAddr(qm *common.QueryParam)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryTransactionHistoryByWalletAddr(ormer,qm)

	return common.MakeResult(err,o)
}

// write operation for TransactionHistory
func (b *TransactionHistoryTrans) AddTransactionHistory(o *models.TransactionHistory)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddTransactionHistory(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,id)
}

func (b *TransactionHistoryTrans) DeleteTransactionHistory(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteTransactionHistory(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *TransactionHistoryTrans) UpdateTransactionHistory(id int64, o *models.TransactionHistory)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateTransactionHistory(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
	    
}