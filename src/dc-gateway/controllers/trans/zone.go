package trans

import (
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about Zone
type ZoneTrans struct {
}

func (b *ZoneTrans) GetAllZone() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllZone(ormer)

	return common.MakeResult(err,o)
}

func (b *ZoneTrans) QueryZone(qm *common.QueryParam)(interface{}, error) {
    ormer := mc.CreateTrans()

	o,err := models.QueryZone(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *ZoneTrans) GetZone(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetZone(ormer, id)

	return common.MakeResult(err,o)
}

func (b *ZoneTrans) GetCurrentZone()(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetZone(ormer, 1)

	return common.MakeResult(err, o)
}

// write operation for zone
func (b *ZoneTrans) AddZone(o *models.Zone)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddZone(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,id)
}

func (b *ZoneTrans) DeleteZone(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer, &e)

	models.DeleteZone(ormer, id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *ZoneTrans) UpdateZone(id int64, o *models.Zone)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateZone(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
}
