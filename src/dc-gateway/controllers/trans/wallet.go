package trans

import (
	"errors"

	"dc-gateway/utxo"
	"dc-gateway/logger"
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about Wallet
type WalletTrans struct {
}

func (b *WalletTrans) GetAllWallet() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllWallet(ormer)
	
	return common.MakeResult(err,o)
}

func (b *WalletTrans) QueryWallet(qm *common.QueryParam)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryWallet(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *WalletTrans) QueryWalletByAccountId(accountId int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryWalletByAccountId(ormer, accountId)

	return common.MakeResult(err,o)
}

func (b *WalletTrans) GetWalletByUtxoAddress(addr string)(interface{}, error) {
	ormer := mc.CreateTrans()
	//has private key in models layer, so discard it at trans layer
	o,err := models.GetWalletByUtxoAddress(ormer, addr)
	if o!=nil{
		o.PrivKey = ""
	}

	return common.MakeResult(err,o)
}

func (b *WalletTrans) GetWallet(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetWallet(ormer,id)

	return common.MakeResult(err,o)
}

// write operation for Wallet
func (b *WalletTrans) AddWallet(o *models.Wallet)(interface{}, error) {
	var e error 

	obj := o
    if obj.PubKey!=""{
        addr,ecafp := utxo.CreateAddressFromPublicKey(0,obj.PubKey)
        if ecafp!=nil{
            return 0, ecafp
        }
        //if obj.UtxoAddress=="" {
        if obj.PubKey!="" {
            obj.UtxoAddress = addr
        }else{
            if obj.UtxoAddress != addr{
        		logger.Log.Warn("Wallet address dose not match public key")
                // return 0,errors.New("Wallet address dose not match public key")
            }
        }
    }

    if obj.UtxoAddress=="" {
        return 0,errors.New("Wallet address can not be empty")
    }
    if obj.PrivKey=="" {
        logger.Log.Warn("Thin mode wallet private key can not be empty")
        //return 0,errors.New("Thin mode wallet private key can not be empty")
    }
	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddWallet(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,id)
}

func (b *WalletTrans) DeleteWallet(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteWallet(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *WalletTrans) UpdateWallet(id int64, o *models.Wallet)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateWallet(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
	    
}
