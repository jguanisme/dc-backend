package trans
 
import (
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about User
type UserTrans struct {
}

func (b *UserTrans) GetAllUser() (interface{}, error) {
	ormer := mc.CreateTrans()

	o, err := models.GetAllUser(ormer)

	return common.MakeResult(err, o)
}

func (b *UserTrans) QueryUser(qm *common.QueryParam)(interface{}, error) {
    ormer := mc.CreateTrans()

	o,err := models.QueryUser(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *UserTrans) GetUser(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetUser(ormer,id)

	return common.MakeResult(err,o)
}

func (b *UserTrans) GetCurrentUser()(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetUser(ormer,1)

	return common.MakeResult(err,o)
}


// write operation for User
func (b *UserTrans) AddUser(o *models.User)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddUser(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,id)
}

func (b *UserTrans) DeleteUser(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteUser(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *UserTrans) UpdateUser(id int64, o *models.User)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateUser(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
}
