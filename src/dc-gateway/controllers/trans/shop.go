package trans

import (
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about Shop
type ShopTrans struct {
}

func (b *ShopTrans) GetAllShop() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllShop(ormer)

	return common.MakeResult(err,o)
}

func (b *ShopTrans) QueryShop(qm *common.QueryParam)(interface{}, error) {
    ormer := mc.CreateTrans()

	o,err := models.QueryShop(ormer, qm)

	return common.MakeResult(err, o)
}

func (b *ShopTrans) GetShop(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetShop(ormer, id)

	return common.MakeResult(err,o)
}

// write operation for Shop
func (b *ShopTrans) AddShop(o *models.Shop)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer, &e)

	id,err := models.AddShop(ormer, o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err, id)
}

func (b *ShopTrans) DeleteShop(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteShop(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *ShopTrans) UpdateShop(id int64, o *models.Shop)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer, &e)

	uo,err := models.UpdateShop(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err, uo)
}
