package trans

import (
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about Currency
type CurrencyTrans struct {
}

func (b *CurrencyTrans) GetAllCurrency() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllCurrency(ormer)

	return common.MakeResult(err,o)
}

func (b *CurrencyTrans) QueryCurrency(qm *common.QueryParam)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryCurrency(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *CurrencyTrans) QueryCurrencyByWalletId(walletId int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryCurrencyByWalletId(ormer, walletId)

	return common.MakeResult(err,o)
}

func (b *CurrencyTrans) GetCurrency(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetCurrency(ormer,id)

	return common.MakeResult(err,o)
}

// write operation for Currency
func (b *CurrencyTrans) AddCurrency(o *models.Currency)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddCurrency(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err, id)
}

func (b *CurrencyTrans) DeleteCurrency(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteCurrency(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *CurrencyTrans) UpdateCurrency(id int64, o *models.Currency)(interface{}, error) {
	var e error
	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateCurrency(ormer, id, o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
	    
}


func (b *CurrencyTrans) CorrectWalletCurrency(addr string)(interface{}, error) {
	ormer := mc.CreateTrans()

	uo,err := HandleCorrectCurrency(ormer,addr)	

	return common.MakeResult(err,uo)
	    
}

// Transactions about InvalidCurrency
type InvalidCurrencyTrans struct {
}

func (b *InvalidCurrencyTrans) GetAllInvalidCurrency() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllInvalidCurrency(ormer)

	return common.MakeResult(err,o)
}

func (b *InvalidCurrencyTrans) QueryInvalidCurrency(qm *common.QueryParam)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryInvalidCurrency(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *InvalidCurrencyTrans) QueryInvalidCurrencyByWalletId(walletId int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryInvalidCurrencyByWalletId(ormer, walletId)

	return common.MakeResult(err,o)
}

func (b *InvalidCurrencyTrans) GetInvalidCurrency(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetInvalidCurrency(ormer,id)

	return common.MakeResult(err,o)
}

// write operation for InvalidCurrency
func (b *InvalidCurrencyTrans) AddInvalidCurrency(o *models.InvalidCurrency)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddInvalidCurrency(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err, id)
}

func (b *InvalidCurrencyTrans) DeleteInvalidCurrency(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteInvalidCurrency(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *InvalidCurrencyTrans) UpdateInvalidCurrency(id int64, o *models.InvalidCurrency)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateInvalidCurrency(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
	    
}