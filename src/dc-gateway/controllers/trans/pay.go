package trans

import (
    "dc-gateway/utxo"
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about Pay
type PayTrans struct {
}

func (b *PayTrans) GetAllPay() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllPay(ormer)

	return common.MakeResult(err,o)
}

func (b *PayTrans) QueryPay(qm *common.QueryParam)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryPay(ormer, qm)

	return common.MakeResult(err,o)
}

func (b *PayTrans) QueryPayByWalletAddr(applyWalletAddr string, receiptWalletAddr string)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.QueryPayByWalletAddr(ormer, applyWalletAddr, receiptWalletAddr)

	return common.MakeResult(err,o)
}

func (b *PayTrans) GetPay(id int64)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetPay(ormer,id)

	return common.MakeResult(err,o)
}

// write operation for Pay
func (b *PayTrans) AddPay(o *models.Pay)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	id,err := models.AddPay(ormer,o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err, id)
}

func (b *PayTrans) DeletePay(id int64)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeletePay(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *PayTrans) UpdatePay(id int64, o *models.Pay)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdatePay(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
	    
}

func (b *PayTrans) TransferPayTx(o *models.Pay)(interface{}, error) {
    var e error
    ormer := mc.CreateTrans()

    m := utxo.GetUtxoServer().GetVerifyMode()
    if m==0{
    	return HandleTransaction(ormer,TRANSFER_OPERATION,o)
    }
	if m==1 {

		mc.BeginTrans(ormer)
		defer mc.EndTrans(ormer,&e)

		qc,cc,pc,evp := VerifyPayTx(ormer,o)
		if evp!=nil {
			e = evp
			return common.MakeResult(e,nil)
		}
		
		ehc := HandlePayCurrency(ormer,qc,cc,pc)
		if ehc!=nil {
			e = ehc
			return common.MakeResult(e,nil)
		}

		ehwb := HandlePayWalletBalance(ormer,o)
		if ehwb!=nil {
			e = ehwb
			return common.MakeResult(e,nil)
		}

		o.State = 3
        id,eap := models.AddPay(ormer,o)
		if eap!=nil {
			e = eap
			return common.MakeResult(e,nil)
		}

		o.Id = id 
		pay,eph := HandlePayHistory(ormer,o)
		if eph!=nil {
			e = eph
			return common.MakeResult(e,nil)
		}

		return common.MakeResult(e,pay)
		
	}

	if m==2 {
		b,_,epay := utxo.GetUtxoServer().PayUtxoDirect(o.UtxoPayTx)
		if !b {
			e = common.MakeError(common.CodePayUtxoDirectError,"PayUtxoDirect Error, Ret: false")
		}else{
			e = epay
		}
		return common.MakeResult(e,nil)
	}
	//if m == 0 
    mc.BeginTrans(ormer)
    defer mc.EndTrans(ormer, &e)
    obj, err := models.TransferPayTx(ormer, o)
    e = err //return for defer func to commit or rollback
    return common.MakeResult(err, obj)
}

func (b *PayTrans) CreatePayTx(o *models.Pay)(interface{}, error) {
    var e error
    ormer := mc.CreateTrans()
    mc.BeginTrans(ormer)
    defer mc.EndTrans(ormer, &e)
    id, err := models.CreatePayTx(ormer, o)
    e = err //return for defer func to commit or rollback
    return common.MakeResult(err, id)
}

func (b *PayTrans) QueryPayTx(walletAddr string,state int64,isApply int64,sn string)(interface{}, error) {
    ormer := mc.CreateTrans()
    o, err := models.QueryPayTx(ormer, walletAddr , state, isApply,sn)
    return common.MakeResult(err, o)
}

func (b *PayTrans) ConfirmPayTx(o *models.Pay)(interface{}, error) {
    var e error
    ormer := mc.CreateTrans()

    m := utxo.GetUtxoServer().GetVerifyMode()
    if m==0{
        apay,egp := models.GetPay(ormer,o.Id)
		if egp!=nil {
			return common.MakeResult(egp,nil)
		}
    	return HandleTransaction(ormer,CONFIRM_OPERATION,apay)
    }
	if m==1 {
	    TransLock()
	    defer TransUnLock()
		mc.BeginTrans(ormer)
		defer mc.EndTrans(ormer,&e)

        apay,egp := models.GetPay(ormer,o.Id)
		if egp!=nil {
			e = egp
			return common.MakeResult(e,nil)
		}

		qc,cc,pc,evp := VerifyPayTx(ormer,apay)
		if evp!=nil {
			e = evp
			return common.MakeResult(e,nil)
		}

		ehc := HandlePayCurrency(ormer,qc,cc,pc)
		if ehc!=nil {
			e = ehc
			return common.MakeResult(e,nil)
		}

		ehwb := HandlePayWalletBalance(ormer,apay)
		if ehwb!=nil {
			e = ehwb
			return common.MakeResult(e,nil)
		}

		o.State = 3
        _,eap := models.UpdatePay(ormer,o.Id,o)
		if eap!=nil {
			e = eap
			return common.MakeResult(e,nil)
		}

		pay,eph := HandlePayHistory(ormer,o)
		if eph!=nil {
			e = eph
			return common.MakeResult(e,nil)
		}

		return common.MakeResult(e,pay)		
	}

	if m==2{
		b,_,epay := utxo.GetUtxoServer().PayUtxoDirect(o.UtxoPayTx)
		if !b {
			e = common.MakeError(common.CodePayUtxoDirectError,"PayUtxoDirect Error, Ret: false")	
		}else{
			e = epay
		}
		return common.MakeResult(e,nil)
	}
	//if m == 0 
	
    TransLock()
    defer TransUnLock()
	mc.BeginTrans(ormer)
    defer mc.EndTrans(ormer, &e)
    p,err := models.ConfirmPayTx(ormer, o)
    e = err //return for defer func to commit or rollback
    return common.MakeResult(err, p)
}