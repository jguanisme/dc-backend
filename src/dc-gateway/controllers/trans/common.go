package trans
 
import (
    "sync"
    "fmt"
    "time"
    "math/rand"
    "encoding/json"

	"dc-gateway/logger"
	"dc-gateway/utxo"
	"dc-gateway/common"
	"dc-gateway/models"
	mdpts "dc-gateway/models/dpts"
	mc "dc-gateway/models/common"

    "github.com/astaxie/beego/orm"
)


//transaction lock
var lock sync.Mutex

func TransLock(){
	//lock.Lock()
}
func TransUnLock(){
	//lock.Unlock()
}
func TestTransUnLock(){
	lock.Unlock()
	defer lock.Unlock()
}
type CommonTrans struct {
}
func (b *CommonTrans) QueryRfscOne(tn string,qm *common.ReflectSqlConds) (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := mdpts.QueryRfscOne(ormer,tn,qm)

	return common.MakeResult(err,o)
}
func (b *CommonTrans) QueryRfscMany(tn string,qm *common.ReflectSqlConds) (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := mdpts.QueryRfscMany(ormer,tn,qm)

	return common.MakeResult(err,o)
}
func VerifyPayTx(ormer orm.Ormer, o *models.Pay)([]*models.Currency, *models.Currency, *models.Currency, error) {
	aw,eaw:=models.GetWalletByUtxoAddress(ormer,o.ApplyWalletAddr)
    if eaw!=nil || aw==nil {
    	return nil,nil,nil, common.MakeError(common.CodeCanNotFindWalletAddressError,"Can Not Find Wallet Address:"+o.ApplyWalletAddr)
    }
    qc,eqc := models.QueryCurrencyByWalletId(ormer,aw.Id)
    if eqc != nil{
		return nil,nil,nil, eqc
    }
    if qc == nil||len(qc)==0{
        return nil,nil,nil, common.MakeError(common.CodeNoCurrencyToPayError,"No Currency of Wallet:"+o.ApplyWalletAddr)
    }
	cc,pc,evp := utxo.GetUtxoServer().PayTransferUtxo(qc,o,aw)
	return qc,cc,pc,evp
}

func hanldeCurrencyWalletIdFromAddr(ormer orm.Ormer,c *models.Currency) error{
	if c!=nil && c.WalletId==0 {
		aw,eaw:=models.GetWalletByUtxoAddress(ormer,c.UtxoAddress)
		if eaw!=nil || aw==nil {
	    	return common.MakeError(common.CodeCanNotFindWalletAddressError,"Can Not Find Wallet Address:"+c.UtxoAddress)
	    }
	    c.WalletId = aw.Id
	}
	return nil
}

func HandlePayCurrency(ormer orm.Ormer,qc []*models.Currency,cc *models.Currency,pc *models.Currency) error{
	if len(qc)>0 {
		for _,v := range qc {
			models.DeleteCurrency(ormer,v.Id)
		}		
	}
	if cc!=nil {
		hanldeCurrencyWalletIdFromAddr(ormer,cc)
		_,eacc := models.AddCurrency(ormer,cc)
		if eacc!=nil {
			return eacc
		}		
	}
	if pc!=nil {		
		hanldeCurrencyWalletIdFromAddr(ormer,pc)
		_,eapc := models.AddCurrency(ormer,pc)
		if eapc!=nil {
			return eapc
		}
	}
	return nil
}

func HandlePayHistory(ormer orm.Ormer,o *models.Pay) (*models.Pay,error){
        pay,egp := models.GetPay(ormer,o.Id)
		if egp!=nil {
			return nil,egp
		}

        _,eah := models.AddPayTransactionHistory(ormer,pay)
		if eah!=nil {
			return nil,eah
		}

		return pay,nil
}

func HandleBankTransHistory(ormer orm.Ormer,o *models.BankTransaction, isDeposit bool) (*models.BankTransaction,error){
        bt,egp := models.GetBankTransaction(ormer,o.Id)
		if egp!=nil {
			return nil,egp
		}

        _,eah := models.AddBankTransactionHistory(ormer,o,isDeposit)
		if eah!=nil {
			return nil,eah
		}

		return bt,nil
}

func HandlePayWalletBalance(ormer orm.Ormer,o *models.Pay) error{
	euwbm := models.UpdateWalletBalanceByAddr(ormer,o.ApplyWalletAddr,uint64(o.Amount),false)
	if euwbm!=nil {
		return euwbm
	}
	euwba := models.UpdateWalletBalanceByAddr(ormer,o.ReceiptWalletAddr,uint64(o.Amount),true)
	if euwba!=nil {
		return euwba
	}
	return nil
}

func saveCardAttribute(ormer orm.Ormer,o *models.BankTransaction){
	if o!=nil{
		if o.CardId>0&&o.CardAttribute==""{
			c,err:=models.GetCard(ormer,o.CardId)
			if err==nil{
				o.CardAttribute=c.Attribute
			}
		}
	}
}

func updateDeposit(ormer orm.Ormer,obj interface{})(interface{},error){
	o := (obj).(*models.BankTransaction)

	o.BankTransactionType = models.BankTransactionTypeDeposit//存钱
	o.State = models.BankTransactionStateBankStart//开始存钱事务，银行操作未完成
	saveCardAttribute(ormer,o)
    orderId,errsbt := models.StartBankTransaction(ormer, o)
    if errsbt!= nil{
    	return nil,errsbt
    }
    bt,errgt := models.GetBankTransactionByOrderId(ormer, orderId)
	if errgt!=nil {
		return nil,errgt
	}
    _,err := HandleBankTransHistory(ormer,bt,true)
	if err!=nil {
		return nil,err
	}
    return orderId,nil
}
func updateIssue(ormer orm.Ormer,obj interface{})(interface{},error){
	o := (obj).(*models.BankTransaction)

	o.BankTransactionType = models.BankTransactionTypeIssue//取钱
	saveCardAttribute(ormer,o)
	var obj1 models.BankTransaction
	obj1.OrderId = o.OrderId
	obj1.BankTransactionType = models.BankTransactionTypeIssue//取钱
	obj1.State = models.BankTransactionStateBankSuccess
	obj1.CardAttribute = o.CardAttribute
    bt,errubt := models.UpdateBankTransactionByOrderId(ormer, &obj1)
    if errubt!= nil{
    	return nil,errubt
    }
    _,err := HandleBankTransHistory(ormer,bt,false)
	if err!=nil {
		return nil,err
	}
    return bt,nil
}
func updateTransfer(ormer orm.Ormer,obj interface{})(interface{},error){
	o := (obj).(*models.Pay)

	o.State = models.PayTransactionStateSuccess
    id,errap :=  models.AddPay(ormer,o)
	if errap!=nil {
		return nil,errap
	}
	o.Id = id
	p,err := HandlePayHistory(ormer,o)
	if err!=nil {
		return nil,err
	}
	return p,nil
}
func updateConfirm(ormer orm.Ormer,obj interface{})(interface{},error){
	o :=(obj).(*models.Pay)
	
	o.State = models.PayTransactionStateSuccess
	pay,errup := models.UpdatePay(ormer,o.Id,o)
	if errup!=nil {
		return nil,errup
	}
	o.Id = pay.Id
	p,err := HandlePayHistory(ormer,o)
	if err!=nil {
		return nil,err
	}
	return p,nil
}

type TransactionData struct{
	amount int64
	qc []*models.Currency
	cc *models.Currency
	pc *models.Currency
	iwallet *models.Wallet
	owallet *models.Wallet
	updateData func(orm.Ormer,interface{})(interface{},error)

}

func handleBankTransaction(ormer orm.Ormer,operation uint,o *models.BankTransaction) (*TransactionData,error){
	var td TransactionData
	td.amount = o.Amount

	bt,err := models.GetBankTransactionByOrderId(ormer,o.OrderId)
	if err!=nil{
		return nil,err
	}

	if operation==ISSUE_OPERATION{
		if  bt!=nil && bt.State>=models.BankTransactionStateBankSuccess {
	    	return nil, common.MakeError(common.CodeTxAlreadyCompletedError,"Bank Transaction Issue Alreadly Compeleted, Order:"+o.OrderId)
	    }
	}

	aw,err := models.GetWalletByUtxoAddress(ormer,o.WalletAddr)
    if err!=nil || aw==nil {
	    return nil, common.MakeError(common.CodeCanNotFindWalletAddressError,"Can Not Find Wallet Address:"+o.WalletAddr)
    }

    if operation==DEPOSIT_OPERATION{
		td.iwallet = aw

		qcw,err := models.QueryCurrencyByWalletId(ormer,aw.Id)
	    if err != nil{
			return nil,err
	    }

	    qc,cc,err := utxo.GetUtxoServer().PayDepositUtxoExtra(qcw,o,aw)
		if err!=nil {
			return nil,err
		}
	    td.qc = qc
		td.cc = cc
		td.pc = nil

		td.updateData = updateDeposit

	}
	if operation==ISSUE_OPERATION{
	    td.owallet = aw

		c,err := utxo.GetUtxoServer().PayIssueUtxo(o)
		if err!=nil {
			return nil,err
		}
		td.qc = nil
		td.cc = nil
		td.pc = c

		td.updateData = updateIssue
	}

	return &td,nil
}

type UtxoData struct{
	UtxoTxid string
	UtxoVoutIndex int64
	UtxoValue int64
	UtxoMessage string
	UtxoAddress string
}

func handlePayUtxoData(inc []*models.Currency,c *models.Currency,p *models.Currency)(string,string){
	in:=[]*UtxoData{}
	for _,v:=range(inc){
		in=append(in,&UtxoData{
			UtxoTxid:v.UtxoTxid,
			UtxoVoutIndex:v.UtxoVoutIndex,
			UtxoValue:v.UtxoValue,
			UtxoMessage:v.UtxoMessage,
			UtxoAddress:v.UtxoAddress,
			})
	}
	out:=[]*UtxoData{}
	if c!=nil{		
		out=append(out,&UtxoData{
				UtxoTxid:c.UtxoTxid,
				UtxoVoutIndex:c.UtxoVoutIndex,
				UtxoValue:c.UtxoValue,
				UtxoMessage:c.UtxoMessage,
				UtxoAddress:c.UtxoAddress,
				})
	}
	if p!=nil{		
		out=append(out,&UtxoData{
				UtxoTxid:p.UtxoTxid,
				UtxoVoutIndex:p.UtxoVoutIndex,
				UtxoValue:p.UtxoValue,
				UtxoMessage:p.UtxoMessage,
				UtxoAddress:p.UtxoAddress,
				})
	}

	s1,s2:="",""
	if len(in)>0{
		sIn,ep := json.Marshal(in)
		if ep != nil {
			return s1,s2
		}
		s1=string(sIn)
	}
	if len(out)>0{
		sOut,ep := json.Marshal(out)
		if ep != nil {
			return s1,s2
		}
		s2=string(sOut)
	}
	return s1,s2
}
//operation: 0 transfer, 1 confirm
func handlePay(ormer orm.Ormer,operation uint,o *models.Pay) (*TransactionData,error){
	var td TransactionData
	ori:=o
	if o.OrderId!=""{
		unpays,err := models.QueryPayByOrderId(ormer,o.OrderId,models.PayTransactionStateQuery)
		if err!=nil{

		}
		if len(unpays)>0{
			for i,v:=range(unpays){
				if i==0{
					o = unpays[i]
				}else{
					models.DeletePay(ormer,v.Id)
				}
			}
		}
	}
	aw,err:=models.GetWalletByUtxoAddress(ormer,o.ApplyWalletAddr)
    if err!=nil || aw==nil {
	    return nil, common.MakeError(common.CodeCanNotFindWalletAddressError,"Can Not Find Wallet Address:"+o.ApplyWalletAddr)
    }
    rw,err:=models.GetWalletByUtxoAddress(ormer,o.ReceiptWalletAddr)
    if err!=nil || aw==nil {
	    return nil, common.MakeError(common.CodeCanNotFindWalletAddressError,"Can Not Find Wallet Address:"+o.ApplyWalletAddr)
    }

    qcw,err := models.QueryCurrencyByWalletId(ormer,aw.Id)
    if err != nil{
		return nil, err
    }
    if qcw == nil||len(qcw)==0{
        return nil, common.MakeError(common.CodeNoCurrencyToPayError,"No Currency of Wallet:"+o.ApplyWalletAddr)
    }

	qc,cc,pc,err := utxo.GetUtxoServer().PayTransferUtxoExtra(qcw,o,aw)
	if err != nil{
		return nil, err
    }

	td.amount = o.Amount
	td.iwallet = aw
	td.owallet = rw
    td.qc = qc
	td.cc = cc
	td.pc = pc
	usin,usout:=handlePayUtxoData(qc,cc,pc)
	ori.UtxoVin=usin
	ori.UtxoVout=usout
	if operation==TRANSFER_OPERATION{
		td.updateData = updateTransfer
	}
	if operation==CONFIRM_OPERATION{
		td.updateData = updateConfirm
	}
	return &td,nil
}
func handleBalance(ormer orm.Ormer,td *TransactionData) (interface{},error){
	if td.iwallet!=nil{		
		err := models.UpdateWalletBalance(ormer,td.iwallet,uint64(td.amount),false)
		if err!=nil {
			return nil,err
		}
	}
	if td.owallet!=nil{		
		err := models.UpdateWalletBalance(ormer,td.owallet,uint64(td.amount),true)
		if err!=nil {
			return nil,err
		}
	}
	return nil,nil
}
func handleTrans2(ormer orm.Ormer,td *TransactionData,o interface{}) (interface{},error){
	var e error = nil
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	err := HandlePayCurrency(ormer,td.qc,td.cc,td.pc)
	if err!=nil {
		e = err
		return nil,err
	}
	if td.updateData != nil{
		d,err := td.updateData(ormer,o)
		if err!=nil{
			e = err
			return nil,err
		}
		return d,nil
	}
	return nil,nil
}
func handleTrans(ormer orm.Ormer,td *TransactionData,o interface{}) (interface{},error){
	d,e:=handleTrans2(ormer,td,o)
	if e==nil{
		handleBalance(ormer,td)
	}
	return d,e
}
const (
	DEFAULT_OPERATION = iota
	DEPOSIT_OPERATION
	ISSUE_OPERATION
	TRANSFER_OPERATION
	CONFIRM_OPERATION
)

func HandleTransaction(ormer orm.Ormer,operation uint,o interface{}) (interface{},error){
	for i:=0;i<3;i++{
		d,err:=HandleTransaction_Once(ormer,operation,o)
		if err==nil{
			return d,err
		}
		if i>=2{
			return d,common.MakeFromError(common.CodeResultError,err)
		}
		logger.Log.Informational("Transaction Faild Continue To Try, Error:",err.Error())

		//need sleep rand seconds
		time.Sleep(time.Duration(rand.Intn(1000))*time.Millisecond)
	}
	return nil,common.MakeError(common.CodeResultError,"Transaction Faild In 3 Times")
}
func HandleTransaction_Once(ormer orm.Ormer,operation uint,o interface{}) (interface{},error){
	switch o.(type){
	case (*models.BankTransaction):{
		td,err := handleBankTransaction(ormer,operation,(o).(*models.BankTransaction))
		if err!=nil{
			return nil,err
		}
		return handleTrans(ormer,td,o)	
	}
	case (*models.Pay):{
		td,err := handlePay(ormer,operation,(o).(*models.Pay))
		if err!=nil{
			return nil,err
		}
		return handleTrans(ormer,td,o)
	}
	default:
		return nil,common.MakeError(common.CodeNotSupportError,"Service Type Not Support Error")
	}

}

func HandleCorrectCurrency(ormer orm.Ormer, walletAddr string) (interface{},error){
	var td TransactionData
	td.amount = 0

	aw,err:=models.GetWalletByUtxoAddress(ormer,walletAddr)
    if err!=nil || aw==nil {
	    return nil, common.MakeError(common.CodeCanNotFindWalletAddressError,"Can Not Find Wallet Address:"+walletAddr)
    }

    qc,err := models.QueryCurrencyByWalletId(ormer,aw.Id)
    if err != nil{
		return nil, err
    }
    if qc == nil||len(qc)==0{
        return nil, common.MakeError(common.CodeNoCurrencyToPayError,"No Currency of Wallet:"+walletAddr)
    }

	var dc []*models.Currency
    for _,v := range(qc){
		_,r,err := utxo.GetUtxoServer().IsValidTxVin(v.UtxoTxid,v.UtxoVoutIndex)
		if !r{
			dc = append(dc,v)
			logger.Log.Informational(fmt.Sprintf("Invalid Currency,Addr:%s,Txid:%s,Index:%d,Error:%s",
				walletAddr,v.UtxoTxid,v.UtxoVoutIndex,err.Error()))
		}else{			
			td.amount = td.amount + v.Amount
		}
		if err != nil{
			//need log warnning
	    }
    }
    if len(dc)>0{
    	td.qc = dc
    	if aw.Balance>td.amount{
    		td.iwallet = aw
    		td.amount = aw.Balance - td.amount
    	}else{
    		td.owallet = aw
    		td.amount = td.amount - aw.Balance 
    	}
		fmt.Println("[ttttttttttt]",td.amount,aw.Balance,aw.UtxoAddress)
    }
	td.cc = nil
	td.pc = nil
	td.updateData = nil

	return handleTrans(ormer,&td,nil)
	//return &td,nil
}