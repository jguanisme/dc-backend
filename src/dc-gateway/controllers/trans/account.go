package trans
 
import (
	"dc-gateway/common"
	"dc-gateway/models"
	mc "dc-gateway/models/common"
)

// Transactions about Account
type AccountTrans struct {
}

func (b *AccountTrans) GetAllAccount() (interface{}, error){
	ormer := mc.CreateTrans()

	o,err := models.GetAllAccount(ormer)

	return common.MakeResult(err,o)
}

func (b *AccountTrans) QueryAccount(qm *common.QueryParam)(interface{}, error) {
    ormer := mc.CreateTrans()

	o, err := models.QueryAccount(ormer, qm)

	return common.MakeResult(err, o)
}

func (b *AccountTrans) GetAccount(id int)(interface{}, error) {
	ormer := mc.CreateTrans()

	o,err := models.GetAccount(ormer, id)

	return common.MakeResult(err, o)
}

// write operation for Account
func (b *AccountTrans) AddAccount(o *models.Account)(interface{}, error) {
	var e error 

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer, &e)

	id, err := models.AddAccount(ormer, o)

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err, id)
}

func (b *AccountTrans) DeleteAccount(id int)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	models.DeleteAccount(ormer,id)

	e = nil

	return common.MakeResult(e,nil)
}

func (b *AccountTrans) UpdateAccount(id int, o *models.Account)(interface{}, error) {
	var e error

	ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer,&e)

	uo,err := models.UpdateAccount(ormer, id, o)	

	e = err //return for defer func to commit or rollback

	return common.MakeResult(err,uo)
}

func (b *AccountTrans) FastRegister(args *models.FastRegArgs)(interface{}, error) {
//	var e error
	ormer := mc.CreateTrans()
//	mc.BeginTrans(ormer)
//	defer mc.EndTrans(ormer, &e)
	Acctid, err := models.FastRegister(ormer, args)
//	e = err //return for defer func to commit or rollback
	return common.MakeResult(err, Acctid)
}

func (b *AccountTrans) AcctLogin(args *models.AcctLoginArgs)(interface{}, error) {
    ormer := mc.CreateTrans()
    Acctid, err := models.AcctLogin(ormer, args)
    return common.MakeResult(err, Acctid)
}

func (b *AccountTrans) ModAcctPwd(args *models.ModAcctPwdArgs)(interface{}, error) {
    ormer := mc.CreateTrans()
    err := models.ModAcctPwd(ormer, args)
    return common.MakeResult(err, nil)
}

func (b *AccountTrans) GetAcctsByUserId(UserId int64)(interface{}, error) {
    ormer := mc.CreateTrans()
    o, err := models.GetAcctsByUserId(ormer, UserId)
    return common.MakeResult(err, o)
}

func (b *AccountTrans) FillUsrInfo(args *models.FillUsrInfoArgs)(interface{}, error) {
	var e error
    ormer := mc.CreateTrans()
	mc.BeginTrans(ormer)
	defer mc.EndTrans(ormer, &e)
    o, err := models.FillUsrInfo(ormer, args)
	e = err //return for defer func to commit or rollback
    return common.MakeResult(err, o)
}
