package controllers

import (
//	"fmt"
	"encoding/json"
	"dc-gateway/models"
	"dc-gateway/common"
	"dc-gateway/controllers/trans"
	
	"github.com/astaxie/beego"
)

// Operations about Account
type AccountController struct {
	beego.Controller
	b trans.AccountTrans
}

// @Title GetAll
// @Description 获取所有的对象列表
// @Success 200 {object} []models.Account "对象列表"
// @router / [get]
func (c *AccountController) GetAll() {
	defer c.ServeJSON()

	d,e := c.b.GetAllAccount()
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Query
// @Description 查询所有的对象列表
// @Param body body common.QueryParam true "查询条件，Conds所有数据为string，示例:{'id':'1'}，Orders支持asc和desc两种，示例:{'created':'asc'}，Size默认为10"
// @Success 200 {object} []models.Account "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /query [post]
func (c *AccountController) Query() {
	defer c.ServeJSON()

	var qm common.QueryParam
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
		return
	}

	d,e := c.b.QueryAccount(&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Get Account
// @Description 根据标识查询对象内容
// @Param id path int true "对象标识"
// @Success 200 {object} models.Account "对象实例"
// @Failure 403 :id 参数为空
// @router /:id [get]
func (c *AccountController) Get() {
	defer c.ServeJSON()

	id,e := c.GetInt(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.GetAccount(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Param body body models.Account true "创建的对象内容"
// @Success 200 {int} models.Account.Id "创建后的对象标识"
// @Failure 403 HTTP BODY 输入参数为空
// @router / [post]
func (c *AccountController) Post() {
	defer c.ServeJSON()

	var o models.Account
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

    d,e := c.b.AddAccount(&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Delete Account
// @Description 删除对象
// @Param id path int true "对象标识"
// @Success 200 {string} "删除成功"
// @Failure 403 参数 :id 未找到
// @router /:id [delete]
func (c *AccountController) Delete() {
	defer c.ServeJSON()

	id,e := c.GetInt(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.DeleteAccount(id)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title Update Account
// @Description 根据标识修改对象
// @Param id path int true "对象标识"
// @Param body body models.Account true "被修改的对象内容"
// @Success 200 {object} models.Account
// @Failure 403 :id 不是int类型
// @router /:id [put]
func (c *AccountController) Put() {
	defer c.ServeJSON()

	var o models.Account
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &o)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}
	id,e := c.GetInt(":id")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := c.b.UpdateAccount(id,&o)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Description quick registration for an account
// @Param body body models.FastRegArgs true
// @Success 200 {int} models.Account.Id "account id"
// @Failure 403 HTTP BODY invalid args
// @router /FastRegister [post]
func (c *AccountController) FastRegister() {
	defer c.ServeJSON()
	var args models.FastRegArgs
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &args)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return
	}

    d,e := c.b.FastRegister(&args)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Description login by user name and password
// @Param body body models.AcctLoginArgs true
// @Success 200 {int} models.Account.Id "account id"
// @Failure 403 HTTP BODY invalid args
// @router /AcctLogin [post]
func (c *AccountController) AcctLogin() {
	defer c.ServeJSON()

	var args models.AcctLoginArgs
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &args)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return
	}

    d,e := c.b.AcctLogin(&args)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Description modify user login password
// @Param body body models.ModAcctPwdArgs true
// @Success 200
// @Failure 403 HTTP BODY invalid args
// @router /ModAcctPwd [post]
func (c *AccountController) ModAcctPwd() {
	defer c.ServeJSON()
	var args models.ModAcctPwdArgs
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &args)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return
	}
	
    d,e := c.b.ModAcctPwd(&args)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Param UserId query int64 true
// @Success 200 {object} []models.Account
// @Failure 403 HTTP BODY invalid args
// @router /GetAcctsByUsrId [get]
func (c *AccountController) GetAcctsByUserId() {
	defer c.ServeJSON()
	UserId, e := c.GetInt("UserId")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return
	}

	d,e := c.b.GetAcctsByUserId(int64(UserId))
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Param body body models.FillUsrInfoArgs true
// @Success 200
// @Failure 403 HTTP BODY invalid args
// @router /FillUsrInfo [post]
func (c *AccountController) FillUsrInfo() {
	defer c.ServeJSON()

	var args models.FillUsrInfoArgs
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &args)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return
	}

	d,e := c.b.FillUsrInfo(&args)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}
