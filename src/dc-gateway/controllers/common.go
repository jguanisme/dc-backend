package controllers

import (
//	"fmt"
	"encoding/json"
	"dc-gateway/common"
	"dc-gateway/controllers/trans"
	"github.com/astaxie/beego"
)

// Operations about Account
type CommonController struct {
	beego.Controller
	b trans.CommonTrans
}

// @Title Send Default SMsg Code
// @Description 发送默认短信验证码
// @Param mobile query string true "对象标识"
// @Success 200 {object} string "随机验证码"
// @Failure 403 参数为空
// @router /sdsmsg [get]
func (c *CommonController) SendSMsg() {
	defer c.ServeJSON()

	mobile := c.GetString("mobile")

	d,e := common.GetSMsgSender("").SendSMsgDefaultCode(mobile)
	if e == nil {
		c.Data["json"] = d
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


// @Title Send Template SMsg Code
// @Description 发送指定模板的短信验证码
// @Param mobile query string true "对象标识"
// @Param templateId query int true "对象标识"
// @Success 200 {object} string "随机验证码"
// @Failure 403 参数为空
// @router /stsmsg [get]
func (c *CommonController) SendTemplateSMsg() {
	defer c.ServeJSON()

	mobile := c.GetString("mobile")
	templateId,e := c.GetInt("templateId")
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterError,e)
		return 
	}

	d,e := common.GetSMsgSender("").SendSMsgTemplateCode(mobile,templateId)
	if e == nil {
		c.Data["json"] = d
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}


// @Title QueryRfscOne
// @Description 查询所有的对象列表
// @Param tname query string true "对象标识"
// @Param body body common.ReflectSqlConds true "查询条件"
// @Success 200 {object} []map[string]interface{} "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /queryRfscOne [post]
func (c *CommonController) QueryRfscOne() {
	defer c.ServeJSON()

	tn := c.GetString("tname")
	var qm common.ReflectSqlConds
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryRfscOne(tn,&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}

// @Title QueryRfscMany
// @Description 查询所有的对象列表
// @Param tname query string true "对象标识"
// @Param body body common.ReflectSqlConds true "查询条件"
// @Success 200 {object} []map[string]interface{} "对象列表"
// @Failure 403 HTTP BODY 输入参数为空
// @router /queryRfscMany [post]
func (c *CommonController) QueryRfscMany() {
	defer c.ServeJSON()

	tn := c.GetString("tname")
	var qm common.ReflectSqlConds
	e := json.Unmarshal(c.Ctx.Input.RequestBody, &qm)
	if e != nil {
		c.Data["json"] = common.MakeFromError(common.CodeParameterBodyError,e)
		return 
	}

	d,e := c.b.QueryRfscMany(tn,&qm)
	if e == nil {
		c.Data["json"] = common.NewOkResult(d)
	} else {
		c.Data["json"] = common.MakeFromError(common.CodeResultError,e)
	}
}