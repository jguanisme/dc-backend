alter table account modify column description varchar(255);
alter table account modify column zone_id bigint(20);
alter table account modify column user_id bigint(20);
alter table account modify column name varchar(255) unique;


alter table bank_transaction modify column id bigint(20) AUTO_INCREMENT;
alter table bank_transaction modify column bank_transaction_type bigint(20);
alter table bank_transaction modify column currency_id bigint(20) default 0;
alter table bank_transaction modify column wallet_type bigint(20);
alter table bank_transaction modify column state bigint(20) default 0;	
alter table bank_transaction modify column bank_transaction_time varchar(255);


alter table card modify column id bigint(20) AUTO_INCREMENT;
alter table card add column card_number varchar(255);
alter table card add column phone varchar(255);
alter table card add column id_number varchar(255);
alter table card add column person_name varchar(255);
alter table card modify column wallet_id bigint(20);


alter table currency modify column id bigint(20) AUTO_INCREMENT;
alter table currency modify column name varchar(255);
alter table currency modify column description varchar(255);
alter table currency modify column source_bank varchar(255);
alter table currency modify column pre_currency_id bigint(20);
alter table currency modify column wallet_id bigint(20);
alter table currency modify column utxo_txid varchar(255);
alter table currency modify column utxo_singature varchar(255);
alter table currency modify column utxo_message varchar(255);


alter table device modify column id bigint(20) AUTO_INCREMENT;
alter table device modify column name varchar(255);
alter table device modify column description varchar(255);

alter table shop add column display_name varchar(255);


alter table transaction modify column id bigint(20) AUTO_INCREMENT;
alter table transaction modify column name varchar(255);
alter table transaction modify column description varchar(255);
alter table transaction add column order_id varchar(255);
alter table transaction modify column related_trans_id bigint(20);
alter table transaction modify column utxo_vin varchar(8192);
alter table transaction modify column utxo_vout varchar(8192);


alter table transaction_history modify column id bigint(20) AUTO_INCREMENT;
alter table transaction_history modify column transaction_type bigint(20);
alter table transaction_history modify column description varchar(2048);
alter table transaction_history modify column attributes varchar(2048);
alter table transaction_history modify column in_name varchar(256);
alter table transaction_history modify column in_addr varchar(64);
alter table transaction_history modify column out_name varchar(256);
alter table transaction_history modify column out_addr varchar(64);
alter table transaction_history add column order_id varchar(255);
alter table transaction_history modify column state bigint(20);
alter table transaction_history modify column wallet_type bigint(20);
alter table transaction_history modify column zone_name varchar(64);
alter table transaction_history modify column transaction_seq varchar(255);
alter table transaction_history modify column transaction_time varchar(255);


drop table user;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(1) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `score` bigint(20) NOT NULL DEFAULT '0',
  `grade` bigint(20) NOT NULL DEFAULT '0',
  `email` varchar(128) DEFAULT NULL,
  `fixed_phone` varchar(15) DEFAULT NULL,
  `mobile_phone` varchar(11) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `post_code` varchar(6) DEFAULT NULL,
  `cert_type` varchar(2) DEFAULT NULL,
  `cert_no` varchar(32) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `real_name_auth_mark` varchar(1) DEFAULT NULL,
  `mobile_auth_mark` varchar(1) DEFAULT NULL,
  `biz_lic_no` varchar(32) DEFAULT NULL,
  `tax_reg_no` varchar(32) DEFAULT NULL,
  `corp_cert_type` varchar(2) DEFAULT NULL,
  `corp_cert_no` varchar(32) DEFAULT NULL,
  `corp_name` varchar(40) DEFAULT NULL,
  `reg_cap` bigint(20) NOT NULL DEFAULT '0',
  `corp_type` varchar(2) DEFAULT NULL,
  `industry` varchar(32) DEFAULT NULL,
  `turnover` bigint(20) NOT NULL DEFAULT '0',
  `mon_amount` bigint(20) NOT NULL DEFAULT '0',
  `corp_web_url` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


alter table wallet modify column id bigint(20) AUTO_INCREMENT;
alter table wallet modify column name varchar(128);
alter table wallet modify column description varchar(255);
alter table wallet add column image varchar(128);
alter table wallet modify column mode bigint(20);
alter table wallet modify column account_id bigint(20);
alter table wallet modify column pub_key varchar(1024);
alter table wallet modify column priv_key varchar(2048);
alter table wallet modify column utxo_address varchar(64) not null unique;

drop table zone;
CREATE TABLE `zone` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `domain` varchar(64) DEFAULT NULL,
  `priv_key` varchar(2048) DEFAULT NULL,
  `pub_key` varchar(1024) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
);