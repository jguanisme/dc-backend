package conf

import(
	"os"
	"fmt"
	"sync"
	"runtime"
	"path"
	"encoding/json"
)

type AppConfig struct{
	DbUser string `json:"dbuser,omitempty"`
	DbPassword string `json:"dbpass,omitempty"`
	DbUrls string `json:"dburls,omitempty"`
	DbName 	string `json:"dbname,omitempty"`
	DbDebug int `json:"dbdebug,omitempty"`
	LogFileName string `json:"logfilename,omitempty"`

	path string
}


var	config *AppConfig
//server lock
var configLock sync.Mutex
func init(){
	GetAppConfig().LoadDefault()
}
func GetAppConfig() *AppConfig {
	if config == nil{
		configLock.Lock()
		if config == nil{
			config = new(AppConfig)
			_,file,_,ok := runtime.Caller(0)
			if ok{
				p:=path.Dir(file)
				config.path=p
			}
		}
		configLock.Unlock()
	}
	return config
}
func (s *AppConfig)LoadFile(fn string)*AppConfig{
	configLock.Lock()
	defer configLock.Unlock()

	file,_:=os.Open(fn)
    defer file.Close()
    
	decoder:=json.NewDecoder(file)
	err:=decoder.Decode(s)
	if err!=nil{
		fmt.Println(err)
	}
	return s
}

func (s *AppConfig)LoadDefault()*AppConfig{
	return s.LoadFile(s.path+"/app.json")
}